@extends('admin.layout.base')

@section('title', 'Email Control ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Email Control
            </h5>
            
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Template Name</th>
                        <th>Subject</th>
                        <th>Email Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($emails as $index => $email)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $email->template_name }}</td>
                        <td>{{ $email->subject }}</td>
                        <td>
                            @if(isset($email->to_address))
                                @foreach(json_decode($email->to_address) as $to_email)
                                    {{$to_email}},
                                @endforeach
                            @else
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin.email-control.edit', $email->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Template Name</th>
                        <th>Subject</th>
                        <th>Email Address</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
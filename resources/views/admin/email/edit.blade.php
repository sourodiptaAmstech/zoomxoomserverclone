@extends('admin.layout.base')

@section('title', 'Update Email Control')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.email-control.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Email Control</h5>

            <form class="form-horizontal" action="{{route('admin.email-control.update', $email->id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="template_name" class="col-xs-2 col-form-label">Template Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $email->template_name }}" name="template_name" id="template_name" placeholder="Template Name" readonly>
					</div>
				</div>

                <div class="form-group row">
                    <label for="subject" class="col-xs-2 col-form-label">Subject</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $email->subject }}" name="subject"  id="subject" placeholder="Subject">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="to_address" class="col-xs-2 col-form-label">Email Address</label>
                    <div class="col-xs-10">
                        @if(isset($email->to_address))
                            <select name="to_address[]" id="select1" class="form-control" multiple>
                            @foreach(json_decode($email->to_address) as $key => $to_em)
                                 <option value="{{$to_em}}"></option>
                            @endforeach
                            </select>
                        @else
                         <select name="to_address[]" class="form-control" multiple></select>
                        @endif
                    </div>
                </div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Email Control</button>
						<a href="{{route('admin.email-control.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.css" />
<script>
    $("select").tagsinput();
</script>
@endsection

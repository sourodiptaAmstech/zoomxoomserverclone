@extends('admin.layout.base')

@section('title', 'Driver details_byBid Statement')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
        	<a href="{{ route('admin.statement.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
        	
        	<h3 class="mb-1">{{$driver->first_name}}'s Overall Bid Statement - {{$driver->created_at->diffForHumans()}}</h3>

        	<div class="row">

				<div class="row row-md mb-2" style="padding: 15px;">
					<div class="col-md-12">
						<div class="box bg-white">
							<div class="box-block clearfix">
								<h5 class="float-xs-left"></h5>
								<div class="float-xs-right">
								</div>
							</div>

				            <table class="table table-striped table-bordered dataTable" id="table-2">
				                <thead>
				                   <tr>
										<td>Booking ID</td>
										<td>Picked up</td>
										<td>Dropped</td>
										<td>Booking Type</td>
										<td>Bid Date</td>
										<td>Bid Cost</td>
										<td>Action</td>
									</tr>
				                </thead>
				                <tbody>
				               
								@foreach($details_byBids as $index => $details_byBid)
								<tr>
									<td>
										{{$details_byBid->passenger_request_id}}
									</td>
									<td>
										@if($details_byBid->source_address != '')
											{{$details_byBid->source_address}}
										@else
											Not Provided
										@endif
									</td>
									<td>
										@if($details_byBid->destination_address != '')
											{{$details_byBid->destination_address}}
										@else
											Not Provided
										@endif
									</td>
									<td>
										@if($details_byBid->booking_type == "ride_now")
											<span class="tag tag-info">Ride Now</span>
										@endif
										@if($details_byBid->booking_type == "schedule_ride")
											<span class="tag tag-info">Schedule Ride</span>
										@endif
									</td>
									<td>
										<span class="text-muted">{{date('d M Y',strtotime($details_byBid->bid_created_at))}}</span>
									</td>
									<td>
										${{$details_byBid->bid_cost}}
									</td>
									<td>
										<a class="text-primary" href="{{route('admin.statement.details',[$details_byBid->passenger_request_id,'driver-bidstatement'])}}"><span class="underline1">View Ride Details</span></a>
									</td>
								</tr>
								@endforeach
								</tbody>		
				                <tfoot>
				                    <tr>
										<td>Booking ID</td>
										<td>Picked up</td>
										<td>Dropped</td>
										<td>Booking Type</td>
										<td>Bid Date</td>
										<td>Bid Cost</td>
										<td>Action</td>
									</tr>
				                </tfoot>
				            </table>
						</div>
					</div>
				</div>
        	</div>
        </div>
    </div>
</div>

@endsection

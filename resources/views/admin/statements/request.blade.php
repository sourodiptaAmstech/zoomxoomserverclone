@extends('admin.layout.base')

@section('title', 'Request details ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            @if($param=='dashboard')
            <a href="{{ route('admin.dashboard.index') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='overall')
            <a href="{{ route('admin.statement',$param) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='driver-statement')
            <a href="{{route('admin.statement.driver.details', $driver->id)}}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='driver-bidstatement')
            <a href="{{route('admin.statement.driver.detailsbybid', $driver->id)}}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='today')
            <a href="{{ route('admin.statement',$param) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='weekly')
            <a href="{{ route('admin.statement',$param) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='monthly')
            <a href="{{ route('admin.statement',$param) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='yearly')
            <a href="{{ route('admin.statement',$param) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            <h4 style="margin-bottom: 2em;">Request details</h4>
            <div class="row">
                <div class="col-md-6">
                    <dl class="row">
                        <dt class="col-sm-4">User Name :</dt>
                        <dd class="col-sm-8">
                            @if(isset($passenger))
                            {{ $passenger->first_name }} {{ $passenger->last_name }}
                            @else
                            Not Assigned
                            @endif
                        </dd>

                        <dt class="col-sm-4">Driver Name :</dt>
                        <dd class="col-sm-8">
                            @if(isset($driver))
                            {{ $driver->first_name }} {{ $driver->last_name }}
                            @else
                            Not Assigned
                            @endif
                        </dd>

                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">{{ $source_address ? $source_address : '-' }}</dd>

                        <dt class="col-sm-4">Drop Address :</dt>
                        <dd class="col-sm-8">{{ $destination_address ? $destination_address : '-' }}</dd>

                        <dt class="col-sm-4">Ride Status : </dt>
                        <dd class="col-sm-8">
                            {{ $passengerRequest->request_status }}
                        </dd>

                    </dl>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<style type="text/css">
    #map {
        height: 450px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    var map;
    var zoomLevel = 11;

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'));

        var marker = new google.maps.Marker({
            map: map,
            //icon: '/asset/img/marker-start.png',
            anchorPoint: new google.maps.Point(0, -29)
        });

         var markerSecond = new google.maps.Marker({
            map: map,
            //icon: '/asset/img/marker-end.png',
            anchorPoint: new google.maps.Point(0, -29)
        });

        var bounds = new google.maps.LatLngBounds();

        source = new google.maps.LatLng({{ $source_lat }}, {{ $source_long }});
        destination = new google.maps.LatLng({{ $destination_lat }}, {{ $destination_long }});

        marker.setPosition(source);
        markerSecond.setPosition(destination);

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
        directionsDisplay.setMap(map);

        directionsService.route({
            origin: source,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log(result);
                directionsDisplay.setDirections(result);

                marker.setPosition(result.routes[0].legs[0].start_location);
                markerSecond.setPosition(result.routes[0].legs[0].end_location);
            }
        });

        @if($passengerRequest->request_status != 'Completed')
        // var markerProvider = new google.maps.Marker({
        //     map: map,
        //     icon: "/asset/img/marker-car.png",
        //     anchorPoint: new google.maps.Point(0, -29)
        // });

        // provider = new google.maps.LatLng();
        // markerProvider.setVisible(true);
        // markerProvider.setPosition(provider);
        // console.log('Provider Bounds', markerProvider.getPosition());
        // bounds.extend(markerProvider.getPosition());
        @endif

        bounds.extend(marker.getPosition());
        bounds.extend(markerSecond.getPosition());
        map.fitBounds(bounds);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places&callback=initMap" async defer></script>
@endsection
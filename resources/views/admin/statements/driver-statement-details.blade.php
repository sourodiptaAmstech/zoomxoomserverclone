@extends('admin.layout.base')

@section('title', 'Driver Ride Statement')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
        	<a href="{{ route('admin.statement.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
        	
        	<h3 class="mb-1">{{$driver->first_name}}'s Overall Statement - {{$driver->created_at->diffForHumans()}}</h3>

        	<div style="text-align: center;padding: 20px;color: blue;font-size: 24px;">
        		<p><strong>
        			{{-- <span>Over All Earning : ${{$totalEarn}}</span> --}}
        		</strong></p>
        	</div>

        	<div class="row">

            	<div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Total No. of Rides</h6>
							<h1 class="mb-1">{{count($rides)}}</h1>
						</div>
					</div>
				</div>


				{{-- <div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Revenue</h6>
							<h1 class="mb-1">${{$totalEarn}}</h1>
							<i class="fa fa-caret-up text-success mr-0-5"></i><span>from {{count($rides)}} Rides</span>
						</div>
					</div>
				</div> --}}

				<div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Cancelled Rides</h6>
							<h1 class="mb-1">
								@if(isset($cancel_count))
									{{$cancel_count}}
								@else
								0
								@endif
							</h1>
							<i class="fa fa-caret-down text-danger mr-0-5"></i>
							<span>for
									@if(isset($cancel_count)) 
									  @if($cancel_count == 0) 
										0.00% 
									  @else
									  {{round($percent,2)}}% 
									  @endif
									@else
									  0.00%
									@endif
								   Rides
							</span>
						</div>
					</div>
				</div>

				<div class="row row-md mb-2" style="padding: 15px;">
					<div class="col-md-12">
						<div class="box bg-white">
							<div class="box-block clearfix">
								<h5 class="float-xs-left"></h5>
								<div class="float-xs-right">
								</div>
							</div>

				            <table class="table table-striped table-bordered dataTable" id="table-2">
				                <thead>
				                   <tr>
										<td>Booking ID</td>
										<td>Picked up</td>
										<td>Dropped</td>
										<td>Request Details</td>
										<td>Dated on</td>
										<td>Status</td>
										<td>Driver's Earned</td>
									</tr>
				                </thead>
				                <tbody>
				               
									@foreach($rides as $index => $ride)
								<tr>
									<td>
										{{$ride->passenger_request_id}}
									</td>
									<td>
										@if($ride->source_address != '')
											{{$ride->source_address}}
										@else
											Not Provided
										@endif
									</td>
									<td>
										@if($ride->destination_address != '')
											{{$ride->destination_address}}
										@else
											Not Provided
										@endif
									</td>
									<td>
										@if($ride->request_status == "Cancel_By_Driver"||$ride->request_status == "Cancel_By_Passenger")
											<span>No Details Found </span>
										@else
											<a class="text-primary" href="{{route('admin.statement.details',[$ride->passenger_request_id,'driver-statement'])}}"><span class="underline">View Ride Details</span></a>
										@endif
									</td>
									<td>
										<span class="text-muted">{{date('d M Y',strtotime($ride->created_at))}}</span>
									</td>
									<td>
										@if($ride->request_status == "Completed")
											<span class="tag tag-success">{{$ride->request_status}}</span>
										@elseif($ride->request_status == "Cancel_By_Driver")
											<span class="tag tag-danger">Cancel By Driver</span>
										@elseif($ride->request_status == "Cancel_By_Passenger")
										<span class="tag tag-danger">Cancel By Passenger</span>
										@elseif($ride->request_status == "Request_Confirm")
										<span class="tag tag-info">Request Confirm</span>
										@else
											<span class="tag tag-info">{{$ride->request_status}}</span>
										@endif
									</td>
									<td>
										@if($ride->request_status == "Completed")
											@if(isset($ride->earn))
											${{$ride->earn}}
											@else
											$0.00
											@endif
										@else
										$0.00
										@endif
									</td>
								</tr>
									@endforeach
								</tbody>		
				                <tfoot>
				                    <tr>
										<td>Booking ID</td>
										<td>Picked up</td>
										<td>Dropped</td>
										<td>Request Details</td>
										<td>Dated on</td>
										<td>Status</td>
										<td>Driver's Earned</td>
									</tr>
				                </tfoot>
				            </table>
						</div>
					</div>

				</div>

        	</div>

        </div>
    </div>
</div>

@endsection

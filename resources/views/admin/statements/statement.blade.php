@extends('admin.layout.base')

@section('title', $page)

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
        	<h3 class="mb-1 page">{{ $page }}</h3>
        	<input type="hidden" value="{{$type}}" id="type_hidden">
        	<div style="text-align: center;padding: 20px;color: blue;font-size: 24px;">
        		<p><strong>
        			{{-- <span>Over All Earning : ${{$totalEarn}}</span> --}}
        		</strong></p>
        	</div>

        	<div class="row">

            	<div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Total No. of Rides</h6>
							<h1 class="mb-1 total_rides">
								@if(isset($totalRides))
									{{$totalRides}}
								@else
								0
								@endif
							</h1>
						</div>
					</div>
				</div>


				{{-- <div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Revenue</h6>
							<h1 class="mb-1">${{$totalEarn}}</h1>
							<i class="fa fa-caret-up text-success mr-0-5"></i><span>from {{count($rides)}} Rides</span>
						</div>
					</div>
				</div> --}}

				<div class="col-lg-4 col-md-6 col-xs-12">
					<div class="box box-block bg-white tile tile-1 mb-2">
						<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
						<div class="t-content">
							<h6 class="text-uppercase mb-1">Cancelled Rides</h6>
							<h1 class="mb-1 total_cancel_rides">
								@if(isset($totalCancelRides))
									{{$totalCancelRides}}
								@else
								0
								@endif
							</h1>
							<i class="fa fa-caret-down text-danger mr-0-5"></i><span class="percent"> 
								for
								@if(isset($percentage)) 
									@if($percentage == 0) 
										0.00%
									@else 
										{{$percentage}}% 
									@endif
								@else
								0.00%
								@endif
								Rides
							</span>
						</div>
					</div>
				</div>

				<div class="row row-md mb-2" style="padding: 15px;">
					<div class="col-md-12">
						<div class="box bg-white">
							<div class="box-block clearfix">
								<h5 class="float-xs-left"></h5>
								<div class="float-xs-right">
								</div>
							</div>

							{{-- @if(count($rides) != 0) --}}
					            <table class="table table-striped table-bordered dataTable" id="table-22">
					                <thead>
					                   <tr>
											<td>Booking ID</td>
											<td>Driver Name</td>
											<td>Passenger Name</td>
											<td>Picked up</td>
											<td>Dropped</td>
											<td>Dated on</td>
											<td>Status</td>
											<td>Driver's Earned</td>
											<td>Request Details</td>
										</tr>
					                </thead>
					               {{--  <tbody>
					               
										@foreach($rides as $index => $ride)
									<tr>
										<td>
											{{$ride->passenger_request_id}}
										</td>
										<td>
											@if(isset($ride->userDriver))
											 {{$ride->userDriver->first_name}} {{$ride->userDriver->last_name}}
											@else
											@endif
										</td>
										<td>
											@if(isset($ride->userPassenger))
											 {{$ride->userPassenger->first_name}} {{$ride->userPassenger->last_name}}
											@else
											@endif
										</td>
										<td>
											@if($ride->source_address != '')
												{{$ride->source_address}}
											@else
												Not Provided
											@endif
										</td>
										<td>
											@if($ride->destination_address != '')
												{{$ride->destination_address}}
											@else
												Not Provided
											@endif
										</td>
										<td>
											@if($ride->request_status == "Cancel_By_Driver"||$ride->request_status == "Cancel_By_Passenger")
												<span>No Details Found </span>
											@else
												<a class="text-primary" href="{{route('admin.statement.details',[$ride->passenger_request_id,$type])}}"><span class="underline">View Ride Details</span></a>

											@endif
										</td>
										<td>
											<span class="text-muted">{{date('d M Y',strtotime($ride->created_at))}}</span>
										</td>
										<td>
											@if($ride->request_status == "Completed")
												<span class="tag tag-success">{{$ride->request_status}}</span>
											@elseif($ride->request_status == "Cancel_By_Driver")
												<span class="tag tag-danger">Cancel By Driver</span>
											@elseif($ride->request_status == "Cancel_By_Passenger")
											<span class="tag tag-danger">Cancel By Passenger</span>
											@elseif($ride->request_status == "Request_Confirm")
											<span class="tag tag-info">Request Confirm</span>
											@else
												<span class="tag tag-info">{{$ride->request_status}}</span>
											@endif
										</td>
										<td>
											@if($ride->request_status == "Completed")
												@if(isset($ride->earn))
												${{$ride->earn}}
												@else
												$0.00
												@endif
											@else
											$0.00
											@endif
										</td>
									</tr>
										@endforeach
									</tbody> --}}		
					                <tfoot>
					                    <tr>
											<td>Booking ID</td>
											<td>Driver Name</td>
											<td>Passenger Name</td>
											<td>Picked up</td>
											<td>Dropped</td>
											<td>Dated on</td>
											<td>Status</td>
											<td>Driver's Earned</td>
											<td>Request Details</td>
										</tr>
					                </tfoot>
					            </table>
				            {{-- @else
				            <h6 class="no-result" style="text-align: center;font-size: large; padding-bottom: 30px;">No results found</h6>
				            @endif --}} 

						</div>
					</div>

				</div>

        	</div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    $(document).ready(function(){
    	var type = $('#type_hidden').val();
        $('#table-22').DataTable( {
        	"columnDefs": [{
	                'targets': [1,2,3,4,7],
	                'orderable': false
	         }],
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-statement')}}",
                "dataType":"json",
                "type":"POST",
                "data":{ "_token":"<?= csrf_token() ?>", "type": type},
                // success:function(res){
                // 	$('.page').html(res.page);
                // 	$('.total_rides').html(res.totalData);
                // 	$('.total_cancel_rides').html(res.totalCancelData);
                // 	$('.percent').html(res.percentage);
                // },
            },
            "columns":[
                {"data":"passenger_request_id"},
                {"data":"driver_name"},
                {"data":"passenger_name"},
                {"data":"source_address"},
                {"data":"destination_address"},
                {"data":"created_at"},
                {"data":"request_status"},
                {"data":"earn"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    });
</script>

@endsection

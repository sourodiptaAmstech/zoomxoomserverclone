@extends('admin.layout.base')

@section('title', 'Drivers Statement')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
        	<h3>Drivers Statement</h3>

        	<div class="row">

				<div class="row row-md mb-2" style="padding: 15px;">
					<div class="col-md-12">
						<div class="box bg-white">
							<div class="box-block clearfix">
								<h5 class="float-xs-left"></h5>
								<div class="float-xs-right">
								</div>
							</div>

							{{-- @if(count($drivers) != 0) --}}
				            <table class="table table-striped table-bordered dataTable" id="table-driver">
				                <thead>
				                   	<tr>
										<td>Driver Name</td>
										<td>Mobile</td>
										<td>Status</td>
										<td>Total Rides</td>
										<td>Total Earning</td>
										<td>Joined at</td>
										<td>Action</td>
									</tr>
				                </thead>
				                {{-- <tbody>
								@foreach($drivers as $index => $driver)
								<tr>
									<td>
										{{$driver->first_name}} 
										{{$driver->last_name}}
									</td>
									<td>
										{{$driver->isdCode}}{{$driver->mobile}}
									</td>
									<td>
										@if(isset($driver->driver_profile))
										@if($driver->driver_profile->status == "approved")
											<span class="tag tag-success">{{$driver->driver_profile->status}}</span>
										@elseif($driver->driver_profile->status == "banned")
											<span class="tag tag-danger">{{$driver->driver_profile->status}}</span>
										@else
											<span class="tag tag-info">{{$driver->driver_profile->status}}</span>
										@endif
										@endif
									</td>
									<td>
										@if($driver->totalRide)
											{{$driver->totalRide}}
										@else
										 	-
										@endif
									</td>
									<td>
										@if($driver->totalEarn)
											${{$driver->totalEarn}}
										@else
										 	$0.00
										@endif
									</td>
									<td>
										@if($driver->created_at)
											<span class="text-muted">{{$driver->created_at->diffForHumans()}}</span>
										@else
										 	-
										@endif
									</td>
									<td>
										<a href="{{route('admin.statement.driver.details', $driver->id)}}" class="btn btn-info">View by Ride</a>
										<a href="{{route('admin.statement.driver.detailsbybid', $driver->id)}}" class="btn btn-info">View by Bid</a>
									</td>
								</tr>
								@endforeach
								</tbody> --}}
											
				                <tfoot>
				                    <tr>
										<td>Driver Name</td>
										<td>Mobile</td>
										<td>Status</td>
										<td>Total Rides</td>
										<td>Total Earning</td>
										<td>Joined at</td>
										<td>Action</td>
									</tr>
				                </tfoot>
				            </table>
				           {{--  @else
				            <h6 class="no-result">No results found</h6>
				            @endif --}} 

						</div>
					</div>

				</div>

        	</div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#table-driver').DataTable( {
        	"columnDefs": [{
	                'targets': [2,3,4],
	                'orderable': false
	         }],
           "order": [[5, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-driver-statement')}}",
                "dataType":"json",
                "type":"POST",
                "data":{ "_token":"<?= csrf_token() ?>" },
            },
            "columns":[
                {"data":"driver_name"},
                {"data":"mobileno"},
                {"data":"status"},
                {"data":"total_rides"},
                {"data":"total_earn"},
                {"data":"join_at"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    });
</script>
@endsection
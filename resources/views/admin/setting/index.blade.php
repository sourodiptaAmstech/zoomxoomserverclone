@extends('admin.layout.base')

@section('title', 'Site Setting')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Site Setting</h5>
            <table class="table table-striped table-bordered dataTable" id="table-setting">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($site_settings as $index => $site_setting)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            @if($site_setting->key == 'site_title')
                                Title
                            @endif
                            @if($site_setting->key == 'site_logo')
                                Logo
                            @endif
                            @if($site_setting->key == 'site_email_logo')
                                Email Logo
                            @endif
                            @if($site_setting->key == 'site_icon')
                                Icon
                            @endif
                            @if($site_setting->key == 'site_copyright')
                                Copyright
                            @endif
                            @if($site_setting->key == 'web_site')
                                Website
                            @endif
                            @if($site_setting->key == 'contact_number')
                                Contact Number
                            @endif
                            @if($site_setting->key == 'contact_email')
                                Contact Email
                            @endif
                            @if($site_setting->key == 'sos_number')
                                SOS Number
                            @endif
                        </td>
                        <td>
                            @if($site_setting->key == 'site_title'||$site_setting->key == 'site_copyright'||$site_setting->key == 'web_site'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number')
                                {{ $site_setting->value }}
                            @elseif($site_setting->key == 'site_logo'||$site_setting->key == 'site_email_logo'||$site_setting->key == 'site_icon')
                                @if(isset($site_setting->value))
                                    @if(File::exists(storage_path('app/public' .str_replace("storage", "", $site_setting->value))))
                                    <img src="{{URL::asset($site_setting->value)}}" style="height: 60px;">
                                    @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                    @endif
                                @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                @endif
                            @endif
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.setting.edit', $site_setting->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i>Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#table-setting').DataTable( {
        responsive: true,
        dom: 'Bfrtip',
        buttons: []
    } );
});
</script>
@endsection
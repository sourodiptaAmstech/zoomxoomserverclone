@extends('admin.layout.base')

@section('title', 'Add Vehicle ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.vehicle.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Vehicle</h5>

            <form class="form-horizontal" action="{{route('admin.vehicle.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="year" class="col-xs-12 col-form-label">Year</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('year') }}" name="year" required id="year" placeholder="Year">
					</div>
				</div>

				<div class="form-group row">
					<label for="make" class="col-xs-12 col-form-label">Make</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('make') }}" name="make" required id="make" placeholder="Make">
					</div>
				</div>

				<div class="form-group row">
					<label for="model" class="col-xs-12 col-form-label">Model</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('model') }}" name="model" required id="model" placeholder="Model">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Vehicle</button>
						<a href="{{route('admin.vehicle.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection

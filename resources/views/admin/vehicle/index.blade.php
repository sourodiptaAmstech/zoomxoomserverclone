@extends('admin.layout.base')

@section('title', 'Vehicles ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Vehicles
            </h5>
            <a href="{{ route('admin.vehicle.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Vehicle</a>
            <table class="table table-striped table-bordered dataTable" id="table-id">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Action</th>
                    </tr>
                </thead>
              
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-id').DataTable( {
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.dataProcessing')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"id"},
                {"data":"year"},
                {"data":"make"},
                {"data":"model"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    </script>

@endsection
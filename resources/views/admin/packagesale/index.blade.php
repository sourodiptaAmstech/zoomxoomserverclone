@extends('admin.layout.base')

@section('title', 'Package Sales Report')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Package Sales Report</h5>

            <div class="row">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="box box-block bg-white tile tile-1 mb-2">
                        <div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
                        <div class="t-content">
                            <h6 class="text-uppercase mb-1">Total Package Sale</h6>
                            <h1 class="mb-1 quantity">{{ $totalQuantity }}</h1>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="box box-block bg-white tile tile-1 mb-2">
                        <div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
                        <div class="t-content">
                            <h6 class="text-uppercase mb-1">Total Earn</h6>
                            <h1 class="mb-1">
                                @php
                                $TotalEarn = null;
                                @endphp
                                @foreach($getPackages as $index => $getPackage)
                                @php
                                $TotalEarn = $TotalEarn + $getPackage->totalCost;
                                @endphp
                                @endforeach
                                <span class="total_earn">{{$TotalEarn}}</span>
                            </h1>
                            <i class="fa fa-caret-up text-success mr-0-5"></i><span>from <span class="total_quantity">{{$totalQuantity}}</span> Packages</span>
                        </div>
                    </div>
                </div>

                <div class="row row-md mb-2" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="box bg-white">
                            <div class="box-block clearfix">
                                <h5 class="float-xs-left"></h5>
                                <div class="float-xs-right">
                                </div>
                            </div>
                        
                       {{--  <div class="form-group">
                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" class="form-control" /><span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                            </div>
                        </div> --}}
                            
                            <button type="button" style="margin-left: 1em;margin-right: 7px;" name="filter" id="filter" class="btn btn-info btn-sm pull-right">Filter</button>
                            <div class="input-daterange">
                            <input type="date" name="to_date" id="to_date" class=" pull-right" style="margin-left: 1em;"/>
                            <input type="date" name="from_date" id="from_date" class="pull-right" style="margin-left: 1em;"/>
                            </div>


                            <table class="table table-striped table-bordered dataTable" id="packagetable">
                                <thead>
                                   <tr>
                                        <th>S.No</th>
                                        <th>Package Name</th>
                                        <th>Quantity</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tBody">
                                @foreach($getPackages as $index => $getPackage)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$getPackage->name}}</td>
                                        <td>{{$getPackage->quantity}}</td>
                                        <th>{{$getPackage->totalCost}}</th>
                                        <td>
                                            <a href="{{ route('admin.package.sales.report.details', $getPackage->credit_package_log_id) }}" class="btn btn-info">Review</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                            
                                <tfoot>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Package Name</th>
                                        <th>Quantity</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<style type="text/css">
.dt-buttons.btn-group {
    margin-bottom: 8px;
}
</style>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $('#packagetable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        bFilter: false,
        bInfo: false,
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    //$('#datetimepicker1').datetimepicker();

    //$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

    // $('.input-daterange').datepicker({
    //     todayBtn: 'linked',
    //     format: 'yyyy-mm-dd',
    //     autoclose: true
    // });
    // $('#from_date').periodpicker({
    //  end: '#to_date',
    //  okButton: false
    // });

    // $(function(){
    //  $('#from_date').datetimepicker({
    //   format:'Y/m/d',
    //   // onShow:function( ct ){
    //   //  this.setOptions({
    //   //   maxDate:$('#to_date').val()?$('#to_date').val():false
    //   //  })
    //   // },
    //   timepicker:false
    //  });
    //  $('#to_date').datetimepicker({
    //   format:'Y/m/d',
    //   // onShow:function( ct ){
    //   //  this.setOptions({
    //   //   minDate:$('#from_date').val()?$('#from_date').val():false
    //   //  })
    //   // },
    //   timepicker:false
    //   });
    // });
    // $('#from_date').datetimepicker({format: 'Y-m-d'});
    // $('#to_date').datetimepicker({format: 'Y-m-d'});
   
    $('#filter').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date != '' &&  to_date != '')
        {
            $('#from_date').css({'border':''});
            $('#to_date').css({'border':''});
            var dataObject={"data":[]};
            $.ajax({
                url: '{{ route('admin.package.sales.report.search') }}',
                method: 'GET',
                data: { from_date: from_date, to_date: to_date },
                success: function(response){
                    var objectLength=Object.keys(response.getPackages).length;
                    if (objectLength>0) {
                    for(var i=0; i<objectLength; i++){
                        dataObject.data.push(
                            [
                                i+1,
                                response.getPackages[i].name,
                                response.getPackages[i].quantity,
                                response.getPackages[i].totalCost,
                                '<a href="report/detail/'+response.getPackages[i].credit_package_log_id+'/'+response.from_date+'/'+response.to_date+'" class="btn btn-info">Review</a>'
                            ]);
                        }
                    $('#packagetable').DataTable().clear().destroy();
                    $('#packagetable').DataTable({
                        responsive: true,
                        dom: 'Bfrtip',
                        bFilter: false,
                        bInfo: false,
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                        ],
                        'data': dataObject.data
                    });

                    $('.quantity').html(response.totalQuantity);
                    $('.total_quantity').html(response.totalQuantity);
                    $('.total_earn').html(response.totalEarn);
                } else {
                    $('#tBody').css('text-align','center').html('<tr><td colspan="5">No Result Found</td></tr>');
                    $('.quantity').html(response.totalQuantity);
                    $('.total_quantity').html(response.totalQuantity);
                    $('.total_earn').html(response.totalEarn);
                }
              }
            });
        } else {
            //alert('Both Date fields are required');
            $('#from_date').css({'border':'1px solid red'});
            $('#to_date').css({'border':'1px solid red'});
        }
    });
});
</script>
@endsection
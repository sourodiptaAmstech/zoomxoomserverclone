@extends('admin.layout.base')

@section('title', 'Package Sales Report Details')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
        
            <div class="box box-block bg-white">
                <h5 class="mb-1">Package Sales Report Details</h5>
               
                <h4> Package Name: {{$pname}} </h4>

                <a href="{{ route('admin.package.sales.report') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>

                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Driver Name</th>
                            <th>Cost</th>
                            <th>Purchase Date</th>
                        </tr>
                    </thead>
                    <tbody id="tBody">
                    @foreach($transactions as $index => $transaction)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$transaction->user->first_name}} {{$transaction->user->last_name}}</td>
                            <td>{{$transaction->transaction_cost}}</td>
                            <th>{{date('d M Y, h:i A',strtotime($transaction->created_at))}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Driver Name</th>
                            <th>Cost</th>
                            <th>Purchase Date</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
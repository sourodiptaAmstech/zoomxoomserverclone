@extends('admin.layout.base')

@section('title', $title)

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                {{$title}}
            </h5>
            
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Subject</th>
                        <th>Reported Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($bydrivers))
                    @foreach($bydrivers as $index => $bydriver)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $bydriver->user->first_name }} {{ $bydriver->user->last_name }}</td>
                        <td>{{ $bydriver->subject }}</td>
                        <td>{{date('d M Y',strtotime($bydriver->created_at))}}</td>
                        <td>
                            <a href="{{ route('admin.report-issue.details', [$bydriver->issues_id,'bydriver']) }}" class="btn btn-info">View Details</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    @if(isset($bypassengers))
                    @foreach($bypassengers as $index => $bypassenger)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $bypassenger->user->first_name }} {{ $bypassenger->user->last_name }}</td>
                        <td>{{ $bypassenger->subject }}</td>
                        <td>{{date('d M Y',strtotime($bypassenger->created_at))}}</td>
                        <td>
                            <a href="{{ route('admin.report-issue.details', [$bypassenger->issues_id,'bypassenger']) }}" class="btn btn-info">View Details</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Subject</th>
                        <th>Reported Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
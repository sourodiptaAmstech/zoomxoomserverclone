@extends('admin.layout.base')

@section('title', 'Report Details')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            @if($param=='bydriver')
            <a href="{{ route('admin.report-issue.bydriver') }}" style="margin-left: 1em;" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            @endif
            @if($param=='bypassenger')
            <a href="{{ route('admin.report-issue.bypassenger') }}" style="margin-left: 1em;" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            @endif
            <h5 style="margin-bottom: 2em;">Report Details</h5>

            <div class="row">
                <div class="col-md-8">
                    <dl class="row">
                        <dt class="col-sm-4">Reported DateTime :</dt>
                        <dd class="col-sm-8">
                            {{date('d M, Y, h:i A',strtotime($reportIssue->created_at))}}
                            ({{$reportIssue->created_at->diffForHumans()}})
                        </dd>

                        <dt class="col-sm-4">Name :</dt>
                        <dd class="col-sm-8">
                            {{ $reportIssue->user->first_name }} {{ $reportIssue->user->last_name }}
                        </dd>

                        <dt class="col-sm-4">Subject :</dt>
                        <dd class="col-sm-8">
                            {{$reportIssue->subject}}
                        </dd>

                        <dt class="col-sm-4">Description :</dt>
                        <dd class="col-sm-8">
                            {{$reportIssue->description}}
                        </dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
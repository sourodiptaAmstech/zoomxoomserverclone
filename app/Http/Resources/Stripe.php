<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Model\Setting;
use App\User;
use App\Model\Driver\DriverProfiles;

class Stripe extends JsonResource
{
    private $stripe;


    public function __construct()
    {
       // parent::__construct( $resource );
       $stripe_secret_key=Setting::where("key","stripe_secret_key")->select("value")->first();
        $this->stripe = \Stripe\Stripe::setApiKey($stripe_secret_key->value);
    }



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function getStripeID(){
        try{
            return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>false,"e"=>false];
        }
        catch(\Exception $e){
            return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
        }
    }

    public function createStripeAccount($user_email){
        try{
           $stripeData= $this->getStripeID();
           if($stripeData['Exception']==false){
               $customer = \Stripe\Customer::create([
                'email' => $user_email,
                ]);

                return ["message"=>"Stripe Account Created!!", "status"=>true,"Exception"=>false,"e"=>false,"stripe_cust_id"=>$customer['id']];
            }
           else{
            return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$stripeData['e']];
           }
        }
        catch(\Exception $e){
            return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
        }
    }
    public function createCard($customer_id,$stripe_token){

        $customer = \Stripe\Customer::retrieve($customer_id);
        if($customer->deleted==null){
            $card = $customer->sources->create(["source" => $stripe_token]);
        }
        else{
            return ["message"=>"Account not Found!!", "status"=>false,"Exception"=>true,"e"=>[]];
        }

        return ["message"=>"Card Added", "card"=>$card,"status"=>true,"Exception"=>false,"e"=>[]];
    }
    public function deleteCard($customer_id,$card_id){
        try{
            $customer = \Stripe\Customer::retrieve($customer_id);
            $customer->sources->retrieve($card_id)->delete();
            return ["message"=>"Card deleted", "status"=>true,"Exception"=>false,"e"=>[]];
        }
        catch(\Exception $e){
            return ["message"=>"Cannot able to delete the card", "status"=>false,"Exception"=>true,"e"=>$e];
        }


    }
    public function createStripeCharge($data){
        try{
            $stripeData= $this->getStripeID();
            if($stripeData['Exception']==false){
                $Charge = \Stripe\Charge::create(array(
                    "amount" => $data->package_cost*100,
                    "currency" => "usd",
                    "customer" => $data->stripe_customer_id,
                    "card" => $data->card_id,
                    "description" => $data->description,
                   // "receipt_email" => Auth::user()->email
                  ));
                  return ["message"=>"Stripe Charge Created!!", "status"=>true,"Exception"=>false,"e"=>false,"trans_id"=>$Charge['id']];
             }
            else{
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$stripeData['e']];
            }
         }
         catch(\Exception $e){
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
         }
    }

}

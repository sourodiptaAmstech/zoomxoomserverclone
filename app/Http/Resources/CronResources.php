<?php


namespace App\Http\Resources;

use App\Model\Admin;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\RequestRide\RequestRideController;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Admin\Driver\DocumentVerificationController;


/**
 * Class CronResources
 * @package App\Http\Resources
 */
class CronResources
{
    public function __invoke()
    {
        $obj = new DocumentVerificationController;
        $obj->cronjobToMailDriver();
        $this->lockCronService();
        $this->scheduleRide();
        $this->unLockCronService();

        return null;
    }
    private function lockCronService(){
        DB::table('cron_lock')->where('id',1)->update(array(
            'status'=>1,
        ));
    }
    private function unLockCronService(){
        DB::table('cron_lock')->where('id',1)->update(array(
            'status'=>0,
        ));
    }
    private function scheduleRide(){
       try{
           $RequestRide=new RequestRideController();
           $RequestRide->sendScheduleRideNotification();
       }
       catch (\Exception $e){

       }
    }


}

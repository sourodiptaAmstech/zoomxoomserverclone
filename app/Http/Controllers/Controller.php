<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Model\Setting;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function requestValidation($request,$rule){
        $validator =  Validator::make($request,$rule);
        if ($validator->fails()) {
            $array=$validator->getMessageBag()->toArray();
            foreach($array as $key=>$val){
                return (object)['message'=>$val[0],"field"=>$key,"status"=>"false"];
            }
        }
        return (object)['message'=>null,"field"=>null,"status"=>"true"];;

    }
    protected function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    protected function setuserData($userAuth){
     // print_r($profile); exit;

        return [
            "user_id"=>$userAuth->id,
            "first_name"=>$userAuth->first_name,
            "last_name"=>$userAuth->last_name,
            "email"=>$userAuth->email ,
            "mobile"=>$this->checkNull($userAuth->mobile),
            "isdCode"=>$this->checkNull($userAuth->isdCode),
            "picture"=>$this->checkNull($userAuth->picture),
            "isMobileVerified"=>$userAuth->isMobileVerified,
            "scope"=>$userAuth->user_scope
        ];

    }
    protected function setPassengerProfileData($profileObject){
        return [
            "rating"=>$this->checkNull($profileObject->rating),
            "company_name"=>$this->checkNull($profileObject->company_name)
        ];

    }
    protected function setuserDataDriver($userAuth){

        return [
            "user_id"=>$userAuth->id,
            "first_name"=>$userAuth->first_name,
            "last_name"=>$userAuth->last_name,
            "email"=>$userAuth->email ,
            "mobile"=>$this->checkNull($userAuth->mobile),
            "isdCode"=>$this->checkNull($userAuth->isdCode),
            "picture"=>$this->checkNull($userAuth->picture),
            "isMobileVerified"=>$userAuth->isMobileVerified,
            "scope"=>$userAuth->user_scope
        ];

    }
    protected function setDriverProfileData($profileObject){

        return [
            "rating"=>$this->checkNull($profileObject->rating),
            "payment_mode"=>$this->checkNull($profileObject->payment_mode),
            "status"=>$this->checkNull($profileObject->status)
            ];
    }
    protected function setDriverServiceData($serviceObject,$ServiceType){
        return [
            "service_id"=>$this->checkNull($serviceObject->service_id),
            "service_type_id"=>$this->checkNull($serviceObject->service_type_id),
           // "service_type_details"=>$this->setSserviceType($ServiceType),
            "status"=>$this->checkNull($serviceObject->status),
            "car_number"=>$this->checkNull($serviceObject->car_number),
            "car_number_expire_date"=>$this->checkNull($serviceObject->car_number_expire_date),
            "car_plate_number"=>$this->checkNull($serviceObject->car_plate_number),
            "car_model"=>$this->checkNull($serviceObject->car_model),
            "car_make"=>$this->checkNull($serviceObject->car_make),
            "car_make_year"=>$this->checkNull($serviceObject->car_make_year),
            "car_color"=>$this->checkNull($serviceObject->car_color)
        ];
    }
    protected function setSserviceType($ServiceType){
        return [
            "id"=>$this->checkNull($ServiceType->id),
            "name"=>$this->checkNull($ServiceType->name),
            "provider_name"=>$this->checkNull($ServiceType->provider_name),
            "image"=>$this->checkNull($ServiceType->image),
            "image_select"=>$this->checkNull($ServiceType->image_select),
            "image_unselect"=>$this->checkNull($ServiceType->image_unselect),
            "capacity"=>$this->checkNull($ServiceType->capacity),
            "insure_price"=>$this->checkNull($ServiceType->insure_price),
            "price"=>$this->checkNull($ServiceType->price),
            "min_price"=>$this->checkNull($ServiceType->min_price),
            "minute"=>$this->checkNull($ServiceType->minute),
            "distance"=>$this->checkNull($ServiceType->distance),
            "calculator"=>$this->checkNull($ServiceType->calculator),
            "description"=>$this->checkNull($ServiceType->description),
            "min_waiting_time"=>$this->checkNull($ServiceType->min_waiting_time),
            "min_waiting_charge"=>$this->checkNull($ServiceType->min_waiting_charge),
            "status"=>$this->checkNull($ServiceType->status)
        ];
    }
    protected function CardData($CardData){
        if($this->checkNull($CardData->is_default)=="")
            $CardData->is_default=0;

        return [
            "user_cards_id  "=>$this->checkNull($CardData->user_cards_id),
            "last_four"=>$this->checkNull($CardData->last_four),
            "card_id"=>$this->checkNull($CardData->card_id),
            "image"=>$this->checkNull($CardData->image),
            "brand"=>$this->checkNull($CardData->brand),
            "is_default"=>$this->checkNull($CardData->is_default),
        ];
    }
    public static function setting()
    {
        $site_settings = Setting::where('key', 'like', "%site%")->get();
        return $site_settings;
    }
    protected function convertToUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
        return $date;
    }
    protected function convertFromUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
     //   echo $timeZone;
      //  echo $date; exit;
        return $date;
    }
    protected function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close ($ch);
        return $return;
    }
}

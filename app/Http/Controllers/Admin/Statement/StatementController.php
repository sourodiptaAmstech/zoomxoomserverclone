<?php

namespace App\Http\Controllers\Admin\Statement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Request\WaypointRequest;
use App\Model\Request\PassengerRequest;
use App\User;
use App\Model\Request\BidRequest;
use \Carbon\Carbon;
use Exception;


class StatementController extends Controller
{
	public function rideDetails($id,$param)
	{
		$passengerRequest = PassengerRequest::find($id);
		$passenger = User::find($passengerRequest->passenger_id);
		$driver = User::find($passengerRequest->driver_id);
		$waypoints=WaypointRequest::where('passenger_request_id', $id)->get();
			$source_address=null;
			$source_long=null;
			$source_lat=null;
			$destination_address=null;
			$destination_long=null;
			$destination_lat=null;
			foreach ($waypoints as $key => $waypoint) {
				if ($waypoint->waypoint_type=='source') {
					$source_address = $waypoint->address;
					$source_long = $waypoint->longitude;
					$source_lat = $waypoint->latitude;
				}
				if ($waypoint->waypoint_type=='destination') {
					$destination_address = $waypoint->address;
					$destination_long = $waypoint->longitude;
					$destination_lat = $waypoint->latitude;
				}
			}

		return view('admin.statements.request', compact('passengerRequest','passenger','driver','source_address','source_long','source_lat','destination_address','destination_long','destination_lat','param'));
	}

	public function driverStatementIndex1()
	{
		$drivers = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->orderBy('created_at' , 'desc')->get();
		
		foreach ($drivers as $key => $driver) {
			$passengerRequests = PassengerRequest::where('driver_id',$driver->id)->get();

			$driver->totalRide=count($passengerRequests);
			$totalEarn=null;
			foreach ($passengerRequests as $key => $passengerRequest) {
				if ($passengerRequest->request_status=='Completed') {
					$bidRequest = BidRequest::where('passenger_request_id',$passengerRequest->passenger_request_id)->where('driver_id',$passengerRequest->driver_id)->first();
					if (isset($bidRequest)) {
						$totalEarn = $totalEarn+$bidRequest->bid_cost;
					}
				}
				$driver->totalEarn = $totalEarn;
			}
		}
		
		return view('admin.statements.driver-statement', compact('drivers'));
	}


	public function driverStatementIndex()
	{
		return view('admin.statements.driver-statement');
	}

	public function ajaxDriverStatement(Request $request)
	{
		$columns = array(
            0 => 'first_name',
			//1 => 'last_name',
			1 => 'isdCode',
			// 1 => 'mobile',
			5 => 'created_at',
		);
		
		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');

        $totalDrivers = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->count();
		
		if(empty($request->input('search.value'))){
			$drivers = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->offset($start)
					->limit($limit)
					->orderBy($order,$dir)
					->get();
			foreach ($drivers as $key => $driver) {
				$passengerRequests = PassengerRequest::where('driver_id',$driver->id)->get();
				$driver->totalRide=count($passengerRequests);
				$totalEarn=null;
				foreach ($passengerRequests as $key => $passengerRequest) {
					if ($passengerRequest->request_status=='Completed') {
						$bidRequest = BidRequest::where('passenger_request_id',$passengerRequest->passenger_request_id)->where('driver_id',$passengerRequest->driver_id)->first();
						if (isset($bidRequest)) {
							$totalEarn = $totalEarn+$bidRequest->bid_cost;
						}
					}
					$driver->totalEarn = $totalEarn;
				}
			}
			
			$totalFiltered = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->count();
		}else{
			$search = $request->input('search.value');
            $drivers = User::where('user_scope', 'driver-service')
            	->where(function($q) use ($search){
					$q->where('first_name', 'like', "%{$search}%")
	                ->orWhere('last_name', 'like', "%{$search}%")
					->orWhere('isdCode','like',"%{$search}%")
					->orWhere('mobile','like',"%{$search}%")
					->orWhere('created_at','like',"%{$search}%");
				})
				->orWhere('user_scope', 'driver-passenger-service')
				->where(function($q) use ($search){
					$q->where('first_name', 'like', "%{$search}%")
	                ->orWhere('last_name', 'like', "%{$search}%")
					->orWhere('isdCode','like',"%{$search}%")
					->orWhere('mobile','like',"%{$search}%")
					->orWhere('created_at','like',"%{$search}%");
				})
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($drivers as $key => $driver) {
				$passengerRequests = PassengerRequest::where('driver_id',$driver->id)->get();
				$driver->totalRide=count($passengerRequests);
				$totalEarn=null;
				foreach ($passengerRequests as $key => $passengerRequest) {
					if ($passengerRequest->request_status=='Completed') {
						$bidRequest = BidRequest::where('passenger_request_id',$passengerRequest->passenger_request_id)->where('driver_id',$passengerRequest->driver_id)->first();
						if (isset($bidRequest)) {
							$totalEarn = $totalEarn+$bidRequest->bid_cost;
						}
					}
					$driver->totalEarn = $totalEarn;
				}
			}
            $totalFiltered =  User::where('user_scope', 'driver-service')
            	->where(function($q) use ($search){
	            	$q->where('first_name', 'like', "%{$search}%")
	                ->orWhere('last_name', 'like', "%{$search}%")
	                ->orWhere('isdCode','like',"%{$search}%")
	                ->orWhere('mobile','like',"%{$search}%")
	                ->orWhere('created_at','like',"%{$search}%");
            	})
            	->orWhere('user_scope', 'driver-passenger-service')
            	->where(function($q) use ($search){
	            	$q->where('first_name', 'like', "%{$search}%")
	                ->orWhere('last_name', 'like', "%{$search}%")
	                ->orWhere('isdCode','like',"%{$search}%")
	                ->orWhere('mobile','like',"%{$search}%")
	                ->orWhere('created_at','like',"%{$search}%");
            	})
				->count();
		}

		$data = array();
		
		if($drivers){
			foreach($drivers as $driver){
                $nestedData['driver_name']     = $driver->first_name." ".$driver->last_name;
				$nestedData['mobileno']   = $driver->isdCode.'-'.$driver->mobile;
				if(isset($driver->driver_profile)){
					if($driver->driver_profile->status == "approved"){
						$nestedData['status'] = '<span class="tag tag-success">'.$driver->driver_profile->status.'</span>';
					}elseif($driver->driver_profile->status == "banned"){
						$nestedData['status'] = '<span class="tag tag-danger">'.$driver->driver_profile->status.'</span>';
					}else{
						$nestedData['status'] = '<span class="tag tag-info">'.$driver->driver_profile->status.'</span>';
					}
				}else{
					$nestedData['status']   = null;
				}
				$nestedData['total_rides']  = $driver->totalRide;
				if (isset($driver->totalEarn)) {
					$nestedData['total_earn']  = '$'.$driver->totalEarn;
				}else{
					$nestedData['total_earn']  = '$0.00';
				}
				$nestedData['join_at']  = $driver->created_at->diffForHumans();
                $nestedData['action'] = '<a href="details/'.$driver->id.'" class="btn btn-info">View by Ride</a>
					<a href="detailsbybid/'.$driver->id.'" class="btn btn-info">View by Bid</a>';
               
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
			"draw"			  => intval($request->input('draw')),
			"recordsTotal"	  => intval($totalDrivers),
			"recordsFiltered" => intval($totalFiltered),
			"data"			  => $data
		);
		
		echo json_encode($json_data);
	}
	

	public function driverStatementDetails($id)
	{
		$driver = User::find($id);
		$rides = PassengerRequest::where('driver_id',$id)->get();
		//$totalEarn = null;
		$cancel_count=null;
		$percent=null;
		if (count($rides)>0) {
		foreach ($rides as $key => $ride) {
			if ($ride->request_status == 'Completed') {
				//$totalEarn = $totalEarn+$ride->earn;
				$bidRequest = BidRequest::where('passenger_request_id',$ride->passenger_request_id)->where('driver_id',$ride->driver_id)->first();
				if (isset($bidRequest)) {
					$ride->earn = $bidRequest->bid_cost;
				}
			}
			
			if ($ride->request_status == 'Cancel_By_Driver'||$ride->request_status == 'Cancel_By_Passenger') {
				$cancel_count = $cancel_count+1;
			}
			$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
			
			foreach ($waypoints as $key => $waypoint) {
				if ($waypoint->waypoint_type=='source') {
					$ride->source_address = $waypoint->address;
				}
				if ($waypoint->waypoint_type=='destination') {
					$ride->destination_address = $waypoint->address;
				}
			}
		}
		$percent = ($cancel_count*100)/count($rides);
	}
		return view('admin.statements.driver-statement-details',compact('rides','cancel_count','percent','driver'));
	}


	public function driverStatementDetailsByBid($id)
	{
		$driver = User::find($id);
		$details_byBids = DB::select('SELECT br.passenger_request_id, br.bid_cost,br.status, br.created_at as bid_created_at, rq.* FROM bid_request br left join passenger_request rq on rq.passenger_request_id=br.passenger_request_id where br.status in("Bid_Done","Bid_Accepted_by_passenger") and br.driver_id=?',[$id]);

		if (count($details_byBids)>0) {
			foreach ($details_byBids as $key => $details_byBid) {
				$waypoints=WaypointRequest::where('passenger_request_id', $details_byBid->passenger_request_id)->get();
			
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$details_byBid->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$details_byBid->destination_address = $waypoint->address;
					}
				}
			}
		}
		
		return view('admin.statements.driver-statement-biddetails',compact('details_byBids','driver'));
	}


	public function statement1($type = 'statement')
	{
	  try{
			$page = 'Ride Statement';

            if($type == 'statement'){
                $page = 'Drivers Overall Ride Statement';
            }elseif($type == 'today'){
                $page = 'Today Statement - '. date('d M Y');
            }elseif($type == 'monthly'){
                $page = 'This Month Statement - '. date('F');
            }elseif($type == 'yearly'){
                $page = 'This Year Statement - '. date('Y');
            }elseif($type == 'weekly'){
                $page = 'This Week Statement - '. Carbon::now()->startOfWeek()->format('d M Y');
            }
          
		 if($type == 'statement'){
			
                $rides = PassengerRequest::all();

            }elseif($type == 'today'){

            	$rides = PassengerRequest::where('created_at', '>=', Carbon::today())->get();

            }elseif($type == 'monthly'){

                $rides = PassengerRequest::whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)
         			->get();

            }elseif($type == 'weekly'){

                $rides = PassengerRequest::where('created_at', '>=', Carbon::now()->startOfWeek())->get();

            }elseif($type == 'yearly'){

                $rides = PassengerRequest::whereYear('created_at', date('Y'))->get();
            }
        //$totalEarn = null;
		$count=null;
		$percent = null;
		if (count($rides)>0) {
		foreach ($rides as $key => $ride) {
			if ($ride->request_status == 'Completed') {
				//$totalEarn = $totalEarn+$ride->earn;
				$bidRequest = BidRequest::where('passenger_request_id',$ride->passenger_request_id)->where('driver_id',$ride->driver_id)->first();
				if (isset($bidRequest)) {
					$ride->earn = $bidRequest->bid_cost;
					//$totalEarn = $totalEarn+$bidRequest->bid_cost;
				}
			}
			
			if ($ride->request_status == 'Cancel_By_Driver'||$ride->request_status == 'Cancel_By_Passenger') {
				$count = $count+1;
			}
			$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
			
			foreach ($waypoints as $key => $waypoint) {
				if ($waypoint->waypoint_type=='source') {
					$ride->source_address = $waypoint->address;
				}
				if ($waypoint->waypoint_type=='destination') {
					$ride->destination_address = $waypoint->address;
				}
			}
		}
		$percent = ($count*100)/count($rides);
	  }
		return view('admin.statements.statement',compact('rides','count','percent','page','type'));

	 } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
      }

	}


	public function statement($type)
	{
		$totalRidesObj = PassengerRequest::leftjoin('bid_request', 'passenger_request.request_bid_id', '=', 'bid_request.bid_request_id');
        $totalCancelRidesObj = PassengerRequest::leftjoin('bid_request', 'passenger_request.request_bid_id', '=', 'bid_request.bid_request_id')
        	->where('passenger_request.request_status','Cancel_By_Driver')
        	->orWhere('passenger_request.request_status','Cancel_By_Passenger');
		
		if($type == 'overall'){
            $page = 'Driver\'s Overall Ride Statement';
			$totalRides = $totalRidesObj->count();
			$totalCancelRides = $totalCancelRidesObj->count();
	
        }elseif($type == 'today'){
     
            $page = 'Today Statement - '. date('d M Y');
            $totalRides = $totalRidesObj->whereDate('passenger_request.created_at', Carbon::today())->count();
           
            $totalCancelRides = $totalRidesObj->where('passenger_request.request_status','Cancel_By_Driver')->orWhere('passenger_request.request_status','Cancel_By_Passenger')->whereDate('passenger_request.created_at', Carbon::today())
				->count();

        }elseif($type == 'weekly'){

            $page = 'This Week Statement - '. Carbon::now()->startOfWeek()->format('d M Y');
            $totalRides = $totalRidesObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
            
			$totalCancelRides = $totalRidesObj->where('passenger_request.request_status','Cancel_By_Driver')->orWhere('passenger_request.request_status','Cancel_By_Passenger')->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

        }elseif($type == 'monthly'){

            $page = 'This Month Statement - '. date('F');
            $totalRides = $totalRidesObj->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)->count();
			$totalCancelRides = $totalRidesObj->where('passenger_request.request_status','Cancel_By_Driver')->orWhere('passenger_request.request_status','Cancel_By_Passenger')->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)->count();

        }elseif($type == 'yearly'){

        	$page = 'This Year Statement - '. date('Y');
        	$totalRides = $totalRidesObj->whereYear('passenger_request.created_at', date('Y'))->count();
			$totalCancelRides = $totalRidesObj->where('passenger_request.request_status','Cancel_By_Driver')->orWhere('passenger_request.request_status','Cancel_By_Passenger')->whereYear('passenger_request.created_at', date('Y'))->count();
        
        }
        if($totalRides!=0){
        	$percentage = round(($totalCancelRides*100)/$totalRides,2);
        } else {
        	$percentage = null;
        }

		return view('admin.statements.statement',compact('page','totalRides','totalCancelRides','percentage','type'));
	}


	public function ajaxStatement(Request $request)
    {
    	$type = $request->type;
      
		$columns = array(
            0 => 'passenger_request_id',
			1 => 'driver_name',
			2 => 'passenger_name',
			3 => 'source_address',
			4 => 'destination_address',
			5 => 'created_at',
			6 => 'request_status',
			7 => 'earn',
			8 => 'action'
		);

		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir   = $request->input('order.0.dir');

		$totalRidesObj = PassengerRequest::leftjoin('bid_request', 'passenger_request.request_bid_id', '=', 'bid_request.bid_request_id');
		$ridesObj = PassengerRequest::leftjoin('bid_request', 'passenger_request.request_bid_id', '=', 'bid_request.bid_request_id')
		 		->leftjoin('users as user_d', 'passenger_request.driver_id', '=', 'user_d.id')
				->leftjoin('users as user_p', 'passenger_request.passenger_id', '=', 'user_p.id')
			 	->select('passenger_request.*', 'bid_request.bid_cost as bid_cost','bid_request.driver_id as bid_driver_id','bid_request.passenger_request_id as bid_passenger_request_id','bid_request.bid_request_id as bid_request_id','user_d.first_name as d_first_name','user_d.last_name as d_last_name','user_p.first_name as p_first_name','user_p.last_name as p_last_name');
		$totalFilteredSearchObj = PassengerRequest::leftjoin('bid_request', 'passenger_request.request_bid_id', '=', 'bid_request.bid_request_id')
		 		->leftjoin('users as user_d', 'passenger_request.driver_id', '=', 'user_d.id')
				->leftjoin('users as user_p', 'passenger_request.passenger_id', '=', 'user_p.id');
		
		if ($type == 'overall') {

        $totalRides = $totalRidesObj->count();
    
		if(empty($request->input('search.value'))){
			$rides = $ridesObj->offset($start)->limit($limit)->orderBy($order,$dir)
				->get();
			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}
			$totalFiltered = $totalRidesObj->count();
		}else{
			
			$search = $request->input('search.value');
            $rides = $ridesObj->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
					->orWhere('passenger_request.request_status', 'like', "%{$search}%")
					->orWhere('passenger_request.created_at','like',"%{$search}%")
					->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}

            $totalFiltered = $totalFilteredSearchObj->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
	                ->orWhere('passenger_request.request_status', 'like', "%{$search}%")
	                ->orWhere('passenger_request.created_at','like',"%{$search}%")
	                ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})
				->count();
		}

	} elseif($type == 'today') {

		$totalRides = $totalRidesObj->whereDate('passenger_request.created_at', Carbon::today())->count();
    	
		if(empty($request->input('search.value'))){
			$rides = $ridesObj->whereDate('passenger_request.created_at', Carbon::today())
		 		->offset($start)
				->limit($limit)
				->orderBy($order,$dir)
				->get();
			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}
			$totalFiltered = $totalRidesObj->whereDate('passenger_request.created_at', Carbon::today())->count();
		}else{
			
			$search = $request->input('search.value');
            $rides = $ridesObj->whereDate('passenger_request.created_at', Carbon::today())
			 	->where(function($q) use ($search){
                    $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
				 	->orWhere('passenger_request.request_status', 'like', "%{$search}%")
					->orWhere('passenger_request.created_at','like',"%{$search}%")
                    ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
                })
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}

            $totalFiltered = $totalFilteredSearchObj->whereDate('passenger_request.created_at', Carbon::today())
				->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
	                ->orWhere('passenger_request.request_status', 'like', "%{$search}%")
	                ->orWhere('passenger_request.created_at','like',"%{$search}%")
	                ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})->count();
		}


	} elseif($type == 'weekly') {

		$totalRides = $totalRidesObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
    	
		if(empty($request->input('search.value'))){
			$rides = $ridesObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
		 		->offset($start)
				->limit($limit)
				->orderBy($order,$dir)
				->get();
			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}
			$totalFiltered = $totalRidesObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
		}else{
			
			$search = $request->input('search.value');
            $rides = $ridesObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
			 	->where(function($q) use ($search){
                    $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
				 	->orWhere('passenger_request.request_status', 'like', "%{$search}%")
					->orWhere('passenger_request.created_at','like',"%{$search}%")
                    ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
                })
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}

            $totalFiltered = $totalFilteredSearchObj->whereBetween('passenger_request.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
				->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
	                ->orWhere('passenger_request.request_status', 'like', "%{$search}%")
	                ->orWhere('passenger_request.created_at','like',"%{$search}%")
	                ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})->count();
		}


	} elseif($type == 'monthly') {

		$totalRides = $totalRidesObj->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)
        	->count();
    	
		if(empty($request->input('search.value'))){
			$rides = $ridesObj->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)
		 		->offset($start)
				->limit($limit)
				->orderBy($order,$dir)
				->get();
			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}
			$totalFiltered = $totalRidesObj->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)
        		->count();
		}else{
			
			$search = $request->input('search.value');
            $rides = $ridesObj->whereMonth('passenger_request.created_at', Carbon::now()->month)
			 	->whereYear('passenger_request.created_at', Carbon::now()->year)
			 	
			 	->where(function($q) use ($search){
                    $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
				 	->orWhere('passenger_request.request_status', 'like', "%{$search}%")
					->orWhere('passenger_request.created_at','like',"%{$search}%")
                    ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
                })
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}

            $totalFiltered = $totalFilteredSearchObj->whereMonth('passenger_request.created_at', Carbon::now()->month)->whereYear('passenger_request.created_at', Carbon::now()->year)
				->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
	                ->orWhere('passenger_request.request_status', 'like', "%{$search}%")
	                ->orWhere('passenger_request.created_at','like',"%{$search}%")
	                ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})
				->count();
		}

	} elseif($type == 'yearly') {

		$totalRides = $totalRidesObj->whereYear('passenger_request.created_at', date('Y'))->count();
    	
		if(empty($request->input('search.value'))){
			$rides = $ridesObj->whereYear('passenger_request.created_at', date('Y'))
		 		->offset($start)
				->limit($limit)
				->orderBy($order,$dir)
				->get();
			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}
			$totalFiltered = $totalRidesObj->whereYear('passenger_request.created_at', date('Y'))->count();
		}else{
			
			$search = $request->input('search.value');
            $rides = $ridesObj->whereYear('passenger_request.created_at', date('Y'))
			 	->where(function($q) use ($search){
                    $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
				 	->orWhere('passenger_request.request_status', 'like', "%{$search}%")
					->orWhere('passenger_request.created_at','like',"%{$search}%")
                    ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
                })
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			foreach ($rides as $key => $ride) {
				$waypoints=WaypointRequest::where('passenger_request_id', $ride->passenger_request_id)->get();
				foreach ($waypoints as $key => $waypoint) {
					if ($waypoint->waypoint_type=='source') {
						$ride->source_address = $waypoint->address;
					}
					if ($waypoint->waypoint_type=='destination') {
						$ride->destination_address = $waypoint->address;
					}
				}
			}

            $totalFiltered = $totalFilteredSearchObj->whereYear('passenger_request.created_at', date('Y'))
				->where(function($q) use ($search){
	                $q->where('passenger_request.passenger_request_id', 'like', "%{$search}%")
	                ->orWhere('passenger_request.request_status', 'like', "%{$search}%")
	                ->orWhere('passenger_request.created_at','like',"%{$search}%")
	                ->orWhere('user_d.first_name','like',"%{$search}%")
					->orWhere('user_d.last_name','like',"%{$search}%")
					->orWhere('user_p.first_name','like',"%{$search}%")
					->orWhere('user_p.last_name','like',"%{$search}%")
					->orWhere('bid_request.bid_cost','like',"%{$search}%");
				})->count();
		}

	} //end yearly

		$data = array();
		
		if($rides){
			foreach($rides as $r){
                $nestedData['passenger_request_id']     = $r->passenger_request_id;
				$nestedData['driver_name']   = $r->d_first_name." ".$r->d_last_name;
				$nestedData['passenger_name']   = $r->p_first_name." ".$r->p_last_name;
				$nestedData['source_address']  = $r->source_address;
				$nestedData['destination_address']  = $r->destination_address;
				$nestedData['created_at']  = date('d M Y',strtotime($r->created_at));
				if($r->request_status == "Completed") {
					$nestedData['request_status'] = '<span class="tag tag-success">'.$r->request_status.'</span>';
				} else if($r->request_status == "Cancel_By_Driver"){
					$nestedData['request_status'] = '<span class="tag tag-danger">Cancel By Driver</span>';
				} else if($r->request_status == "Cancel_By_Passenger"){
					$nestedData['request_status'] = '<span class="tag tag-danger">Cancel By Passenger</span>';
				} else if($r->request_status == "Request_Confirm"){
					$nestedData['request_status'] = '<span class="tag tag-info">Request Confirm</span>';
				} else {
					$nestedData['request_status'] = '<span class="tag tag-info">'.$r->request_status.'</span>';
				}
				
				if($r->request_status == "Completed"){
					if(isset($r->bid_cost)){
						$nestedData['earn']  = '$'.$r->bid_cost;
					} else {
						$nestedData['earn']  = '$0.00';
					}
				} else {
					$nestedData['earn']  = '$0.00';
				}

				if($r->request_status == "Cancel_By_Driver"||$r->request_status == "Cancel_By_Passenger"){
					$nestedData['action'] = '<span>No Details Found </span>';
				} else {
					$nestedData['action'] = '<a class="text-primary" href="details/'.$r->passenger_request_id.'/'.$type.'"><span class="underline">View Ride Details</span></a>';
				}

				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
			"draw"			  => intval($request->input('draw')),
			"recordsTotal"	  => intval($totalRides),
			"recordsFiltered" => intval($totalFiltered),
			"data"			  => $data,
			// "page"			  => $page,
			// "totalData"		  => $totalData,
			// "totalCancelData" => $totalCancelData,
			// "percentage"      => $percentage
		);

		echo json_encode($json_data);
    }


    public function statement_today(){
        return $this->statement('today');
    }

    public function statement_weekly(){
        return $this->statement('weekly');
    }

    public function statement_monthly(){
        return $this->statement('monthly');
    }

    public function statement_yearly(){
        return $this->statement('yearly');
    }

}

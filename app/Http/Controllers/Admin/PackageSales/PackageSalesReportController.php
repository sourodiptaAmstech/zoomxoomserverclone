<?php

namespace App\Http\Controllers\Admin\PackageSales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Package\CreditPackageLog;
use App\Model\Transaction\CreditTransactionLog;

class PackageSalesReportController extends Controller
{
	public function packageSalesReport()
	{
		$getPackages=DB::select('SELECT COUNT(ctl.credit_transaction_log_id) as quantity, sum(ctl.transaction_cost) as totalCost, cpl.name  FROM credit_transaction_log ctl left join credit_package_log cpl on cpl.credit_package_log_id=ctl.credit_package_log_id where ctl.transaction_type="credit" and ctl.transaction_status="Completed" and ctl.refer_transaction_id IS NULL and ctl.bid_id IS NULL GROUP by cpl.name');

		$totalQuantity = null;
		foreach ($getPackages as $key => $getPackage) {
			$package = CreditPackageLog::where('name',$getPackage->name)->first();
			$getPackage->credit_package_log_id = $package->credit_package_log_id;
			$totalQuantity += $getPackage->quantity;
		}
		
		return view('admin.packagesale.index', compact('getPackages','totalQuantity'));
	}

	public function searchPackageSalesReport(Request $request)
	{
		$from_date = $request->from_date;
		$to_date = $request->to_date;

		$getPackages = DB::select('SELECT COUNT(ctl.credit_transaction_log_id) as quantity, sum(ctl.transaction_cost) as totalCost, cpl.name  FROM credit_transaction_log ctl left join credit_package_log cpl on cpl.credit_package_log_id=ctl.credit_package_log_id where ctl.transaction_type="credit" and ctl.transaction_status="Completed" and ctl.refer_transaction_id IS NULL and ctl.bid_id IS NULL and Date(ctl.created_at) BETWEEN ? and ? GROUP by cpl.name',[$from_date,$to_date]);
		
		$totalQuantity = null;
		$totalEarn = null;
		foreach ($getPackages as $key => $getPackage) {
			$package = CreditPackageLog::where('name',$getPackage->name)->first();
			$getPackage->credit_package_log_id=$package->credit_package_log_id;
			$totalEarn=$totalEarn+$getPackage->totalCost;
			$totalQuantity += $getPackage->quantity;
		}
		return response()->json([
            "success" => true,
            "message" => "Package sales report",
            "errors" => array("exception"=>["Everything is OK."]),
            'getPackages' => $getPackages,
            'totalQuantity' => $totalQuantity,
            'totalEarn' => $totalEarn,
            'from_date' => $from_date,
            'to_date' => $to_date
        ],201);

	}

	public function packageSalesReportDetails($id)
	{
		$package=CreditPackageLog::find($id);
		$pname=$package->name;
		$transactions=CreditTransactionLog::where('credit_package_log_id',$package->credit_package_log_id)->where('transaction_status','Completed')->where('transaction_type','credit')->where('bid_id', null)->where('refer_transaction_id', null)->get();
		return view('admin.packagesale.details',compact('transactions','pname'));
	}

	public function packageSalesReportDetailsBySearch($id,$from_date,$to_date)
	{
		$package=CreditPackageLog::find($id);
		$pname=$package->name;
		$transactions=CreditTransactionLog::whereBetween('created_at', [$from_date,$to_date])->where('credit_package_log_id',$package->credit_package_log_id)->where('transaction_status','Completed')->where('transaction_type','credit')->where('bid_id', null)->where('refer_transaction_id', null)->get();
		return view('admin.packagesale.details',compact('transactions','pname'));
	}

}

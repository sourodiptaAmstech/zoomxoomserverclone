<?php

namespace App\Http\Controllers\Admin\Vehicle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;



class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $vehicles = Vehicle::orderBy('created_at' , 'desc')->paginate(10);
        // return view('admin.vehicle.index', compact('vehicles'));
        return view('admin.vehicle.index');
    }

    public function getVehicles(Request $request)
    {
		$columns = array(
            0 => 'id',
			1 => 'year',
			2 => 'make',
			3 => 'model',
			4 => 'action'
		);
		
        $totalData = Vehicle::count();
		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');
		
		if(empty($request->input('search.value'))){
			$posts = Vehicle::offset($start)
					->limit($limit)
					->orderBy($order,$dir)
					->get();
			$totalFiltered = Vehicle::count();
		}else{
			$search = $request->input('search.value');
            $posts = Vehicle::where('id', 'like', "%{$search}%")
                            ->orWhere('year', 'like', "%{$search}%")
							->orWhere('make','like',"%{$search}%")
							->orWhere('model','like',"%{$search}%")
							->offset($start)
							->limit($limit)
							->orderBy($order, $dir)
							->get();
            $totalFiltered = Vehicle::where('id', 'like', "%{$search}%")
                            ->orWhere('year', 'like', "%{$search}%")
                            ->orWhere('make','like',"%{$search}%")
                            ->orWhere('model','like',"%{$search}%")
							->count();
		}		

		$data = array();
		
		if($posts){
			foreach($posts as $r){
                $nestedData['id']     = $r->id;
				$nestedData['year']   = $r->year;
				$nestedData['make']   = $r->make;
				$nestedData['model']  = $r->model;
                $nestedData['action'] = '<a href="vehicle/'.$r->id.'/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                <button class="btn btn-danger" onclick="return confirm("Are you sure?")"><i class="fa fa-trash"></i> Delete</button>';
               
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
			"draw"			  => intval($request->input('draw')),
			"recordsTotal"	  => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"			  => $data
		);
		
		echo json_encode($json_data);
    }
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vehicle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'year'  => 'required|integer',
            'make'  => 'required',
            'model' => 'required',
        ]);

        try{

            Vehicle::create($request->all());
            return redirect()->route('admin.vehicle.index')->with('flash_success','Vehicle Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Vehicle Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $vehicle = Vehicle::findOrFail($id);
            return view('admin.vehicle.edit',compact('vehicle'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'year'  => 'required|integer',
            'make'  => 'required',
            'model' => 'required',
        ]);

        try {
            Vehicle::where('id',$id)->update([
                    'year' => $request->year,
                    'make' => $request->make,
                    'model' => $request->model,
                ]);
            return redirect()->route('admin.vehicle.index')->with('flash_success', 'Vehicle Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Vehicle Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Vehicle::find($id)->delete();
            return back()->with('message', 'Vehicle deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Vehicle Not Found');
        }
    }
}

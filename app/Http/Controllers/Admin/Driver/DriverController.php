<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Driver\DriverProfiles;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
use App\Model\Driver\DriverServices;
use App\Model\Vehicle;
use App\Model\ServiceType;
use App\Model\Driver\DriverDocuments;
use App\Model\Document;
use App\Model\Driver\DriverCarImages;
use App\Http\Controllers\Api\Transaction\TransactionController;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotification;
use App\Model\Driver\DriverDocumentReason;
use App\Model\Driver\DriverReason;
use App\Model\DeleteUser;


class DriverController extends Controller
{
   
    public function index()
    {
         return view('admin.driver.index');
    }

    public function ajaxDriver(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            //2 => 'last_name',
            2 => 'email',
            3 => 'isdCode',
        );
        
        $totalData =  User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))){
            $drivers = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            $totalFiltered = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->count();
        }else{
            $search = $request->input('search.value');
            $drivers = User::where('user_scope', 'driver-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->orWhere('user_scope', 'driver-passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = User::where('user_scope', 'driver-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->orWhere('user_scope', 'driver-passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->count();
        }       

        $data = array();

        if($drivers){
            foreach($drivers as $d){
                if (isset($d->driver_profile)) {
                    if($d->driver_profile->status == "onboarding" || $d->driver_profile->status == "banned"){
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-danger driver-status" data-toggle="tooltip" data-value="Activate" data-id="'.$d->id.'" title="Click to Active">Inactive</a>';
                    }else{
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-success driver-status" data-toggle="tooltip" data-value="Inactivate" data-id="'.$d->id.'" title="Click to Inactive">Active</a>';
                    }
                }else{
                    $activationButton = null;
                }

                $nestedData['id']     = $d->id;
                $nestedData['driver_name']   = $d->first_name." ".$d->last_name;
                $nestedData['email']   = $d->email;
                $nestedData['mobile']  = $d->isdCode.'-'.$d->mobile;
                if(isset($d->driver_profile)){
                    $nestedData['rating'] = $d->driver_profile->rating;
                }else{
                    $nestedData['rating'] = null;
                }
                $nestedData['action'] = '<span style="line-height: 33px;"><a href="driver/'.$d->id.'/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                    <a href="drivers/document/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Document</a>
                    <a href="driver/carimages/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Car Images</a>
                    <a href="driver/transaction/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Package</a>
                    '.$activationButton.'</span>&nbsp;<a href="driver/delete/'.$d->id.'" class="btn btn-danger d-delete"><i class="fa fa-trash"></i> Delete</a>';

                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        
        echo json_encode($json_data);
    }


    public function deleteDriver($id)
    {
        $driver = User::find($id);
        
        if ($driver->user_scope == "driver-service") {
            $deleteUser = new DeleteUser;
            $deleteUser->user_id = $id;
            $deleteUser->isdCode = $driver->isdCode;
            $deleteUser->mobile = $driver->mobile;
            $deleteUser->email = $driver->email;
            $deleteUser->user_scope = $driver->user_scope;
            $deleteUser->save();

            $driver->isdCode = Date('Y-m-d H:i:s');
            $driver->mobile = Date('Y-m-d H:i:s');
            $driver->email = Date('Y-m-d H:i:s');
            $driver->user_scope = 'deleted-driver-service';
            $driver->save();
        }
        if ($driver->user_scope == "driver-passenger-service") {
            $deleteUser = new DeleteUser;
            $deleteUser->user_id = $id;
            $deleteUser->isdCode = $driver->isdCode;
            $deleteUser->mobile = $driver->mobile;
            $deleteUser->email = $driver->email;
            $deleteUser->user_scope = 'driver-service';
            $deleteUser->save();

            $driver->user_scope = 'passenger-service';
            $driver->save();
        }
        return redirect()->back()->with('flash_success', 'Driver deleted successfully.');
    }

  
    public function create()
    {
        return view('admin.driver.create');
    }

    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'first_name'        => 'required|max:255',
                'last_name'         => 'required|max:255',
                'email'             => 'required|email|max:255|unique:users',
                'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'password'          => 'required|confirmed|min:6',
                'car_plate_number'  => 'required',
                'car_number_expire_date' => 'required',
                'mobile'            => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:users'
            ]);

            if (empty($request->mobile)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password=bcrypt(trim($request->password));
            $User->user_scope="driver-service";
            $User->email=$request->email;
            $User->first_name=$request->first_name;
            $User->last_name=$request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/profile');
                $picture = str_replace("public", "storage", $picture);
                $User->picture=$pflash_erroricture;
            }
            $User->mobile=$request->mobile;
            $User->isdCode=$request->code;
            $User->save();

            $DriverProfiles=new DriverProfiles();
            $DriverProfiles->user_id=$User->id;
            $DriverProfiles->save();

            $DriverServices=new DriverServices();
            $DriverServices->user_id=$User->id;
            $DriverServices->car_model=$request->car_type;
            $DriverServices->car_number_expire_date=$request->car_number_expire_date;
            //$DriverServices->car_make_year=$request->car_make_year;
            $DriverServices->car_make=$request->car_make;
            $DriverServices->car_plate_number=$request->car_plate_number;
            $DriverServices->save();
           
            if(!empty($User)){

                if($User->id>0){
                    return redirect()->back()->with('flash_success', 'Thank you for registering with us!');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }

    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $driver = User::find($id);
            return view('admin.driver.edit',compact('driver'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

   
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,email,'.$id,
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'car_plate_number'  => 'required',
            'car_number_expire_date' => 'required',
            'mobile'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:users,mobile,'.$id
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $driver = User::find($id);

            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/'.$id.'/profile');
                $picture = str_replace("public", "storage", $picture);
                $driver->picture=$picture;
            }

            $driver->first_name    = $request->first_name;
            $driver->last_name     = $request->last_name;
            $driver->email         = $request->email;
            $driver->mobile        = $request->mobile;
            $driver->isdCode       = $request->code;
            $driver->save();

            $DriverServices = DriverServices::where('user_id', $id)->first();
            $DriverServices->car_model=$request->car_type;
            $DriverServices->car_number_expire_date=$request->car_number_expire_date;
            $DriverServices->car_make=$request->car_make;
            $DriverServices->car_plate_number=$request->car_plate_number;
            $DriverServices->save();

            return redirect()->route('admin.driver.index')->with('flash_success', 'Driver Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Driver Not Found');
        }
    }

    public function listDriverDocument($id)
    {
        $driver_documents = DriverDocuments::where('user_id', $id)->get();
        if ($driver_documents->count()>0) {
            return view('admin.driver.document', compact('driver_documents'));
        } else {
            return redirect()->back()->with('flash_error', 'Document not found!');
        }
    }

    public function driverDocumentVerification(Request $request,$id)
    {
        $user = User::find($request->user_id);
        $document = DriverDocuments::where('id', $request->document_id)->first();
      
        if ($request->status == 'ACTIVE') {
            $document->status = $request->status;
        }
        if ($request->status == 'INVALID') {
            $document->status = $request->status;
            //send email
            $msg="Your document ".$document->document->name." has been invalided by admin. The reason for invalidation is that ".$request->reason;
            $sub = 'Document Invalid Notification';
            Notification::send($user, new EmailNotification($msg,$sub));
            //send sms
            $bdmsg = "Your document ".$document->document->name." has been invalided by admin.";
            $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
        }
        if ($request->status == 'EXPIRE') {
            $document->status = $request->status;
            //send email
            $msg="Your document ".$document->document->name." has been expired by admin. The reason for expiration is that ".$request->reason;
            $sub = 'Document Expire Notification';
            Notification::send($user, new EmailNotification($msg,$sub));
            //send sms
            $bdmsg = "Your document ".$document->document->name." has been expired by admin.";
            $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
        }
        $document->save();

        return response()->json([
            'success' => true,
            'message' => 'Document verified successfully'
        ], 200);

    }

    public function driverActivationProcess($id)
    {
        $user = User::find($id);
        if ($user->driver_profile->status == 'approved') {
            $driver_profile = DriverProfiles::where('user_id', $id)->get();
            $driver_profile[0]->status = 'banned';
            $driver_profile[0]->save();
            // //send email
            // $msg="Your driver account has been Inactivated by admin.";
            // $sub = 'ZoomXoom Driver InActivation Notification';
            // Notification::send($user, new EmailNotification($msg,$sub));
            //send sms
            $bdmsg = 'Your ZoomXoom driver account has been Inactivated by admin.';
            $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
            return redirect()->back()->with('flash_success', 'Driver is inactivated successfully');
        } else {

        if ($user->isMobileVerified != 0) {
            $driver_service = DriverServices::where('user_id', $id)->first();
            if ($driver_service != null) {
                    if ($driver_service->car_plate_number != null) {
                        if ($driver_service->car_number_expire_date > date('Y-m-d')) {
                            $documents = Document::where('status', 1)->get();
                            
                            if ($documents->count()>0) {

                              foreach ($documents as $key => $document) {
                                $document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->first();
                                if ($document==null) {
                                    return redirect()->back()->with('flash_error', 'Not allow to activate driver! All the documents not uploaded. Please upload all the documents!');
                                }
                              }

                              foreach ($documents as $key => $document) {
                                $active_document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->where('status','ACTIVE')->first();
                                if ($active_document==null) {
                                    return redirect()->back()->with('flash_error', 'Uploaded documents are not active document. Not allow to activate driver!');
                                }
                              }

                              $driver_profile = DriverProfiles::where('user_id', $id)->get();
                              if ($driver_profile->count()>0) {
                                    $driver_profile[0]->status = 'approved';
                                    $driver_profile[0]->save();
                                //send email
                                $msg="Welcome! Your driver account is activated successfully.";
                                $sub = 'ZoomXoom Driver Activation Notification';
                                Notification::send($user, new EmailNotification($msg,$sub));
                                //send sms
                                $bdmsg = 'Welcome! Your ZoomXoom driver account is activated successfully.';
                                $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);

                                    return redirect()->back()->with('flash_success', 'Driver is activated successfully');
                              } else {
                                return redirect()->back()->with('flash_error', 'Driver profile not found. Not allow to activate driver!');
                              }

                            } else {
                                return redirect()->back()->with('flash_error', 'Document not found. Not allow to activate driver!');
                            }
                            
                        } else {
                            return redirect()->back()->with('flash_error', 'Driver\'s car number has expired. Not allow to activate driver!');
                        }
                    }else{
                        return redirect()->back()->with('flash_error', 'Car plate number not found! Not allow to activate driver');
                    }
            } else {
                return redirect()->back()->with('flash_error', 'Driver service not found! Not allow to activate driver');
            }
          
        } else {
            return redirect()->back()->with('flash_error', 'Mobile is not verified! Not allow to activate driver');
        }
      }

    }
   
    
    public function driverCarImages($id)
    {
        $service = DriverServices::where('user_id', $id)->first();
        if (!empty($service)) {
            $carimages = DriverCarImages::where('service_id', $service->service_id)->get();
            if ($carimages->count()>0) {
                return view('admin.driver.carimage', compact('carimages'));
            } else {
                return redirect()->back()->with('flash_error', 'Driver car images not found!');
            }
        } else {
            return redirect()->back()->with('flash_error', 'Driver service not found!');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function driverTransaction($id)
    {
        $transaction_obj = new TransactionController;
        $transaction = $transaction_obj->getPackagesBrought($id);
        if($transaction['statusCode'] == 200){
            return view('admin.driver.package', compact('transaction'));
        } elseif ($transaction['statusCode'] == 500) {
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        } elseif ($transaction['statusCode'] == 400) {
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        } else {
            return redirect()->back()->with('flash_error', 'Your not authorized to access!');
        }
    }

    public function twilioMobileNotification($isdCode,$mobile,$bdmsg)
    {
        $twilio_id="AC8a3e5f8b2adfaa3f1af9cbeb265c06a7";
        $twilio_token="073e05c75c6f5211fa3fbe961ddba752";
        $authBase64=base64_encode($twilio_id.":".$twilio_token);
        $twilio_ph_no="+14159656861";
        //curl of twillo
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twilio.com/2010-04-01/Accounts/$twilio_id/Messages.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_USERPWD=>"$twilio_id:$twilio_token",
            CURLOPT_HTTPAUTH=>CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => "Body=$bdmsg&From=$twilio_ph_no&To=$isdCode.$mobile",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic $authBase64"
            ),
        ));
        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);
        $msg="OTP sent successfully!";
        if($httpcode!==201){
            $msg= $response['message'];
        }
        return ["message"=>$msg,"status"=>$httpcode];
    }

    public function documentVerificationReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $reason = DriverDocumentReason::where('driver_id',$request->user_id)->where('document_id',$request->id)->first();
        if(empty($reason))
        {
            $reason = new DriverDocumentReason;
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
            }
            $reason->save();
        } else {
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
                $reason->reason_for_expire = null;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
                $reason->reason_for_invalid = null;
            }
            $reason->save();
        }

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>["Everything is OK."]),
            "reason" => $request->reason
        ],201);
    }

    public function driverInactiveBannedReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $user = User::find($request->id);

        $reason = DriverReason::where('driver_id',$request->id)->first();

        if(empty($reason))
        {
            $reason = new DriverReason;
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        } else {
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        }

        //send email
        $msg="Your driver account has been Inactivated by admin. The reason for inactivation is that ".$request->reason;
        $sub = 'ZoomXoom Driver InActivation Notification';
        Notification::send($user, new EmailNotification($msg,$sub));

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>["Everything is OK."])
        ],201);
    }
    
}

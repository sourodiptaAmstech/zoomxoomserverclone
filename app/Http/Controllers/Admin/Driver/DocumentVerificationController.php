<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Document;
use App\Model\Driver\DriverDocuments;
use App\Model\Driver\DriverDocumentLog;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotification;
use App\Model\Email\EmailControl;
use App\Jobs\SendEmailJob;


class DocumentVerificationController extends Controller
{
   public function allDocumentCheck($id, $isEmail=true)
   {
    $user = User::find($id);
    $documents = Document::where('status', 1)->get();
    foreach ($documents as $key => $document) {
        $document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->first();
        if(empty($document)){
            $driver_log = DriverDocumentLog::where('user_id', $id)->first();
            if(empty($driver_log)){
                $driver_log_obj = new DriverDocumentLog;
                $driver_log_obj->user_id = $id;
                $driver_log_obj->is_all_doc_uploaded = 0;
                $driver_log_obj->is_email_fail_to_admin = 1;
                $driver_log_obj->is_email_fail_to_admin_date = date('Y-m-d H:i:s');
                $driver_log_obj->is_notify_email_driver = 1;
                $driver_log_obj->is_notify_email_driver_date = date('Y-m-d H:i:s');
                $driver_log_obj->save();
            } else {
                $driver_log->is_all_doc_uploaded = 0;
                $driver_log->is_email_fail_to_admin = 1;
                $driver_log->is_email_fail_to_admin_date = date('Y-m-d H:i:s');
                $driver_log->is_notify_email_driver = 1;
                $driver_log->is_notify_email_driver_date = date('Y-m-d H:i:s');
                $driver_log->save();
            }
            //send email
            if (isset($isEmail)) {
                $msg="Your profile is not completed, please complete your profile!";
                $sub = 'ZoomXoom Driver Profile Not Completed';
                Notification::send($user, new EmailNotification($msg,$sub));
            }

            return response(['message'=>"Driver profile not completed","data"=>[],"errors"=>array("exception"=>["Profile not completed"])],400);
        }
        
    }

    $driver_log = DriverDocumentLog::where('user_id', $id)->first();
    if(empty($driver_log)){
        $driver_log_obj = new DriverDocumentLog;
        $driver_log_obj->user_id = $id;
        $driver_log_obj->is_all_doc_uploaded = 1;
        $driver_log_obj->is_email_fail_to_admin = 0;
        $driver_log_obj->is_email_fail_to_admin_date = null;
        $driver_log_obj->is_notify_email_driver = 0;
        $driver_log_obj->is_notify_email_driver_date = null;
        $driver_log_obj->save();

    } else {
        $driver_log->is_all_doc_uploaded = 1;
        $driver_log->is_email_fail_to_admin = 0;
        $driver_log->is_email_fail_to_admin_date = null;
        $driver_log->is_notify_email_driver = 0;
        $driver_log->is_notify_email_driver_date = null;
        $driver_log->save();
    }

    $msg="New driver ".$user->first_name." ".$user->last_name." is registered successfully, please verify all the documents";
    $this->emailToAdmin($msg);
    return response(['message'=>"Driver profile completed successfully","data"=>[],"errors"=>array("exception"=>["Everything is OK."])],201);

   }

   public function emailToAdmin($msg)
   {
        $email_control = EmailControl::where('template_name','document verification')->first();
        $emails = json_decode($email_control->to_address);
        foreach ($emails as $email) {
            $admin = User::where('email',$email)->first();
            $sub = $email_control->subject;
            Notification::send($admin, new EmailNotification($msg,$sub));
        }
    }

    public function cronjobToMailDriver()
    {
        $drivers = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->get();
        foreach ($drivers as $key => $driver) {
            $driver_log = DriverDocumentLog::where('user_id',$driver->id)->where('is_all_doc_uploaded',0)->first();
            if (isset($driver_log)) {
                $notify_email_date = $driver_log->is_notify_email_driver_date;

                $date_after_5days = date('Y-m-d H:i:s', strtotime($notify_email_date.'+5 days'));
                $date_diff = $date_after_5days<=date('Y-m-d H:i:s');

                if($date_diff) {
                    $driver_log->is_notify_email_driver_date = date('Y-m-d H:i:s');
                    $driver_log->save();
                    //send email
                    $details['email'] = $driver->email;
                    dispatch(new SendEmailJob($details));
                }
            }
        }
        // return response()->json([
        //     "success" => true,
        //     "message" => "Cron job run successfully",
        //     "errors" => array("exception"=>["Everything is OK."])
        // ],201);
    }

}

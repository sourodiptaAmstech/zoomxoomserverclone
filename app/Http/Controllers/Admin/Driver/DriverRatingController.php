<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\PassengerRequest;
use App\Http\Controllers\Admin\Driver\DocumentVerificationController;
use URL;
use App\Model\Driver\DriverRatingAlert;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotification;
use App\User;
use App\Http\Controllers\Admin\Driver\DriverController;
use App\Model\Driver\DriverProfiles;
use App\Notifications\PushNotification;


class DriverRatingController extends Controller
{
    public function driverRatingReview(Request $request, $id)
    {
        $rating = PassengerRequest::where('driver_id', $id)->orderBy('created_at', 'desc')->take(7)->get();
        $last_id = $rating[6]->passenger_request_id;
        $first_id = $rating[0]->passenger_request_id;
        $totalRating = $rating[0]->driver_rating+$rating[1]->driver_rating+$rating[2]->driver_rating+$rating[3]->driver_rating+$rating[4]->driver_rating+$rating[5]->driver_rating+$rating[6]->driver_rating;
        $avg_rating = $totalRating/7;
        if ($avg_rating <= 3) {
            $email_obj = new DocumentVerificationController;
            $url = URL::route('admin.driver.rating-list',[$id,$last_id,$first_id]);
            $msg = "Check driver average rating: ".$url;
            $email_obj->emailToAdmin($msg);
        }
        return response()->json([
            "success" => true,
            "message" => "Driver rating review",
            "errors" => array("exception"=>["Everything is OK."])
        ],201);
    }

    public function ratingList($id,$last_id,$first_id)
    {
        $ratings = PassengerRequest::whereBetween('passenger_request_id', array($last_id,$first_id))->where('driver_id', $id)->orderBy('created_at', 'desc')->take(7)->get();
        $totalRating = $ratings[0]->driver_rating+$ratings[1]->driver_rating+$ratings[2]->driver_rating+$ratings[3]->driver_rating+$ratings[4]->driver_rating+$ratings[5]->driver_rating+$ratings[6]->driver_rating;
        $avg_rating = $totalRating/7;
        return view('admin.driver.rating', compact('ratings','avg_rating', 'last_id', 'first_id'));
    }

    public function searchRating(Request $request,$ids)
    {
        $id = $request->id;
        if ($request->value == 'last7rides') {
            $rate = 7;
        }
        if ($request->value == 'last10rides') {
            $rate = 10;
        }
        if ($request->value == 'last15rides') {
            $rate = 15;
        }
        if ($request->value == 'last20rides') {
            $rate = 20;
        }
        
        $ratings = PassengerRequest::where('driver_id', $id)->orderBy('created_at', 'desc')->take($rate)->get();
        $totalRating = 0;
        foreach ($ratings as $key => $rating) {
            $totalRating = $totalRating + $rating->driver_rating;
        }
        $avg_rating = round($totalRating/$rate,2);
        
        return response()->json([
            "success" => true,
            "message" => "Driver rating review",
            "errors" => array("exception"=>["Everything is OK."]),
            "ratings" => $ratings,
            "avg_rating" => $avg_rating
        ],201);
    }

    public function driverRatingAlert(Request $request,$ids)
    {
        $user = User::find($request->id);
        $this->validate($request, [
            'email_body'    => 'required',
        ]);

        $rating_obj = new DriverRatingAlert;
        $rating_obj->driver_id = $request->id;
        $rating_obj->email_body = $request->email_body;
        $rating_obj->date_time = date('Y-m-d H:i:s');
        $rating_obj->avg_rating = $request->avg_rating;
        $rating_obj->index_value =  json_encode(array(7,$request->last_id,$request->first_id));
        $rating_obj->save();
        //send sms
        $bdmsg = "Warning alert-You have repeat 7 consecutive low rating from customer";
        $driver_obj = new DriverController;
        $driver_obj->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
        //send email
        $msg = $request->email_body;
        $sub = 'Rating Alert Notification';
        Notification::send($user, new EmailNotification($msg,$sub));

        // $driver = DriverProfiles::where('user_id', $request->id)->get();
        // $FCMTokens = [];
        // $PushNotification = "";
        // $pushnotificationsData = [];
        // if(count($driver)>0){
        //     foreach($driver as $key=>$val){
        //         if($val->device_type=="android"){
        //             $FCMTokens['Android'][]=$val->device_token;
        //         }
        //         if($val->device_type=="ios"){
        //             $FCMTokens['IOS'][]=$val->device_token;
        //         }
        //     }
        //     $pushnotificationsData['to'] = $FCMTokens;
        //     $pushnotificationsData['notification'] = array(
        //         'title' => 'RATING ALERT',
        //         'text' => '',
        //         'click_action' => '',
        //         'body'=>'You have repeat seven consecutive low rating from driver.'
        //     );
        //     $pushnotificationsData['data'] = [
        //         "type"=>"RATINGALERT"
        //     ];
        //     $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
        // }
        
        return response()->json([
            "success" => true,
            "message" => "Successfully done rating alert to the driver",
            "errors" => array("exception"=>["Everything is OK."]),
        ],201);

    }

}

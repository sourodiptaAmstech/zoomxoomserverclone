<?php

namespace App\Http\Controllers\Admin\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Passenger\PassengersProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
use App\Http\Controllers\Api\Passenger\RegisterController;
// use Storage;
use App\Model\DeleteUser;


class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.passenger.index');
    }

    public function ajaxPassenger(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            //2 => 'last_name',
            2 => 'email',
            3 => 'isdCode',
        );
        
        $totalData =  User::where('user_scope', 'passenger-service')->orWhere('user_scope', 'driver-passenger-service')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))){
            $passengers = User::where('user_scope', 'passenger-service')->orWhere('user_scope', 'driver-passenger-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            $totalFiltered = User::where('user_scope', 'passenger-service')->orWhere('user_scope', 'driver-passenger-service')->count();
        }else{
            $search = $request->input('search.value');
            $passengers = User::where('user_scope', 'passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->orWhere('user_scope', 'driver-passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = User::where('user_scope', 'passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->orWhere('user_scope', 'driver-passenger-service')
                ->where(function($q) use ($search){
                    $q->where('id', 'like', "%{$search}%")
                    ->orWhere('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%")
                    ->orWhere('isdCode','like',"%{$search}%")
                    ->orWhere('mobile','like',"%{$search}%");
                })
                ->count();
        }       

        $data = array();

        if($passengers){
            foreach($passengers as $p){
                $nestedData['id']     = $p->id;
                $nestedData['passenger_name']   = $p->first_name." ".$p->last_name;
                $nestedData['email']   = $p->email;
                $nestedData['mobile']  = $p->isdCode.'-'.$p->mobile;
                if(isset($p->passenger_profile)){
                    $nestedData['rating'] = $p->passenger_profile->rating;
                }else{
                    $nestedData['rating'] = null;
                }
                $nestedData['action'] = '<a href="passenger/'.$p->id.'/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>&nbsp;<a href="passenger/delete/'.$p->id.'" class="btn btn-danger p-delete"><i class="fa fa-trash"></i> Delete</a>';

                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        
        echo json_encode($json_data);
    }


    public function deletePassenger($id)
    {
        $passenger = User::find($id);
        if ($passenger->user_scope == "passenger-service") {
            $deleteUser = new DeleteUser;
            $deleteUser->user_id = $id;
            $deleteUser->isdCode = $passenger->isdCode;
            $deleteUser->mobile = $passenger->mobile;
            $deleteUser->email = $passenger->email;
            $deleteUser->user_scope = $passenger->user_scope;
            $deleteUser->save();

            $passenger->isdCode = Date('Y-m-d H:i:s');
            $passenger->mobile = Date('Y-m-d H:i:s');
            $passenger->email = Date('Y-m-d H:i:s');
            $passenger->user_scope = 'deleted-passenger-service';
            $passenger->save();
        }
        if ($passenger->user_scope == "driver-passenger-service") {
            $deleteUser = new DeleteUser;
            $deleteUser->user_id = $id;
            $deleteUser->isdCode = $passenger->isdCode;
            $deleteUser->mobile = $passenger->mobile;
            $deleteUser->email = $passenger->email;
            $deleteUser->user_scope = 'passenger-service';
            $deleteUser->save();

            $passenger->user_scope = 'driver-service';
            $passenger->save();
        }
        return redirect()->back()->with('flash_success', 'Passenger deleted successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.passenger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{
            $this->validate($request, [
                'first_name'        => 'required|max:255',
                'last_name'         => 'required|max:255',
                'email'             => 'required|email|max:255|unique:users',
                'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'password'          => 'required|confirmed|min:6',
                'mobile'            => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:users'
            ]);
            if (empty($request->mobile)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password = bcrypt(trim($request->password));
            $User->user_scope = "passenger-service";
            $User->email = $request->email;
            $User->first_name = $request->first_name;
            $User->last_name = $request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/passenger/profile');
                $picture = str_replace("public", "storage", $picture);
                $User->picture = $picture;
            }
            $User->mobile = $request->mobile;
            $User->isdCode = $request->code;
            $User->save();

            // create the user's profile
            $PassengersProfile = new PassengersProfile();
            $PassengersProfile->user_id = $User->id;
            $PassengersProfile->company_name = $request->company_name;
            $PassengersProfile->payment_mode = "CASH";
            $PassengersProfile->save();
           
            if(!empty($User)){

                if($User->id>0){
                    return redirect()->back()->with('flash_success', 'Thank you for registering with us!');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $passenger = User::find($id);
            return view('admin.passenger.edit',compact('passenger'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,email,'.$id,
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:users,mobile,'.$id
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $passenger = User::find($id);

            if(isset($request->picture) && !empty($request->picture)){
                //$Storage=Storage::delete($passenger->picture);
                $picture = $request->picture->store('public/passenger/'.$id.'/profile');
                $picture = str_replace("public", "storage", $picture);
                $passenger->picture = $picture;
            }

            $passenger->first_name    = $request->first_name;
            $passenger->last_name     = $request->last_name;
            $passenger->email         = $request->email;
            $passenger->mobile        = $request->mobile;
            $passenger->isdCode       = $request->code;
            $passenger->save();

            $passenger_profile = PassengersProfile::where('user_id', $id)->first();
            if (isset($passenger_profile)) {
                $passenger_profile->company_name = $request->company_name;
                $passenger_profile->save();
            }

            return redirect()->route('admin.passenger.index')->with('flash_success', 'Passenger Updated Successfully');    
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Passenger Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin\Email;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Email\EmailControl;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;


class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emails = EmailControl::orderBy('created_at', 'desc')->get();
        return view('admin.email.index', compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $email = EmailControl::findOrFail($id);
            return view('admin.email.edit', compact('email'));
        }
        catch (ModelNotFoundException $e){
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if (isset($request->to_address)) {
                $email_array = array_unique($request->to_address);
            } else {
                $email_array = null;
            }
            
            $email = EmailControl::find($id);
            $email->template_name = $request->template_name;
            $email->subject       = $request->subject;
            $email->to_address    = $email_array;
            $email->save();
            return redirect()->route('admin.email-control.index')->with('flash_success', 'Email Control Updated Successfully');
        }
        catch (Exception $e){
            return back()->with('flash_error', 'Email Control Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

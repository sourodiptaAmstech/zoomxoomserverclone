<?php

namespace App\Http\Controllers\Admin\ReportIssue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ReportIssue\ReportIssue;

class ReportIssueController extends Controller
{
    public function reportedByDriver(Request $request)
    {
        $bydrivers = ReportIssue::where('reportedby','driver')->get();
        $title= 'Issues Reported By Driver';
        return view('admin.reportissue.index', compact('bydrivers','title'));
    }

    public function reportedByPassenger(Request $request)
    {
    	$bypassengers = ReportIssue::where('reportedby','passenger')->get();
    	$title= 'Issues Reported By Passenger';
        return view('admin.reportissue.index', compact('bypassengers','title'));
    }
    public function reportIssueDetails($id,$param)
    {
    	$reportIssue = ReportIssue::find($id);
    	return view('admin.reportissue.details', compact('reportIssue','param'));
    }
}

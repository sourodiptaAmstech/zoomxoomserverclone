<?php

namespace App\Http\Controllers\Api\ChangePassword;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;


class ChangePasswordController extends Controller
{
	public function changePassword(Request $request)
	{
        try {
        	$rule=['old_password' => 'required','password' => 'required|min:6|confirmed'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }

	        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {

	        	return response(['message'=>'Your old password does not matches with the password you provided. Please try again',"errors"=>array("exception"=>["Invalid credentials"])],422);
	        }
	        if(strcmp($request->get('old_password'), $request->get('password')) == 0){

	        	return response(['message'=>'New Password cannot be same as your old password. Please choose a different password',"errors"=>array("exception"=>["Invalid credentials"])],422);
	        }
	     
	        $user = Auth::user();
	        $user->password = bcrypt($request->get('password'));
	        $user->save();

	        return response(['message'=>'Password Updated',"errors"=>array("exception"=>["Everything is OK."])],201);
	    }
	    catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }

	}
    
}

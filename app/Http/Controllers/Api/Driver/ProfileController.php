<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use App\Model\Driver\DriverDocuments;
use App\Model\Driver\DriverProfiles;
use App\Model\Driver\DriverServices;
use App\Model\Document;
use App\Model\ServiceType;
use App\Http\Resources\Stripe;
use App\Model\UserCard;


class ProfileController extends Controller
{
    public function AddCard(Request $request){
        try{
            $rule=['stripe_token' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };


            $Stripe=new Stripe();
            // check for stripe customer id
            $DriverProfiles=DriverProfiles::where("user_id",Auth::user()->id)->first();

            if($DriverProfiles->stripe_cust_id==null || $DriverProfiles->stripe_cust_id=="" ){
                // create customer in stripe
                $StripeReturn=$Stripe->createStripeAccount(Auth::user()->email);
                if($StripeReturn['status']==true && $StripeReturn['Exception']==false){
                    $DriverProfiles->stripe_cust_id=$StripeReturn['stripe_cust_id'];
                    $DriverProfiles->save();
                 }
                 else{
                    return response(['message'=>$StripeReturn['message'],"data"=>[],"errors"=>array("exception"=>["Stripe Exception"],"e"=>$StripeReturn['e'])],500);
                 }
            }

            if($DriverProfiles->stripe_cust_id!==null && $DriverProfiles->stripe_cust_id!="" ){
                $card=$Stripe->createCard($DriverProfiles->stripe_cust_id,$request->stripe_token);
            }
            else{
                return response(['message'=>"Cannot able to add customer card","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
            }
            if($card['status']==false && $card['Exception']==true){
                return response(['message'=>$card['message'],"data"=>[],"errors"=>array("exception"=>["Stripe Exception"],"e"=>$card['e'])],400);
            }
            $exist=UserCard::where('user_id',Auth::user()->id)
                                ->where('last_four',$card['card']['last4'])
                                ->where('brand',$card['card']['brand'])
                                ->count();
            if($exist == 0){
                $create_card = new UserCard;
                $create_card->user_id = Auth::user()->id;
                $create_card->card_id = $card['card']['id'];
                $create_card->last_four = $card['card']['last4'];
                $create_card->brand = $card['card']['brand'];
                $create_card->is_default =0;
                $create_card->save();
            }else{
                return response(['message'=>"Card Already Added","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
            }

            // set default card
            $findCardAll =UserCard::where("user_id",Auth::user()->id)->get()->toArray();
            if(count($findCardAll)===1){
                $create_card->is_default =1;
                $create_card->save();
            }
            return response(['message'=>"Card Added","data"=>$this->CardData($create_card),"errors"=>array("exception"=>["Card Added"])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }

    public function ListCard(Request $request){
        try{
            $cards=UserCard::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get()->toArray();
            if(!empty($cards))
            return response(['message'=>"Card data send","data"=>(array)$cards,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Cards not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function DeleteCard(Request $request){
        try{
            $rule=['card_id' => 'required|exists:user_cards,card_id,user_id,'.Auth::user()->id];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            $DriverProfiles=DriverProfiles::where("user_id",Auth::user()->id)->first();
            $Stripe=new Stripe();
            $card=$Stripe->deleteCard($DriverProfiles->stripe_cust_id,$request->card_id);
            UserCard::where('card_id',$request->card_id)->delete();
            return response(['message'=>"Card Deleted","data"=>[],"errors"=>array("exception"=>["Card Deleted"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }
    public function setDefaultCard(Request $request){
        try{
            $rule=['card_id' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
              $UserCardUnSetDefault=DB::table('user_cards')->where('user_id', Auth::user()->id)->update(array('is_default' => 0));
              $UserCardSetDefault=DB::table('user_cards')->where('card_id', $request->card_id)->update(array('is_default' => 1));
              // card list
            //  $cards=UserCard::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get()->toArray();
             // return response(['message'=>"Default card updated","data"=>(array)$cards,"errors"=>array("exception"=>["Everything is OK."])],200);

              return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

    public function ProfileCompletion(){
        try{

            $arrayOfRequired=['mobile','picture','isMobileVerified'];
            $total=3;
            $profileFillUp=0;

            $User=User::find(Auth::user()->id);
           // echo Auth::user()->id; exit;
            if($User->mobile!="" && $User->mobile!=null && $User->mobile!="null" && $User->mobile!="na" && $User->mobile!="NA")
            $profileFillUp+=1;

            if($User->picture!="" && $User->picture!=null && $User->picture!="null" && $User->picture!="na" && $User->picture!="NA")
            $profileFillUp+=1;

            if($User->isMobileVerified==1)
            $profileFillUp+=1;

            $DriverDocument=DriverDocuments::where("user_id",$User->id)->get()->toArray();
            $profileFillUp+=count($DriverDocument);

            $Profile=DriverProfiles::where("user_id",$User->id)->get()->toArray();

            $DriverService=DriverServices::where("user_id",$User->id)->get()->toArray();
            $total +=1;
            if(count($DriverService)>0)
            $profileFillUp +=1;

            $Document=Document::where("deleted_at",null)->where("status",1)->get()->toArray();
            $total+=count($Document);

            $percentage=($profileFillUp*100)/$total;
            //return $percentage;
            return response(['message'=>"Profile completion percentage","data"=>["percentage"=>$percentage,"profileFillUp"=>$profileFillUp,"total"=>$total],"errors"=>array("exception"=>["Profile completion percentage."])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }

    public function getProfile(Request $request){
        try{
            $User=User::find(Auth::user()->id);
            $DriverProfile=DriverProfiles::where("user_id",$User->id)->firstOrFail();
            $DriverService=DriverServices::where("user_id",$User->id)->firstOrFail();
            $ServiceType=new ServiceType();
            //if($DriverService->service_type_id!=null && $DriverService->service_type_id!=0 && $DriverService->service_type_id!=""){
           //     $ServiceType=ServiceType::where("id",$DriverService->service_type_id)->firstOrFail();
           // }

           // print_r($DriverProfile);exit;
            return response(['message'=>"Profile data send","data"=>["user_data"=>$this->setuserDataDriver($User),
                "profile_data"=>$this->setDriverProfileData($DriverProfile),
                "service_data"=>$this->setDriverServiceData($DriverService,$ServiceType)],"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }
    public function profileEdit(Request $request){
        try{ // 'mobile' => 'required|unique:drivers',
            $rule=[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'car_number_expire_date' => 'required',
            'car_plate_number' => 'required',
            'car_model' => 'required',
            'car_make' => 'required'];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };

            $user_id=Auth::user()->id;

            $User=User::findOrFail($user_id);
            $User->first_name=$request->first_name;
            $User->last_name=$request->last_name;
            $User->Save();

         /*   $DriverProfiles=DriverProfiles::where("user_id",$user_id)->firstOrFail();
            $DriverProfiles->user_id=$User->id;
            $DriverProfiles->device_id=$request->device_id;
            $DriverProfiles->device_token=$request->device_token;
            $DriverProfiles->device_type=$request->device_type;
            $DriverProfiles->save(); */



            $DriverServices=DriverServices::where("user_id",$user_id)->firstOrFail();
            $DriverServices->car_number_expire_date=$request->car_number_expire_date;
            $DriverServices->car_model=$request->car_model;
            $DriverServices->car_make=$request->car_make;
            $DriverServices->car_plate_number=$request->car_plate_number;
            $DriverServices->save();

            return response(['message'=>"Your profile is been updated","data"=>[],"errors"=>array("exception"=>["Resoures Created"])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }

}

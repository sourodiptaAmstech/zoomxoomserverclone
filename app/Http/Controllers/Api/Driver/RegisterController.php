<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Driver\DriverProfiles;
use App\Model\Driver\DriverServices;
use Storage;

class RegisterController extends Controller
{
    //
    public function registration(Request $request){
        try{ // 'mobile' => 'required|unique:drivers',
            $rule=['device_id' => 'required','device_type' => 'required|in:android,ios','device_token' => 'required',
            'first_name' => 'required|max:255','last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users','password' => 'required|min:6|confirmed',
          //  'service_type_id' => 'required',//,'car_number' => 'required',
            'car_number_expire_date' => 'required','car_plate_number' => 'required',
            'car_model' => 'required',
            //'car_make_year' => 'required',
                'car_make' => 'required'];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };


            $User=new User();
            $User->email=$request->email;
            $User->password=bcrypt(trim($request->password));
            $User->user_scope="driver-service";
            $User->login_by="manual";
            $User->first_name=$request->first_name;
            $User->last_name=$request->last_name;
            $User->user_scope="driver-service";

            $User->Save();

            $DriverProfiles=new DriverProfiles();
            $DriverProfiles->user_id=$User->id;
            $DriverProfiles->device_id=$request->device_id;
            $DriverProfiles->device_token=$request->device_token;
            $DriverProfiles->device_type=$request->device_type;
            $DriverProfiles->save();



            $DriverServices=new DriverServices();
            $DriverServices->user_id=$User->id;
           // $DriverServices->service_type_id=0;
            //$DriverServices->car_number=$request->car_number;
            $DriverServices->car_number_expire_date=$request->car_number_expire_date;
            $DriverServices->car_model=$request->car_model;
            $DriverServices->car_make_year=$request->car_make_year;
            $DriverServices->car_make=$request->car_make;
            $DriverServices->car_plate_number=$request->car_plate_number;
            $DriverServices->save();

            if(!empty($User)){
                if($User->id>0){
                    return response(['message'=>"Thank you for registering with us!","data"=>(object)["user_id"=>$User->id],"errors"=>array("exception"=>["Resoures Created"])],201);
                 }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }

    }
    public function passengerReegisterAsDriver(Request $request){
        try{
            $rule=['device_id' => 'required','device_type' => 'required|in:android,ios','device_token' => 'required',
            //'service_type_id' => 'required',
            'car_number_expire_date' => 'required',
            'car_plate_number' => 'required',
            'car_model' => 'required',
            'car_make' => 'required'];



            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };



            $User=User::findOrFail(Auth::user()->id);
            $User->user_scope="driver-passenger-service";
            $User->Save();
            Auth::user()->user_scope=$User->user_scope;

            $DriverProfiles=new DriverProfiles();
            $DriverProfiles->user_id=$User->id;
            $DriverProfiles->device_id=$request->device_id;
            $DriverProfiles->device_token=$request->device_token;
            $DriverProfiles->device_type=$request->device_type;
            $DriverProfiles->save();



            $DriverServices=new DriverServices();
            $DriverServices->user_id=$User->id;
         //   $DriverServices->service_type_id=$request->service_type_id;
            //$DriverServices->car_number=$request->car_number;
            $DriverServices->car_number_expire_date=$request->car_number_expire_date;
            $DriverServices->car_model=$request->car_model;
            $DriverServices->car_make_year=$request->car_make_year;
            $DriverServices->car_make=$request->car_make;
            $DriverServices->car_plate_number=$request->car_plate_number;
            $DriverServices->save();

            if(!empty($User)){
                if($User->id>0){
                    return response(['message'=>"Thank you for registering with us!","data"=>(object)["user_id"=>$User->id],"errors"=>array("exception"=>["Resoures Created"])],201);
                 }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }
}

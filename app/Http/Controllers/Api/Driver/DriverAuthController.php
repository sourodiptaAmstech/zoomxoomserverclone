<?php

namespace App\Http\Controllers\Api\Driver;


use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ResetPasswordOTP;
use App\Model\Driver\DriverProfiles;
use Carbon\Carbon;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class DriverAuthController extends Controller
{
    //
    public function loginByID($user_id){
        try{
            if(!Auth::loginUsingId($user_id)){
                return ['message'=>'The email address or password you entered is incorrect',
                "errors"=>array("exception"=>["Invalid credentials"])];
            }
            $access_token=Auth::user()->createToken('ZoomXoomPersonalAccessClient',['driver-service'])->accessToken;

            return ['message'=>'Login successfully',"errors"=>(object)array(),'data'=>array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
                "user_data"=>$this->setuserDataDriver(Auth::user())
                )];
            }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])];
        }
    }

    public function login(Request $request){
        try{
            $rule=['email' => 'required|email|max:255', 'password'=>'required',
            'device_id' => 'required','device_type' => 'required|in:android,ios','device_token' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422); };

            if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                return response(['message'=>'The email address or password you entered is incorrect',
                "errors"=>array("exception"=>["Invalid credentials"])],401);
            }
            $access_token=Auth::user()->createToken('ZoomXoomPersonalAccessClient',['driver-service'])->accessToken;

            if(Auth::user()->user_scope!=="passenger-service"){
                $DriverProfile=DriverProfiles::where("user_id",Auth::user()->id)->first();
                $DriverProfile->device_token=$request->device_token;
                $DriverProfile->device_id=$request->device_id;
                $DriverProfile->device_type=$request->device_type;
                $DriverProfile->save();
            }


            //,DriverProfiles::where("user_id",Auth::user()->id)->firstOrFail()

            return response(['message'=>'Login successfully',
            "errors"=>(object)array(),
            'data'=>array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
                "user_data"=>$this->setuserDataDriver(Auth::user())
                )],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'The email address or password you entered is incorrect',
            "errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
     /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */


    public function forgot_password(Request $request){
        try{
            $rule=['email' => 'required|email',];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $Driver = User::where('email' , $request->email)->firstOrFail();
            $otp = mt_rand(100000, 999999);
            $Driver->otp = $otp;
            $Driver->save();
            Notification::send($Driver, new ResetPasswordOTP($otp));
            return response(['message'=>"OTP sent to your email!","data"=>(object)[],"errors"=>array("exception"=>["Invalid validation"])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }


    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */
    public function  reset_password(Request $request){
        try{
            $rule=['email' => 'required|email','password' => 'required|confirmed|min:6','otp'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }

            $User = User::where('email' , $request->email)->firstOrFail();

            if((int)$User->otp!==(int)$request->otp){
                return response(['message'=>"Invalid Otp","field"=>"otp",
                "errors"=>array("exception"=>["Request Validation Failed"])],403);
            }
            $User->password = bcrypt($request->password);
            $User->save();
            return response(['message'=>"You have reset your password successfully. Now you can login to your account.","data"=>(object)[],"errors"=>array("exception"=>["Password Reset!"])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }

    }

    public function logout(Request $request){
       // SELECT * FROM `oauth_access_tokens` WHERE `scopes` LIKE '%driver-service%'
       try{
           $userID=Auth::user()->id;
           DB::table('oauth_access_tokens')->where('scopes', 'LIKE', '%driver-service%')->where("user_id",$userID)->delete();
           DB::table('drivers_profile')->where('user_id', $userID)->update(['device_token' => '','device_id'=>'']);
           return response(['message'=>"You have logout successfully.","data"=>(object)[],"errors"=>array("exception"=>["Logout!"])],200);
                      
       }
       catch(\Illuminate\Database\QueryException  $e){
           return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"e"=>$e)],403);
        }
    }
}

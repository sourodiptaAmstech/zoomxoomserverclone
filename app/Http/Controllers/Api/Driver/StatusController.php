<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use App\Model\Driver\DriverProfiles;
use App\Model\Driver\DriverServices;
use App\Model\Document;
use App\Model\Driver\DriverDocuments;
use App\Model\ServiceType;

class StatusController extends Controller
{
    //
    private function checkDriverStatus(){
        try{

            $user_id=Auth::user()->id;
            $status="onboarding";
            $DriverProfiles=DriverProfiles::where("user_id",$user_id)->get()->toArray();
            $DriverServices=DriverServices::where("user_id",$user_id)->get()->toArray();
          //  'onboarding', 'approved', 'banned
          if(count($DriverProfiles)>0){
               if($DriverProfiles[0]['status']==="onboarding" || $DriverProfiles[0]['status']==="banned"){
                   $status=$DriverProfiles[0]['status'];
                }
                else if($DriverProfiles[0]['status']==="approved"){
                    $status=$DriverProfiles[0]['status'];
                }
            }
            //'active', 'offline', 'riding'
            if(count($DriverServices)>0 && $status=="approved"){
                $status=$DriverServices[0]['status'];
            }
            return $status;
         }
         catch(\Illuminate\Database\QueryException  $e){
             return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
         }
         catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
             return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
         }
         catch(ModelNotFoundException $e)
         {
             return response(['message'=>"Your account yet to be approved!.","data"=>(object)[],"errors"=>array("exception"=>["Account is not approved"])],403);
         }

    }
    public function DocumentCompletion(){
        try{
            $total=0;
            $profileFillUp=0;

            $User=User::find(Auth::user()->id);
            $DriverDocument=DriverDocuments::where("user_id",$User->id)->get()->toArray();
            $profileFillUp+=count($DriverDocument);

            $Document=Document::where("deleted_at",null)->where("status",1)->get()->toArray();
            $total+=count($Document);
            $percentage=($profileFillUp*100)/$total;
            $status=$this->checkDriverStatus();


           // $User=User::find(Auth::user()->id);
            $DriverProfile=DriverProfiles::where("user_id",$User->id)->firstOrFail();
            $DriverService=DriverServices::where("user_id",$User->id)->firstOrFail();
            $ServiceType=new ServiceType();
            //if($DriverService->service_type_id!=null && $DriverService->service_type_id!=0 && $DriverService->service_type_id!=""){
           //     $ServiceType=ServiceType::where("id",$DriverService->service_type_id)->firstOrFail();
           // }

           // print_r($DriverProfile);exit;

            return response(['message'=>"Profile completion percentage","data"=>[
                "percentage"=>$percentage,
                "profileFillUp"=>$profileFillUp,
                "total"=>$total,
                "status"=>$status,
                "user_data"=>$this->setuserDataDriver($User),
                "profile_data"=>$this->setDriverProfileData($DriverProfile),
                "service_data"=>$this->setDriverServiceData($DriverService,$ServiceType)
            ],"errors"=>array("exception"=>["Profile completion percentage."])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }
}

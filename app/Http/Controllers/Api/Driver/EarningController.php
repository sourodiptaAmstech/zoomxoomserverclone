<?php


namespace App\Http\Controllers\Api\Driver;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Request\BidRequest;
use App\Http\Controllers\Api\Transaction\TransactionController;
use App\Model\Request\PassengerRequest;
use App\Http\Controllers\Api\Driver\PackageController;
use App\Model\Request\WaypointRequest;
use Illuminate\Support\Facades\DB;
use App\Model\Passenger\PassengersProfile;
use App\Model\Driver\DriverProfiles;

class EarningController extends Controller
{
  
    public function myEarning(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required',"sortBy"=>"required"];
            $validator=$this->requestValidation($request->all(),$rule);
            $totalEarning=[];
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            if($request->sortBy=="all"){
                $totalEarning=DB::select('select count(pr.passenger_request_id) as totalTrip, COALESCE(sum(br.bid_cost),0) as totalCost 
                from passenger_request pr 
                left join bid_request br on br.bid_request_id=pr.request_bid_id
                where pr.request_status=? and pr.driver_id=?', ['Completed',Auth::user()->id]);
                $requestBooking=PassengerRequest::where("driver_id",Auth::user()->id)->where("request_status","Completed")->orderBy('passenger_request_id', 'desc')
                ->paginate(10)->toArray();   
              
            }
            else if($request->sortBy=="date"){
                $totalEarning=DB::select('select count(pr.passenger_request_id) as totalTrip, COALESCE(sum(br.bid_cost),0) as totalCost 
                from passenger_request pr 
                left join bid_request br on br.bid_request_id=pr.request_bid_id
                where pr.request_status=? and pr.driver_id=? and Date(pr.updated_at) BETWEEN ? and ?', ['Completed',Auth::user()->id, $request->fromDate,$request->toDate]);
               
               /* $requestBooking= DB::select('select * from passenger_request 
                where request_status=? and driver_id=? and Date(updated_at) BETWEEN ? and ? 
                order by passenger_request_id desc', ['Completed',Auth::user()->id, $request->toDate,$request->fromDate])
                ->paginate(10)->toArray();   */
                $requestBooking=PassengerRequest::where("driver_id",Auth::user()->id)
                ->where("request_status","Completed")
                ->whereBetween("updated_at",array($request->fromDate,$request->toDate))
                ->orderBy('passenger_request_id', 'desc')
                ->paginate(10)->toArray();   
                
            }
          
           
            $myBooking=[];         
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['scheduleDateTime']=$this->checkNull($requestBooking["data"][$key]['scheduleDateTime']);
                if($requestBooking["data"][$key]['scheduleDateTime']!=""){
                    $requestBooking["data"][$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking["data"][$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['updated_at']=$this->checkNull($requestBooking["data"][$key]['updated_at']);
                if($requestBooking["data"][$key]['updated_at']!=""){
                    $requestBooking["data"][$key]['updated_at']=$this->convertFromUTC($requestBooking["data"][$key]['updated_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['driver_remarks']=$this->checkNull($requestBooking["data"][$key]['driver_remarks']);
                $requestBooking["data"][$key]['requested_serviceType']=$this->checkNull($requestBooking["data"][$key]['requested_serviceType']);
                if($requestBooking["data"][$key]['requested_serviceType']==""){
                    $requestBooking["data"][$key]['requested_serviceType']=0;
                }
               // $requestBooking["data"][$key]['preference']=$this->checkNull($requestBooking["data"][$key]['preference']);
                
               
                //$wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
               // $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
              //  $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
              //  $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking["data"][$key]['request_bid_id'])->first()->toArray();
                $myBooking[$key]['bidDetails']['created_at']=$this->convertFromUTC($myBooking[$key]['bidDetails']['created_at'],$request->timeZone);
                $myBooking[$key]['bidDetails']['updated_at']=$this->convertFromUTC($myBooking[$key]['bidDetails']['updated_at'],$request->timeZone);

            }
            $requestBooking["data"]= $myBooking;
            $requestBooking["totalEarning"]=$totalEarning;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'No data!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
}

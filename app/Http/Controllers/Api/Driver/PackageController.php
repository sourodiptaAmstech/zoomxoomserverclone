<?php


namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Package\CreditPackageMst;
use App\Model\Package\CreditPackageLog;
use App\Model\UserCard;
use App\Model\Driver\DriverProfiles;
use App\Http\Controllers\Api\Transaction\TransactionController;

//use App\Model\





class PackageController extends Controller
{

    public function getPackages(Request $request){
        try{
            $packages=CreditPackageMst::all()->toArray();
            if(!empty($packages))
                return response(['message'=>"Packages data send","data"=>(array)$packages,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
                return response(['message'=>"Packages not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }

    public function buyPackages(Request $request){
        try{
            $rule=['credit_package_log_id' => 'required',"card_id"=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            $DriverProfiles=DriverProfiles::where("user_id",Auth::user()->id)->firstOrFail();

            if($DriverProfiles->stripe_cust_id==null|| $DriverProfiles->stripe_cust_id==""){
                return response(['message'=>'Please add your credit or debit card!',"errors"=>array("exception"=>["SItripe account not found"])],404);
            }

            $UserCard=UserCard::where("card_id",$request->card_id)->get()->toArray();
            if(empty($UserCard)){
                return response(['message'=>'Please add your credit or debit card!',"errors"=>array("exception"=>["Card Not found"])],404);
            }

            $CreditPackageLog=CreditPackageLog::findOrFail($request->credit_package_log_id);
            $stripeTrancationObjet=(object)[
                "credit_package_log_id"=>$CreditPackageLog->credit_package_log_id,
                "package_name"=>$CreditPackageLog->name,
                "package_cost"=>$CreditPackageLog->cost,
                "package_value"=>$CreditPackageLog->face_value,
                "description"=>"Tracation for the package; name: $CreditPackageLog->name  number: $CreditPackageLog->credit_package_log_id",
                "user_id"=>Auth::user()->id,
                "card_id"=>$request->card_id,
                "stripe_customer_id"=>$DriverProfiles->stripe_cust_id
            ];
            $TransactionController=new TransactionController();
           $Transaction= $TransactionController->buyPackage($stripeTrancationObjet);
           if($Transaction['statusCode']===201){
               return response(['message'=>"Transaction Successfully","data"=>[],"errors"=>array("exception"=>["Everything is OK."])],201);
           }
           else{
            return response(['message'=>"Transaction Successfully","data"=>[],"errors"=>array("exception"=>["Everything is OK."])],402);
           }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

    public function getBoughtPackages(Request $request){
        try{

            $user_id=Auth::user()->id;
            $Transaction=new TransactionController();
            $getPackages=$Transaction->getPackagesBrought($user_id);
            if($getPackages['statusCode']==200)
            return response(['message'=>"Transaction Log","data"=>$getPackages['data'],"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["From Transaction class"],"e"=>[])],500);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
    public function totalDebitCredit(Request $request){
        try{

            $user_id=Auth::user()->id;
            $Transaction=new TransactionController();
            $getPackages=$Transaction->totalDebitCredit($user_id);
            if($getPackages['statusCode']==200)
            return response(['message'=>"Total Debit Credit Log","data"=>$getPackages['data'],"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["From Transaction class"],"e"=>[])],500);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
    public function getPackagesByID($data){
        try{
            $CreditPackageLog=CreditPackageLog::where("credit_package_log_id",$data['credit_package_log_id'])->first()->toArray();
            return $CreditPackageLog;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
}

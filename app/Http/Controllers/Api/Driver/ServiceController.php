<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Driver\DriverServices;
use App\Model\Driver\DriverProfiles;

class ServiceController extends Controller
{
    //
    public function toggleOnlineStatus(Request $request){
        try{

           $user_id=Auth::user()->id;
           $rule=['status' => 'required|in:active,offline,riding'];
           $validator=$this->requestValidation($request->all(),$rule);
           if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
           $DriverProfiles=DriverProfiles::where("user_id",$user_id)->where("status","approved")->firstOrFail();
           $DriverServices=DriverServices::where("user_id",$user_id)->firstOrFail();
           $DriverServices->status=$request->status;
           $DriverServices->save();
           return response(['message'=>"Service status is updated","data"=>["service_status"=>$DriverServices->status],"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Your account yet to be approved!.","data"=>(object)[],"errors"=>array("exception"=>["Account is not approved"])],403);
        }
    }

}

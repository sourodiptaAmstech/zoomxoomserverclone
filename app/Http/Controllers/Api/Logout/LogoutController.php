<?php


namespace App\Http\Controllers\Api\Logout;


use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class LogoutController
 * @package App\Http\Controllers\Api\Logout
 */
class LogoutController extends Controller
{
    /**
     * @param Request $request
     */
    public function logout(Request $request){
        try{


return  false;

        }  catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'The email address or password you entered is incorrect',
                "errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

}

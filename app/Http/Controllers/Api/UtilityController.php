<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Vehicle;
use App\Model\ServiceTypes;
use App\Model\Documents;
use App\Model\Driver\Driver;
use App\Model\Driver\DriverDocuments;
use App\Model\ServiceType;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Exception;
use League\CommonMark\Block\Element\Document;
use Storage;


class UtilityController extends Controller
{
    public function Vehicle(Request $request){
        try{
            $vehicle=Vehicle::orderBy('year', 'DESC')->get()->toArray();
            if(!empty($vehicle))
            return response(['message'=>"Vehicle data send","data"=>(array)$vehicle,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Vehicle not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch (Exception $e) {
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Internal Server Error"])],500);
        }
    }
    public function getServiceType(Request $request){
        try{

            $serviceTypes=ServiceType::orderBy('name', 'DESC')->get()->toArray();
            if(!empty($serviceTypes))
            return response(['message'=>"Service data send","data"=>(array)$serviceTypes,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Service not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch (Exception $e) {
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Internal Server Error"])],500);
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $document_id
     * @return \Illuminate\Http\Response
     */
    public function uploadDocument(Request $request,$document_id){
        try{
            $id=Auth::user()->id;
            $rule=['file' => 'required|mimes:jpeg,bmp,png','document_side' => 'required|in:front,back'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $Document = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->get()->toArray();
            if(!empty($Document)){
                $DriverDocuments = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->firstOrFail();
                if(isset($request->file) && !empty($request->file && $request->document_side==="front")){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments->front_url=$picture;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
                if(isset($request->file) && !empty($request->file) && $request->document_side==="back"){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments->back_url=$picture;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }
            else{
                if(isset($request->file) && !empty($request->file) && $request->document_side==="back"){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments=new DriverDocuments;
                    $DriverDocuments->back_url=$picture;
                    $DriverDocuments->user_id=$id;
                    $DriverDocuments->document_id=$document_id;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
                if(isset($request->file) && !empty($request->file) && $request->document_side==="front"){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments=new DriverDocuments;
                    $DriverDocuments->front_url=$picture;
                    $DriverDocuments->user_id=$id;
                    $DriverDocuments->document_id=$document_id;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }
            return response(['message'=>"Document updated successfully","data"=>[],"errors"=>array("exception"=>["Document updated successfully."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function profileImageUpdate(Request $request){
        try{
            $rule=['picture' => 'required|mimes:jpeg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            $User = User::where("id",Auth::user()->id)->firstOrFail();
            if ($request->picture != "") {
                $Storage=Storage::delete($User->picture);
                $User->picture = $request->picture->store('public/users/'.Auth::user()->id.'/profile');
                $User->picture=str_replace("public", "storage", $User->picture);
            }
            $User->save();
            return response(['message'=>"Thank you for uploading your profile image!","data"=>(object)["picture"=>$User->picture],"errors"=>array("exception"=>["Resoures Created"])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }


    public function list(Request $request)
    {
        try{

            $id=Auth::user()->id;
            $VehicleDocuments = DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' .$id . ') AS driver_document_status'),
                        DB::raw('(select driver_documents.front_url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_front_url'),
                        DB::raw('(select driver_documents.back_url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_back_url'),
                        DB::raw('(select driver_documents.expires_at  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_expires_at')
                    )
            ->where('type', 'VEHICLE')->where('status', 1)
            ->get();
            $DriverDocuments =  DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_status'),
                        DB::raw('(select driver_documents.front_url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_front_url'),
                        DB::raw('(select driver_documents.back_url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_back_url'),
                        DB::raw('(select driver_documents.expires_at  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' .$id . ') AS driver_document_expires_at')

                    )
                    ->where('type', 'DRIVER')->where('status', 1)
                    ->get();

                    $VehicleImage = DB::table('documents')
                    ->select('documents.*',
                        DB::raw('(select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_status'),
                        DB::raw('(select driver_documents.front_url   from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . ') AS driver_document_url')
                    )
                    ->where('type', 'VEHICLE IMAGE')->where('status', 1)
                    ->get();
                    //       $driver = Auth::user();
                    $Documnets=[
                        'VehicleDocuments' => $VehicleDocuments,
                        'DriverDocuments' => $DriverDocuments,
                        'VehicleImage' => $VehicleImage,
                        // 'id' => Auth::user()->id
                    ];
                    return response(['message'=>"Vehicle data send","data"=>(array)$Documnets,"errors"=>array("exception"=>["Everything is OK."])],200);
                }
                catch(\Illuminate\Database\QueryException  $e){
                    return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
                }
                catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
                    return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
                }
                catch(ModelNotFoundException $e)
                {
                    return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
                }
            }

        }

<?php

namespace App\Http\Controllers\Api\Bid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Request\BidRequest;
use App\Http\Controllers\Api\Transaction\TransactionController;
use App\Model\Request\PassengerRequest;
use App\Http\Controllers\Api\Driver\PackageController;
use App\Model\Request\WaypointRequest;
use Illuminate\Support\Facades\DB;
use App\Model\Passenger\PassengersProfile;
use App\Model\Driver\DriverProfiles;


class BidController extends Controller
{
    //
    public function placeBid(Request $request){
        try{
            $rule=['request_id' => 'required','bid_cost' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            // find active request
            $PassengerRequest=PassengerRequest::where("passenger_request_id",$request->request_id)->where("request_status","Requested")->firstOrFail();
            // find the bit
            $BidRequest=BidRequest::where("passenger_request_id",$request->request_id)->where("driver_id",Auth::user()->id)->firstOrFail();
            if( $BidRequest->status=="Request_sent_for_bid"){
                $TransactionController= new TransactionController();
                $Transaction= $TransactionController->saveTheBid(["user_id"=>Auth::user()->id,"bid_request_id"=>$BidRequest->bid_request_id]);
                // print_r($Transaction);
                if($Transaction['statusCode']==201){
                    $BidRequest->bid_cost=$request->bid_cost;
                    $BidRequest->status="Bid_Done";
                    $BidRequest->transaction_id=$Transaction['data']['transaction_id'];
                    $BidRequest->save();
                    return response(['message'=>"Your bid request is been placed. Please wait for the passenger to response!","data"=>(object)["transaction_id"=>$Transaction['data']['transaction_id'],"bid_request_id"=>$BidRequest->bid_request_id],"errors"=>array("exception"=>["Everything is OK."])],201);
                }
                else if($Transaction['statusCode']==402){
                    return response(['message'=>$Transaction['message'],"data"=>(object)[],"errors"=>array("exception"=>["Everything is OK."])],$Transaction['statusCode']);
                }
            }
            else{
                return response(['message'=>"You have already bid for this ride.","data"=>(object)["transaction_id"=>$BidRequest->transaction_id,"bid_request_id"=>$BidRequest->bid_request_id,"bid_cost"=>$BidRequest->bid_cost],"errors"=>array("exception"=>["Bid Conflict"])],409);
            }
            return response(['message'=>$Transaction['message'],"data"=>(object)[],"errors"=>array("exception"=>["Error From Transation class."])],$Transaction['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function acceptBidRequest(Request $request){
        try{
            $rule=['request_id' => 'required','bid_request_id' => 'required','driver_id'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            // check wheather passenger is already accpected the request or not
            $PassengerRequest=PassengerRequest::where("passenger_request_id",$request->request_id)->first();
            if($PassengerRequest->request_status=="Requested"){
                $BidRequest=BidRequest::where("bid_request_id",$request->bid_request_id)->where("driver_id",$request->driver_id)->where("status","Bid_Done")->firstOrFail();
                $BidRequest->status="Bid_Accepted_by_passenger";
                $BidRequest->save();
                $PassengerRequest->request_status="Request_Confirm";
                $PassengerRequest->driver_id=$request->driver_id;
                $PassengerRequest->request_bid_id=$BidRequest->bid_request_id;
                $PassengerRequest->save();
                return response(['message'=>'We have notified your driver about your acceptance of their bid.',"data"=>(object)[],"errors"=>array("exception"=>["BID Accepted "])],200);
            }
            else{
                return response(['message'=>'You have already accepted the driver request',"data"=>(object)[],"errors"=>array("exception"=>["BID Accepted Previouly "])],409);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function driverMyBid(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required'];
            $BidRequest=BidRequest::where("driver_id",Auth::user()->id)->where(function($q) {
                $q->where('status', 'Bid_Accepted_by_passenger')
                  ->orWhere('status', 'Bid_Done');
            })->orderBy('bid_request_id', 'desc')
            ->paginate(10)->toArray();
            $TransactionController= new TransactionController();
            $PackageController=new PackageController();
            $myBid=[];
            foreach($BidRequest["data"] as $key=>$val){
               
                if($BidRequest["data"][$key]['created_at']!=""){
                    $BidRequest["data"][$key]['created_at']=$this->convertFromUTC($BidRequest["data"][$key]['created_at'],$request->timeZone);
                }
              //  $Transaction= $TransactionController->getTransactionByID(["transaction_id"=>$BidRequest["data"][$key]['transaction_id']]);
               // $CreditPackage=$PackageController->getPackagesByID(["credit_package_log_id"=>$Transaction['credit_package_log_id']]);
                $passengerRequest=PassengerRequest::select("passenger_id")->where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->first()->toArray();
               // $wayPointsSource=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
               // $wayPointsDestination=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
                //$myBid[$key]['TransactionDetails']= $Transaction;
               // $myBid[$key]['CreditPackage']= $CreditPackage;
             //   $myBid[$key]['passengerRequest']= $passengerRequest;
              //  $myBid[$key]['WaypointRequest']["source"]= $wayPointsSource;
                //$myBid[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBid[$key]['bidDetails']=$BidRequest["data"][$key];
                $myBid[$key]['passengersProfile']=DB::table('passengers_profile')->join('users', 'users.id', '=', 'passengers_profile.user_id')->select('users.first_name','users.last_name', 'users.picture')->where("passengers_profile.user_id",$passengerRequest['passenger_id'])->get()->toArray();
                
                $myBid[$key]['passengersProfile']=$myBid[$key]['passengersProfile'][0];
            }
            $BidRequest["data"]= $myBid;
            return response(['message'=>"Done","data"=>(object)$BidRequest,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function driverMyBooking(Request $request){
        try{
            $BidRequest=BidRequest::where("driver_id",Auth::user()->id)
            ->Where('status', 'Bid_Accepted_by_passenger')
            ->orderBy('bid_request_id', 'desc')
            ->paginate(10)->toArray();
            $TransactionController= new TransactionController();
            $PackageController=new PackageController();
            $myBid=[];
            foreach($BidRequest["data"] as $key=>$val){
                //$Transaction= $TransactionController->getTransactionByID(["transaction_id"=>$BidRequest["data"][$key]['transaction_id']]);
               // $CreditPackage=$PackageController->getPackagesByID(["credit_package_log_id"=>$Transaction['credit_package_log_id']]);
                $passengerRequest=PassengerRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->first()->toArray();
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBid[$key]['TransactionDetails']= $Transaction;
              //  $myBid[$key]['CreditPackage']= $CreditPackage;
                $myBid[$key]['passengerRequest']= $passengerRequest;
                $myBid[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBid[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBid[$key]['bidDetails']=$BidRequest["data"][$key];
                

            }
            $BidRequest["data"]= $myBid;
            return response(['message'=>"Done","data"=>(object)$BidRequest,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function driverCompletedRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }

            $requestBooking=PassengerRequest:: where("driver_id",Auth::user()->id)->where("request_status","Completed")->orderBy('passenger_request_id', 'desc')
            ->paginate(10)->toArray();   
            $myBooking=[];         
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['scheduleDateTime']=$this->checkNull($requestBooking["data"][$key]['scheduleDateTime']);
                if($requestBooking["data"][$key]['scheduleDateTime']!=""){
                    $requestBooking["data"][$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking["data"][$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['updated_at']=$this->checkNull($requestBooking["data"][$key]['updated_at']);
                if($requestBooking["data"][$key]['updated_at']!=""){
                    $requestBooking["data"][$key]['updated_at']=$this->convertFromUTC($requestBooking["data"][$key]['updated_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['driver_remarks']=$this->checkNull($requestBooking["data"][$key]['driver_remarks']);
                $requestBooking["data"][$key]['requested_serviceType']=$this->checkNull($requestBooking["data"][$key]['requested_serviceType']);
                if($requestBooking["data"][$key]['requested_serviceType']==""){
                    $requestBooking["data"][$key]['requested_serviceType']=0;
                }
                $requestBooking["data"][$key]['preference']=$this->checkNull($requestBooking["data"][$key]['preference']);
                
               
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
                $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking["data"][$key]['request_bid_id'])->first()->toArray();
                

            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function driverSheduleRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $currentDateTime = date("Y-m-d H:i");
            $requestBooking=PassengerRequest:: where("driver_id",Auth::user()->id)->where("booking_type","schedule_ride")
            ->where("request_status","Request_Confirm")->where("scheduleDateTime",">",$currentDateTime)->orderBy('passenger_request_id', 'desc')
            ->paginate(10)->toArray();   
            $myBooking=[];         
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['scheduleDateTime']=$this->checkNull($requestBooking["data"][$key]['scheduleDateTime']);
                if($requestBooking["data"][$key]['scheduleDateTime']!=""){
                    $requestBooking["data"][$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking["data"][$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['updated_at']=$this->checkNull($requestBooking["data"][$key]['updated_at']);
                if($requestBooking["data"][$key]['updated_at']!=""){
                    $requestBooking["data"][$key]['updated_at']=$this->convertFromUTC($requestBooking["data"][$key]['updated_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['driver_remarks']=$this->checkNull($requestBooking["data"][$key]['driver_remarks']);
                $requestBooking["data"][$key]['requested_serviceType']=$this->checkNull($requestBooking["data"][$key]['requested_serviceType']);
                if($requestBooking["data"][$key]['requested_serviceType']==""){
                    $requestBooking["data"][$key]['requested_serviceType']=0;
                }
                $requestBooking["data"][$key]['preference']=$this->checkNull($requestBooking["data"][$key]['preference']);


                $wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
                $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking["data"][$key]['request_bid_id'])->first()->toArray();

            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    
    public function driverTotalEarning(Request $request){
        try{
            
            $package=[];
            $totalEarning=DB::select('select sum(bid_cost) as bid_cost from bid_request
            where driver_id = ?  and status=? ', [Auth::user()->id,"Bid_Accepted_by_passenger"]);
            $BidRequest=BidRequest::where("driver_id",Auth::user()->id)
            ->Where('status', 'Bid_Accepted_by_passenger')
            ->orderBy('bid_request_id', 'desc')
            ->paginate(10)->toArray();
            $TransactionController= new TransactionController();
            $PackageController=new PackageController();
            $myBid=[];
            foreach($BidRequest["data"] as $key=>$val){
                //$Transaction= $TransactionController->getTransactionByID(["transaction_id"=>$BidRequest["data"][$key]['transaction_id']]);
               // $CreditPackage=$PackageController->getPackagesByID(["credit_package_log_id"=>$Transaction['credit_package_log_id']]);
                $passengerRequest=PassengerRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->first()->toArray();
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$BidRequest["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBid[$key]['TransactionDetails']= $Transaction;
              //  $myBid[$key]['CreditPackage']= $CreditPackage;
                $myBid[$key]['passengerRequest']= $passengerRequest;
                $myBid[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBid[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBid[$key]['bidDetails']=$BidRequest["data"][$key];
                

            }
            $BidRequest["data"]= $myBid;
            $package=[
                        "totalEarning"=>$totalEarning,
                        "data"=>$myBid
                    ];
                    return ['message'=>"OK","data"=>$package,"errors"=>array("exception"=>["ok"],"e"=>[]),"statusCode"=>200];
                }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>401];
        }
    } 

    public function passengerCompletedRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $requestBooking=PassengerRequest:: where("passenger_id",Auth::user()->id)->where("request_status","Completed")->orderBy('passenger_request_id', 'desc')
            ->paginate(10)->toArray();   
            $myBooking=[];         
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['scheduleDateTime']=$this->checkNull($requestBooking["data"][$key]['scheduleDateTime']);
                if($requestBooking["data"][$key]['scheduleDateTime']!=""){
                    $requestBooking["data"][$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking["data"][$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['updated_at']=$this->checkNull($requestBooking["data"][$key]['updated_at']);
                if($requestBooking["data"][$key]['updated_at']!=""){
                    $requestBooking["data"][$key]['updated_at']=$this->convertFromUTC($requestBooking["data"][$key]['updated_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['driver_remarks']=$this->checkNull($requestBooking["data"][$key]['driver_remarks']);
                $requestBooking["data"][$key]['requested_serviceType']=$this->checkNull($requestBooking["data"][$key]['requested_serviceType']);
                if($requestBooking["data"][$key]['requested_serviceType']==""){
                    $requestBooking["data"][$key]['requested_serviceType']=0;
                }
                $requestBooking["data"][$key]['preference']=$this->checkNull($requestBooking["data"][$key]['preference']);
               
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
                $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking["data"][$key]['request_bid_id'])->first()->toArray();
                

            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function passengerSheduleRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $currentDateTime = date("Y-m-d H:i");
            $requestBooking=PassengerRequest:: where("passenger_id",Auth::user()->id)->where("booking_type","schedule_ride")
            ->where("request_status","Request_Confirm")->where("scheduleDateTime",">",$currentDateTime)->orderBy('passenger_request_id', 'desc')
            ->paginate(10)->toArray();   
            $myBooking=[];         
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['scheduleDateTime']=$this->checkNull($requestBooking["data"][$key]['scheduleDateTime']);
                if($requestBooking["data"][$key]['scheduleDateTime']!=""){
                    $requestBooking["data"][$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking["data"][$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['updated_at']=$this->checkNull($requestBooking["data"][$key]['updated_at']);
                if($requestBooking["data"][$key]['updated_at']!=""){
                    $requestBooking["data"][$key]['updated_at']=$this->convertFromUTC($requestBooking["data"][$key]['updated_at'],$request->timeZone);
                }
                $requestBooking["data"][$key]['driver_remarks']=$this->checkNull($requestBooking["data"][$key]['driver_remarks']);
                $requestBooking["data"][$key]['requested_serviceType']=$this->checkNull($requestBooking["data"][$key]['requested_serviceType']);
                if($requestBooking["data"][$key]['requested_serviceType']==""){
                    $requestBooking["data"][$key]['requested_serviceType']=0;
                }
                $requestBooking["data"][$key]['preference']=$this->checkNull($requestBooking["data"][$key]['preference']);

               
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking["data"][$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
                $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking["data"][$key]['request_bid_id'])->first()->toArray();
                

            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }
    public function RideByRequest(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=['request_id' => 'required','timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $requestBooking=PassengerRequest:: where("passenger_request_id",$request->request_id)->get()->toArray();   
            $myBooking=[];         
            foreach($requestBooking as $key=>$val){
               
                $requestBooking[$key]['scheduleDateTime']=$this->checkNull($requestBooking[$key]['scheduleDateTime']);
                if($requestBooking[$key]['scheduleDateTime']!=""){
                    $requestBooking[$key]['scheduleDateTime']=$this->convertFromUTC($requestBooking[$key]['scheduleDateTime'],$request->timeZone);
                }
                $requestBooking[$key]['created_at']=$this->checkNull($requestBooking[$key]['created_at']);
                if($requestBooking[$key]['created_at']!=""){
                    $requestBooking[$key]['created_at']=$this->convertFromUTC($requestBooking[$key]['created_at'],$request->timeZone);
                }
                $requestBooking[$key]['updated_at']=$this->checkNull($requestBooking[$key]['updated_at']);
                if($requestBooking[$key]['updated_at']!=""){
                    $requestBooking[$key]['updated_at']=$this->convertFromUTC($requestBooking[$key]['updated_at'],$request->timeZone);
                }
                $requestBooking[$key]['driver_remarks']=$this->checkNull($requestBooking[$key]['driver_remarks']);
                $requestBooking[$key]['requested_serviceType']=$this->checkNull($requestBooking[$key]['requested_serviceType']);
                if($requestBooking[$key]['requested_serviceType']==""){
                    $requestBooking[$key]['requested_serviceType']=0;
                }
                $requestBooking[$key]['preference']=$this->checkNull($requestBooking[$key]['preference']);





               
                $wayPointsSource=WaypointRequest::where("passenger_request_id",$requestBooking[$key]['passenger_request_id'])->where("waypoint_type","source")->first()->toArray();
                $wayPointsDestination=WaypointRequest::where("passenger_request_id",$requestBooking[$key]['passenger_request_id'])->where("waypoint_type","destination")->first()->toArray();
              //  $myBooking[$key]['passengerRequest']= $passengerRequest;
                $myBooking[$key]['WaypointRequest']["source"]= $wayPointsSource;
                $myBooking[$key]['WaypointRequest']["destination"]= $wayPointsDestination;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $myBooking[$key]['bidDetails']=BidRequest::where("bid_request_id",$requestBooking[$key]['request_bid_id'])->first()->toArray();
                $myBooking[$key]['passengersProfile']=DB::table('passengers_profile')->join('users', 'users.id', '=', 'passengers_profile.user_id')->select('passengers_profile.*', 'users.*')->where("passengers_profile.user_id",$requestBooking[$key]['passenger_id'])->get()->toArray();
                $myBooking[$key]['driversProfile']=DB::table('drivers_profile')->join('users', 'users.id', '=', 'drivers_profile.user_id')->select('drivers_profile.*', 'users.*')->where("drivers_profile.user_id",$requestBooking[$key]['driver_id'])->get()->toArray();
                $myBooking[$key]['passengersProfile']=$myBooking[$key]['passengersProfile'][0];
                $myBooking[$key]['driversProfile']=$myBooking[$key]['driversProfile'][0];

                

            }
            $requestBooking=[];
            $requestBooking= $myBooking[0];
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }

}

<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use App\Model\Passenger\PassengersProfile;


class ProfileController extends Controller
{
    //
    public function edit(Request $request){
        try{ // 'mobile' => 'required|unique:drivers',
            $rule=[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };

            $user_id=Auth::user()->id;
            $User=User::findOrFail($user_id);
            $User->first_name=$request->first_name;
            $User->last_name=$request->last_name;
            $User->Save();
            $PassengersProfile=PassengersProfile::where("user_id",$user_id)->firstOrFail();
            $PassengersProfile->company_name=$request->company_name;
            $PassengersProfile->save();
            return response(['message'=>"Your profile is been updated","data"=>[],"errors"=>array("exception"=>["Resoures Created"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
    public function get(Request $request){
        try{
            $User=User::find(Auth::user()->id);
            $PassengersProfile=PassengersProfile::where("user_id",Auth::user()->id)->firstOrFail();
            return response(['message'=>"Profile data send","data"=>["user_data"=>$this->setuserData($User),
                "profile_data"=>$this->setPassengerProfileData($PassengersProfile)],"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }
}

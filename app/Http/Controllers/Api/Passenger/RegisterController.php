<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Passenger\PassengersProfile;
use Validator;

class RegisterController extends Controller
{

    public function registration(Request $request){
        try{
               // 'mobile' => 'required|unique:users',
               /// 'password' => 'required|min:6',
               $rule=['social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],'device_type' => 'required|in:android,ios','device_token' => 'required','device_id' => 'required','login_by' => 'required|in:manual,facebook,google','first_name' => 'required|max:255','last_name' => 'required|max:255','email' => 'required|email|max:255|unique:users'];
               $validator=$this->requestValidation($request->all(),$rule);
               if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };

               if($request->social_unique_id=="" || $request->social_unique_id==null){
                $rule=['password' => 'required|min:6'];
                $validator=$this->requestValidation($request->all(),$rule);
               if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            }

            // creating new users
            $User = new User();
            $User->login_by=$request->login_by;
            if($request->login_by==="manual")
                $User->password=bcrypt(trim($request->password));
            else
                $User->social_unique_id =$request->social_unique_id;
            $User->user_scope="passenger-service";
            $User->email=$request->email;
            $User->first_name=$request->first_name;
            $User->last_name=$request->last_name;
            $User->picture=$request->picture;

            $User->save();

            // create the user's profile
            $PassengersProfile= new PassengersProfile();
            $PassengersProfile->user_id=$User->id;
            $PassengersProfile->company_name=$request->company_name;
            $PassengersProfile->device_token=$request->device_token;
            $PassengersProfile->device_id=$request->device_id;
            $PassengersProfile->device_type=$request->device_type;
            $PassengersProfile->payment_mode="CASH";
           $PassengersProfile->save();
           
            if(!empty($User)){

                if($User->id>0){

                    return response(['message'=>"Thank you for registering with us!","data"=>(object)["user_id"=>$User->id],"errors"=>array("exception"=>["Everything is OK."])],201);
                }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["No Content"])],404);
        }

        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }

    }

}

<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ResetPasswordOTP;
use App\Model\Passenger\PassengersProfile;
use Carbon\Carbon;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    //


    public function loginByID($user_id){
        try{
            if(!Auth::loginUsingId($user_id)){
                return ['message'=>'The email address or password you entered is incorrect',
                "errors"=>array("exception"=>["Invalid credentials"])];
            }
            $access_token=Auth::user()->createToken('ZoomXoomPersonalAccessClient',['passenger-service'])->accessToken;
            return ['message'=>'Login successfully',"errors"=>(object)array(),'data'=>array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
                "user_data"=>$this->setuserData(Auth::user())
                )];
                //,PassengersProfile::where("user_id",Auth::user()->id)->firstOrFail();
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])];
        }
    }

    public function login(Request $request){
        try{

            $rule=['email' => 'required|email|max:255', 'password'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422); };

            if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                return response(['message'=>'The email address or password you entered is incorrect',
                "errors"=>array("exception"=>["Invalid credentials"])],401);
            }

            if(Auth::user()->user_scope=="driver-service"){
                $rule=['device_type' => 'required', 'device_token'=>'required', 'device_id'=>'required'];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,
                    "errors"=>array("exception"=>["Request Validation Failed"])],422); };
                    $PassengerProfile=new PassengersProfile();
                    $PassengerProfile->device_token=$request->device_token;
                    $PassengerProfile->device_type=$request->device_type;;
                    $PassengerProfile->device_id=$request->device_id;
                    $PassengerProfile->user_id=Auth::user()->id;
                    $PassengerProfile->save();
                    $user=User::where("id",Auth::user()->id)->firstOrFail();
                    $user->user_scope="driver-passenger-service";
                    Auth::user()->user_scope="driver-passenger-service";
                    $user->save();
                }
                else{
                    $PassengersProfile=PassengersProfile::where("user_id",Auth::user()->id)->first();
                    $PassengersProfile->device_token=$request->device_token;
                    $PassengersProfile->device_id=$request->device_id;
                    $PassengersProfile->device_type=$request->device_type;
                    $PassengersProfile->save();
                }

                $access_token=Auth::user()->createToken('ZoomXoomPersonalAccessClient',['passenger-service'])->accessToken;
                return response(['message'=>'Login successfully',
                "errors"=>(object)array(),
                'data'=>array(
                    "access_token"=>$access_token,
                    "token_type"=>'Bearer',
                    "user_data"=>$this->setuserData(Auth::user())
                    )],200);
                //,PassengersProfile::where("user_id",Auth::user()->id)->firstOrFail()
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
    }

    public function SocialLogin(Request $request){
        try{
            $rule=['login_by'=>'required|in:facebook,google','social_unique_id' => 'required|exists:users'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                if($validator->message==="The selected social unique id is invalid."){
                    return response(['message'=>"User not found","field"=>$validator->field,
                    "errors"=>array("exception"=>["User not found"])],403);
                }
                else{
                    return response(['message'=>$validator->message,"field"=>$validator->field,
                    "errors"=>array("exception"=>["Request Validation Failed"])],422);
                }
            }
            $user=User::where("social_unique_id",$request->social_unique_id)->where("login_by",$request->login_by)->get()->toArray();
            if(empty($user)){ return response(['message'=>"User not found","field"=>"social_unique_id","errors"=>array("exception"=>["User not found"])],403);}
            $token=$this->loginByID($user[0]["id"]);
            return response(['message'=>"Login successfully","data"=>(object)$token['data'],"errors"=>array("exception"=>["Invalid validation"])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */


    public function forgot_password(Request $request){
        try{
            $rule=['email' => 'required|email',];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $User = User::where('email' , $request->email)->firstOrFail();
            $otp = mt_rand(100000, 999999);
            $User->otp = $otp;
            $User->save();
            Notification::send($User, new ResetPasswordOTP($otp));
            return response(['message'=>"OTP sent to your email!","data"=>(object)[],"errors"=>array("exception"=>["Invalid validation"])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"],"e"=>$e)],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }


    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */
    public function  reset_password(Request $request){
        try{
            $rule=['email' => 'required|email','password' => 'required|confirmed|min:6','otp'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $user = User::where('email' , $request->email)->firstOrFail();
            if((int)$user->otp!==(int)$request->otp){
                return response(['message'=>"Invalid Otp","field"=>"otp",
                "errors"=>array("exception"=>["Request Validation Failed"])],403);
            }
            $user->password = bcrypt($request->password);
            $user->save();
            return response(['message'=>"You have reset your password successfully. Now you can login to your account.","data"=>(object)[],"errors"=>array("exception"=>["Password Reset!"])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }

    }

    public function logout(Request $request){
        // SELECT * FROM `oauth_access_tokens` WHERE `scopes` LIKE '%driver-service%'
        try{
            $userID=Auth::user()->id;
            DB::table('oauth_access_tokens')->where('scopes', 'LIKE', '%passenger-service%')->where("user_id",$userID)->delete();
            DB::table('passengers_profile')->where('user_id', $userID)->update(['device_token' => '','device_id'=>'']);
            return response(['message'=>"You have logout successfully.","data"=>(object)[],"errors"=>array("exception"=>["Logout!"])],200);
                       
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
         }
         catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
             return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
         }
         catch(ModelNotFoundException $e)
         {
             return response(['message'=>"Enter email is not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
         }
        }
}

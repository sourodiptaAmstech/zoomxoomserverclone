<?php


namespace App\Http\Controllers\Api\Firebase;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Google\Cloud\Firestore\FirestoreClient;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class FirebaseController extends Controller
{
  private $fireBaseUrl;
  private $key;
  function __construct() {
    $projectID="zoomxoom-1585023674934";
    $this->key="AIzaSyB6jKTGz8y1o6RVlc5HPUrk7fcqJe9joYA";
    $this->fireBaseUrl="https://firestore.googleapis.com/v1/projects/$projectID/databases/(default)/documents/";
    
   // "driver_collection/DRIVER-6?updateMask.fieldPaths=active_ride_type&updateMask.fieldPaths=active_ride_doc&key=AIzaSyB6jKTGz8y1o6RVlc5HPUrk7fcqJe9joYA"
  }
  public function scheduleUpdate($data){
    try{
      Log::info("file firebase Line 24");
   
    $collection=$data->collectionName;
    Log::info("file firebase Line 28");
    $collectionKey=$data->collectionKey;
    Log::info("file firebase Line 30");
    $collectingFieldUpdate=$data->collectingFieldUpdate;
    Log::info("file firebase Line 32");
    $url=$this->fireBaseUrl.$collection.'/'.$collectionKey.'?'.$collectingFieldUpdate.'&'.'key='.$this->key;
    Log::info("file firebase Line 34");
    $CURLOPT_POSTFIELDS=$data->POSTFIELDS;
    Log::info("file firebase Line 36");
    Log::info($url);
    $curl = curl_init();
    Log::info("file firebase Line 39");
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "PATCH",
      CURLOPT_POSTFIELDS =>$CURLOPT_POSTFIELDS,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json"
      ),
    ));
    Log::info("file firebase Line 46");
    $response = curl_exec($curl);
    Log::info("file firebase Line 48");
    curl_close($curl);
    Log::info("file firebase Line 49");
    return $url;
    Log::info("file firebase Line 50");
  }
  catch(\Illuminate\Database\QueryException  $e){
    Log::info(json_encode($e));
    return false;
}
catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
    Log::info(json_encode($e));
    return false;
}
catch(ModelNotFoundException $e)
{
    Log::info(json_encode($e));
    return false;
}
  catch (\Exception $e){
    Log::info("firebase Exception");
    Log::info(json_encode($e));
    Log::info("firebase Exception");
    return false;
}
  }
}
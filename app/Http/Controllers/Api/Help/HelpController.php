<?php


namespace App\Http\Controllers\Api\Help;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class HelpController extends Controller
{
    public function help(Request $request){
        $help=DB::select("SELECT *  FROM `settings` WHERE `key` IN ('contact_email','contact_number','sos_number','web_site')");
        $helpData=[];
        foreach($help as $key =>$val){
            $helpData[$val->key]=$val->value;
        }
        return response(['message'=>"Done","data"=>(object)$helpData,"errors"=>array("exception"=>["Everything is OK."])],200);
    }


}
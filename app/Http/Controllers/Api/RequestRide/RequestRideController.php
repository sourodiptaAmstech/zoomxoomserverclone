<?php

namespace App\Http\Controllers\Api\RequestRide;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Request\PassengerRequest;
use App\Model\Request\WaypointRequest;
use App\Model\Request\BidRequest;
use App\Model\Driver\DriverProfiles;
use App\Model\Passenger\PassengersProfile;
use Illuminate\Support\Facades\DB;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\Firebase\FirebaseController;


/**
 * Class RequestRideController
 * @package App\Http\Controllers\Api\RequestRide
 */
class RequestRideController extends Controller
{
    //
    /**
     * @param int $request_id
     * @param array $WPData
     * @return bool|string
     */
    private function sendNotification(int $request_id, array $WPData,string $bbokingType){
        $db=DB::select('select dp.device_token, dp.device_type from drivers_profile as dp left join bid_request as br on br.passenger_request_id=?
                        where dp.user_id=br.driver_id and dp.device_token<>? and dp.device_token<>? ', [$request_id,"","DEVICE_TOKEN"]);
        $FCMTokens=[];
        $PushNotification="";
        $pushnotificationsData = [];
        if(count($db)>0){
            foreach($db as $key=>$val){
                if($val->device_type=="android"){
                    $FCMTokens['Android'][]=$val->device_token;
                }
                if($val->device_type=="ios"){
                    $FCMTokens['IOS'][]=$val->device_token;
                }
            }
            $pushnotificationsData['to'] = $FCMTokens;
            //EWRIDE/RIDECANCEL/NORMAL/ACOUNTACTIVE/ACOUNTBAND
            $pushnotificationsData['notification'] = array(
                'title' => 'NEW RIDE',
                'text' => '',
                'click_action' => '',
                'body'=>'You have a new ride request'
            );
            $pushnotificationsData['data'] = [
                "type"=>"NEWRIDE"
            ];
            $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
        }
        return $PushNotification;
    }

    private function generateStaticMap($s_latitude,$s_longitude,$d_latitude,$d_longitude){
        try{
            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$s_latitude.",".$s_longitude."&destination=".$d_latitude.",".$d_longitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');

            $json = $this->curl($details);
            $details = json_decode($json, TRUE);
            $route_key = $details['routes'][0]['overview_polyline']['points'];
            $map_icon_start = asset('asset/img/marker-start.png');
            $map_icon_end = asset('asset/img/marker-end.png');
            
            $static_map = "https://maps.googleapis.com/maps/api/staticmap?".
            "autoscale=1".
            "&size=640x260".
            "&maptype=terrain".
            "&format=png".
            "&visual_refresh=true".
            "&markers=icon:".$map_icon_start."%7C".$s_latitude.",".$s_longitude.
            "&markers=icon:".$map_icon_end."%7C".$d_latitude.",".$d_longitude.
            "&path=color:0x000000|weight:2|enc:".$route_key.
            "&key=".env('GOOGLE_MAP_KEY');
            return ["static_map"=>$static_map,"route_key"=>$route_key];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
       
    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function store(Request $request){
        try{
          // print_r($request->all());exit;
            $request['timeZone']=$timeZone=$request->header("timeZone");
            //sss
          //
            $rule=[
                'booking_type' => 'required|in:ride_now,schedule_ride',
                'source_address' => 'required',
                'source_lat' => 'required',
                'source_lng' => 'required',
                'destination_address' => 'required',
                'dest_lat' => 'required',
                'dest_lng' => 'required',
                'timeZone'=>'required'
                ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            if($request->booking_type==="schedule_ride"){
                $rule=[
                    'date'=>'required',
                    'time'=>'required',
                    'service_type'=>'required'
                ];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){
                    return response(['message'=>$validator->message,"field"=>$validator->field,
                        "errors"=>array("exception"=>["Request Validation Failed"])],422);
                }
            }

            // added data to the passenger request table to get the passenger request id
            $PassengerRequest=new PassengerRequest();
            $PassengerRequest->passenger_id=Auth::user()->id;
            $PassengerRequest->request_type='single_waypoint'; // for single or multiwaypoint
            $PassengerRequest->booking_type=$request->booking_type;
            $PassengerRequest->request_status='Requested';
            $PassengerRequest->preference=$request->preference;
            if($request->booking_type==="schedule_ride"){
                $PassengerRequest->scheduleDateTime=$this->convertToUTC(date("Y-m-d H:i:s",strtotime($request->date." ".$request->time)),$request->timeZone);
                $PassengerRequest->requested_serviceType=$request->service_type;
            }
            $mapDetails=$this->generateStaticMap($request->source_lat,$request->source_lng,$request->dest_lat,$request->dest_lng);
            $PassengerRequest->route_key=$mapDetails['route_key'];
            $PassengerRequest->static_map=$mapDetails['static_map'];
            $PassengerRequest->save();

            // storing the scouce and destination details
            $WPData=[
                array(
                    "passenger_request_id"=>$PassengerRequest->passenger_request_id,
                    "address"=>$request->source_address,
                    "longitude"=>$request->source_lng,
                    "latitude"=>$request->source_lat,
                    "waypoint_type"=>"source",
                    "created_at"=>now(),
                    "updated_at"=>now()
                ), array(
                    "passenger_request_id"=>$PassengerRequest->passenger_request_id,
                    "address"=>$request->destination_address,
                    "longitude"=>$request->dest_lng,
                    "latitude"=>$request->dest_lat,
                    "waypoint_type"=>"destination",
                    "created_at"=>now(),
                    "updated_at"=>now()
                )

            ];

            $WaypointRequest=WaypointRequest::insert($WPData);
            $DriverBID=[];
          //  print_r($request->drivers);
            foreach($request->drivers as $key=>$val){
                $DriverBID[]=array("driver_id"=>$val['id'],
                "passenger_request_id"=>$PassengerRequest->passenger_request_id,
                "status"=>'Request_sent_for_bid',
                "created_at"=>now(),
                "updated_at"=>now()
            );
            }
            if(!empty($DriverBID))
                $BidRequest=BidRequest::insert($DriverBID);
                // send notification to the driver againts the request id
                $PushNotification=$this->sendNotification($PassengerRequest->passenger_request_id,$WPData,$request->booking_type);
                return response(['message'=>"Your ride request is been placed. Near by driver will contact you soon!","data"=>(object)["request_id"=>$PassengerRequest->passenger_request_id],"errors"=>array("exception"=>["Everything is OK."],"PushNotification"=>$PushNotification)],201);






        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function ratingRemarksSubmit(Request $request){
        try{
           //echo $calledBy = $request->route()->getName(); exit;
            $rule=[
                'request_id' => 'required',
                'rating' => 'required',
                'remarks' => 'required|max:500'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,
                "errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $PassengerRequest=PassengerRequest::where("passenger_request_id",$request->request_id)->firstOrFail();
            if($request->route()->getName()==="driver.ratingRemarks"){
                $PassengerRequest->passenger_remarks=$request->remarks;
                $PassengerRequest->passenger_rating=$request->rating;
                $PassengerRequest->request_status="Completed";
                $PassengerRequest->save();
                $PassengerRating=PassengerRequest::select(DB::raw('AVG(passenger_rating) as passenger_rating'))->where("passenger_id",$PassengerRequest->passenger_id)->firstOrFail();



                $PassengersProfile=PassengersProfile::where("user_id",$PassengerRequest->passenger_id)->firstOrFail();
                if($PassengersProfile->rating===0.00)
                $PassengersProfile->rating=$PassengersProfile->passenger_rating;
                else
                $PassengersProfile->rating=number_format((float)(($PassengerRating->passenger_rating)), 2, '.', '');
                $PassengersProfile->save();
                
            }
            else if($request->route()->getName()=="passenger.ratingRemarks"){                
                $PassengerRequest->driver_remarks=$request->remarks;
                $PassengerRequest->driver_rating=$request->rating;   
                $PassengerRequest->request_status="Completed";
                $PassengerRequest->save();           
                $DriverRating=PassengerRequest::select(DB::raw('AVG(driver_rating) as driver_rating'))->where("driver_id",$PassengerRequest->driver_id)->firstOrFail();
                $DriverProfiles=DriverProfiles::where("user_id",$PassengerRequest->driver_id)->firstOrFail();
                if($DriverProfiles->rating===0.00)
                $DriverProfiles->rating=$PassengerRequest->driver_rating;
                else
                $DriverProfiles->rating=number_format((float)(($DriverRating->driver_rating)), 2, '.', '');
                $DriverProfiles->save();
            }
           
            return response(['message'=>"Thank you for your feedback!","data"=>(object)["request_id"=>$PassengerRequest->passenger_request_id],"errors"=>array("exception"=>["Everything is OK."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'You dont have any ride',"errors"=>array("exception"=>["No ride"])],403);
        }

    }

    public function sendScheduleRideNotification(){
        try{

            $minutes_to_add = 1;
            //echo "==========";
           // echo "===";
            $currentDateTime = date("Y-m-d H:i",strtotime("+10 minutes",strtotime(date('Y-m-d H:i'))));

            $addedOneMin = new \DateTime($currentDateTime);
            $addedOneMin->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
         //  echo "==========";
          // echo "===";

            $addedOneMin = $addedOneMin->format('Y-m-d H:i');
          //  Log::info("Date Time IN sendScheduleRideNotification :".$currentDateTime);
            Log::info("between ".$currentDateTime." and".$addedOneMin);
            Log::info("file RequestRideController Line 243");

            $db=DB::select('select dp.device_token as driver_device_token,
                                   dp.device_type as driver_device_type,
                                   pp.device_token as passenger_device_token,
                                   pp.device_type as passenger_device_type,
                                   pr.passenger_request_id,pr.driver_id  from drivers_profile as dp
                            left join passenger_request as pr on pr.booking_type=? and pr.request_status=? and  pr.scheduleDateTime BETWEEN ? and ?
                            left join passengers_profile as pp on pp.user_id=pr.passenger_id
                        where dp.user_id=pr.driver_id and dp.device_token<>? and dp.device_token<>? ', ["schedule_ride","Request_Confirm",$currentDateTime,$addedOneMin,"","DEVICE_TOKEN"]);

            if(!empty($db)){
                foreach($db as $key=>$val){
                    $FCMTokens=[];
                    $PushNotification="";
                    $pushnotificationsData = [];
                    if($val->driver_device_type=="android"){
                        $FCMTokens['Android'][]=$val->driver_device_token;
                    }
                    if($val->driver_device_type=="ios"){
                        $FCMTokens['IOS'][]=$val->driver_device_token;
                    }
                    if($val->passenger_device_type=="android"){
                        $FCMTokens['Android'][]=$val->passenger_device_token;
                    }
                    if($val->passenger_device_type=="ios"){
                        $FCMTokens['IOS'][]=$val->passenger_device_token;
                    }
                    $pushnotificationsData['to'] = $FCMTokens;
                    $pushnotificationsData['notification'] = array(
                        'title' => 'Schedule Ride',
                        'text' => '',
                        'click_action' => '',
                        'body'=>'You have a schedule ride'
                    );
                    $pushnotificationsData['data'] = [
                        "type"=>"SCHEDULERIDE",
                        "passenger_request_id"=>$val->passenger_request_id

                    ];
                 
                    Log::info("file RequestRideController Line 324");
                    //write to the firebase 
                    $Firebase=new FirebaseController();
                    Log::info("file RequestRideController Line 325");
                    $fireBaseData=array(
                        'collectionName'=>'driver_collection',
                        'collectionKey'=>'DRIVER-'.$val->driver_id,
                        'collectingFieldUpdate'=>'updateMask.fieldPaths=active_ride_type&updateMask.fieldPaths=active_ride_doc',
                        'POSTFIELDS'=>json_encode(['fields'=>['active_ride_doc'=>['stringValue'=>'ride_req_'.$val->passenger_request_id],'active_ride_type'=>['stringValue'=>'RIDENOW']]])
                    );
                    Log::info("In schedule FireBase Request");
                    Log::info(json_encode($fireBaseData));
                  $firebaseWrite=  $Firebase->scheduleUpdate((object)$fireBaseData);

                  Log::info("In schedule FireBase Request");
                 Log::info("Date Time Pused send:".$currentDateTime);
                
                    Log::info(json_encode($PushNotification));
                    Log::info(json_encode($pushnotificationsData));
                    Log::info("In schedule ride push notification");
                    Log::info("In schedule FireBase Request");
                    Log::info(json_encode($fireBaseData));
                    Log::info("In schedule FireBase write");
                    Log::info(json_encode($firebaseWrite));
                    Log::info("In schedule FireBase write");
                    $PushNotification=PushNotification::PushNotifications($pushnotificationsData);

                }


            }
            return true;
        }
        catch(\Illuminate\Database\QueryException  $e){
            Log::info(json_encode($e));
            return false;
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            Log::info(json_encode($e));
            return false;
        }
        catch(ModelNotFoundException $e)
        {
            Log::info(json_encode($e));
            return false;
        }
        catch (\Exception $e){
            Log::info(json_encode($e));
            return false;
        }
    }

    public function cancelRequest(Request $request){
        try{
            //echo $calledBy = $request->route()->getName(); exit;
             $rule=['request_id' => 'required'];
             $validator=$this->requestValidation($request->all(),$rule);
             if($validator->status=="false"){
                 return response(['message'=>$validator->message,"field"=>$validator->field,
                 "errors"=>array("exception"=>["Request Validation Failed"])],422);
             }
             $PassengerRequest=PassengerRequest::where("passenger_request_id",$request->request_id)->firstOrFail();
             $cancelBy="";
             Log::info(json_encode($request->route()->getName()));
             if($request->route()->getName()==="driver.cancle"){
                 $cancelBy="Cancel_By_Driver";
                 $this->sendCancelNotificationPassenger($PassengerRequest->passenger_id,$request->request_id);
             }
             else if($request->route()->getName()=="passenger.cancle"){
                $cancelBy="Cancel_By_Passenger";
                $this->sendCancelNotificationDriver($PassengerRequest->driver_id,$request->request_id);
            }
             $PassengerRequest->request_status=$cancelBy;
             $PassengerRequest->save();
             return response(['message'=>"You have successfully cancelled the request",
             "data"=>(object)["request_id"=>$PassengerRequest->passenger_request_id],
             "errors"=>array("exception"=>["Everything is OK."])],200);
         }
         catch(\Illuminate\Database\QueryException  $e){
             return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
         }
         catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
             return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
         }
         catch(ModelNotFoundException $e)
         {
             return response(['message'=>'You dont have any ride',"errors"=>array("exception"=>["No ride"])],403);
         }
 
    }

    private function sendCancelNotificationPassenger($passenger_id,$passenger_request_id){
        try{ Log::info(json_encode($passenger_id));
            $db=DB::select('select pp.device_token as passenger_device_token,
            pp.device_type as passenger_device_type
            from  passengers_profile as pp where pp.user_id=? 
            and pp.device_token<>? and pp.device_token<>? ', [$passenger_id,"","DEVICE_TOKEN"]);
            if(!empty($db)){
               
                foreach($db as $key=>$val){
                    Log::info(json_encode($val));
                    $FCMTokens=[];
                    $PushNotification="";
                    $pushnotificationsData = [];
                    if($val->passenger_device_type=="android"){
                        $FCMTokens['Android'][]=$val->passenger_device_token;
                    }
                    if($val->passenger_device_type=="ios"){
                        $FCMTokens['IOS'][]=$val->passenger_device_token;
                    }
                    $pushnotificationsData['to'] = $FCMTokens;
                    Log::info(json_encode($pushnotificationsData));
                    $pushnotificationsData['notification'] = array(
                        'title' => 'Cancel Ride',
                        'text' => '',
                        'click_action' => '',
                        'body'=>'Driver had cancel the schedule ride'
                    );
                    Log::info(json_encode(100));
                    $pushnotificationsData['data'] = [
                        "type"=>"CANCELRIDE",
                        "passenger_request_id"=>$passenger_request_id
                    ];
                    Log::info(json_encode(101));
                    Log::info(json_encode($pushnotificationsData));
                    $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
                    Log::info("sendCancelNotificationPassenger");
                 Log::info("file RequestRideController Line 407");
                    Log::info(json_encode($PushNotification));
                    Log::info(json_encode($pushnotificationsData));
                    Log::info("sendCancelNotificationPassenger");
                }
            }
            return true;
        }
        catch (\Exception $e){
            return false;
        }
    }
    
    private function sendCancelNotificationDriver($passenger_id,$passenger_request_id){
        try{
            $db=DB::select('select pp.device_token as device_token,
            pp.device_type as device_type
            from  drivers_profile as pp where pp.user_id=? 
            and pp.device_token<>? and pp.device_token<>? ', [$passenger_id,"","DEVICE_TOKEN"]);
            if(!empty($db)){
                foreach($db as $key=>$val){
                    $FCMTokens=[];
                    $PushNotification="";
                    $pushnotificationsData = [];
                    if($val->device_type=="android"){
                        $FCMTokens['Android'][]=$val->device_token;
                    }
                    if($val->device_type=="ios"){
                        $FCMTokens['IOS'][]=$val->device_token;
                    }
                    $pushnotificationsData['to'] = $FCMTokens;
                    $pushnotificationsData['notification'] = array(
                        'title' => 'Cancel Ride',
                        'text' => '',
                        'click_action' => '',
                        'body'=>'Passenger had cancel the schedule ride'
                    );
                    $pushnotificationsData['data'] = [
                        "type"=>"CANCELRIDE",
                        "passenger_request_id"=>$passenger_request_id
                    ];
                    $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
                    Log::info("sendCancelNotificationDriver");
                    Log::info("file RequestRideController Line 449");
                       Log::info(json_encode($PushNotification));
                       Log::info(json_encode($pushnotificationsData));
                       Log::info("sendCancelNotificationDriver");
                }
            }
            return true;
        }
        catch (\Exception $e){
            return false;
        }
    }
}

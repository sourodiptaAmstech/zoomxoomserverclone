<?php

namespace App\Http\Controllers\Api\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\Stripe;
use App\Model\Transaction\CreditTransactionLog;

class TransactionController extends Controller
{
    //
    protected function getDeductablePackage($user_id){
        $deductablePackage=[];
        $package=$this->getPackagesBrought($user_id);
        $package=$package['data']['packageDetails'];
        $credit_transaction_log_id = array_column($package, 'credit_transaction_log_id');
        array_multisort($credit_transaction_log_id, SORT_ASC, $package);
        if(count($package)>0){
            foreach($package as $key=>$val){
                if((int)$val['remaining_credit']>0){
                    $deductablePackage=$val;
                    break;
                }
            }
        }
        return $deductablePackage;
    }

    public function buyPackage($data){
        try{


            $CreditTransactionLog=new CreditTransactionLog();
            $CreditTransactionLog->user_id=$data->user_id;
            $CreditTransactionLog->transaction_id=strtotime("now").rand(9999,4);
            $CreditTransactionLog->credit_package_log_id=$data->credit_package_log_id;
            $CreditTransactionLog->transaction_type="credit";
            $CreditTransactionLog->transaction_mode="stripe";
            $CreditTransactionLog->transaction_description=$data->description;
            $CreditTransactionLog->transaction_credit=$data->package_value;
            $CreditTransactionLog->transaction_cost=$data->package_cost;
            $CreditTransactionLog->transaction_status="Pending";
            $CreditTransactionLog->save();
            $Stripe=new Stripe();
            $StripeReturn=$Stripe->createStripeCharge($data);
            if($StripeReturn['status']==true && $StripeReturn['Exception']==false){
                $CreditTransactionLog->transaction_status="Completed";
                $CreditTransactionLog->transaction_payment_gateway_id=$StripeReturn['trans_id'];
                $CreditTransactionLog->save();
                return ['message'=>"transaction Complete","data"=>$CreditTransactionLog,"errors"=>array("exception"=>["transaction Complete"],"e"=>[]),"statusCode"=>201];
            }
            return ['message'=>"transaction Failed","data"=>[],"errors"=>array("exception"=>["transaction Failed"],"e"=>[]),"statusCode"=>402];


        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>400];
        }

    }
    public function getPackagesBrought($user_id){
        try{
            $totalDebitCredit=$this->totalDebitCredit($user_id);
            $package=["packageDetails"=>[],"totalDebitCredit"=>$totalDebitCredit['data']];
            $getPackage=DB::select('select ctl.*,cpl.name,cpl.face_value from credit_transaction_log as ctl
            left join credit_package_log as cpl on cpl.credit_package_log_id=ctl.credit_package_log_id
            where ctl.transaction_type = ? and ctl.transaction_mode=? and ctl.user_id=?', ["credit","stripe",$user_id]);
            if(count($getPackage)>0){
                foreach($getPackage as $key=>$value){
                   // print_r($value);exit;
                    $getCreditSum=DB::select('select sum(transaction_credit) as transaction_credit from credit_transaction_log
                    where refer_transaction_id = ? ', [$value->transaction_id]);
                    $package["packageDetails"][]=[
                        "name"=>$value->name,
                        "face_value"=>$value->face_value,
                        "transaction_status"=>$value->transaction_status,
                        "transaction_cost"=>$value->transaction_cost,
                        "remaining_credit"=>(int)$value->transaction_credit-(int)$getCreditSum[0]->transaction_credit,
                        "credit_package_log_id"=>$value->credit_package_log_id,
                        "transaction_id"=>$value->transaction_id,
                        "credit_transaction_log_id"=>$value->credit_transaction_log_id,
                        "created_at"=>$value->created_at,
                        "updated_at"=>$value->updated_at
                    ];
                }
            }
            return ['message'=>"OK","data"=>$package,"errors"=>array("exception"=>["ok"],"e"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>401];
        }
    }
    public function totalDebitCredit($user_id){
        try{
            $package=[];
            $getDebitSum=DB::select('select sum(transaction_credit) as transaction_credit from credit_transaction_log
                    where user_id = ? and transaction_type=? and transaction_status=? ', [$user_id,"debit","Completed"]);
           $getCreditSum=DB::select('select sum(transaction_credit) as transaction_credit from credit_transaction_log
                    where user_id = ?  and transaction_type=? and transaction_status=?', [$user_id,"credit","Completed"]);
            $package=[
                        "TotalDebit_BitCredit"=>(int)$getDebitSum[0]->transaction_credit,
                        "TotalCredit_BitCredit"=>(int)$getCreditSum[0]->transaction_credit,
                        "remaining_credit"=>(int)$getCreditSum[0]->transaction_credit-(int)$getDebitSum[0]->transaction_credit,
                    ];
                    return ['message'=>"OK","data"=>$package,"errors"=>array("exception"=>["ok"],"e"=>[]),"statusCode"=>200];
                }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>401];
        }
    }
    public function saveTheBid($data){
        try{
            //echo 1; exit;
            // check and get the package value
            $deductablePackage=$this->getDeductablePackage($data['user_id']);
          //  print_r($deductablePackage); exit;

            if(count($deductablePackage)>0){
                $CreditTransactionLog=new CreditTransactionLog();
                $CreditTransactionLog->user_id=$data['user_id'];
                $CreditTransactionLog->transaction_id=strtotime("now").rand(9999,4);
                $CreditTransactionLog->credit_package_log_id=$deductablePackage['credit_package_log_id'];
                $CreditTransactionLog->transaction_type="debit";
                $CreditTransactionLog->transaction_mode="internal";
                $CreditTransactionLog->transaction_description="Credit Use For Bidding";
                $CreditTransactionLog->transaction_credit=1;
                $CreditTransactionLog->refer_transaction_id=$deductablePackage['transaction_id'];
                $CreditTransactionLog->transaction_status="Completed";
                $CreditTransactionLog->bid_id=$data['bid_request_id'];
                $CreditTransactionLog->save();
                return ['message'=>"Credit Use For Bidding","data"=>["transaction_id"=> $CreditTransactionLog->transaction_id],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[]),"statusCode"=>201];


            }

            return ['message'=>"You dont have any credit to bid for the ride","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[]),"statusCode"=>402];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>401];
        }
    }
    public function getTransactionByID($data){
        try{
            $CreditTransactionLog=CreditTransactionLog::where("transaction_id",$data['transaction_id'])->first()->toArray();
            return $CreditTransactionLog;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"]),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"]),"statusCode"=>401];
        }
    }
}

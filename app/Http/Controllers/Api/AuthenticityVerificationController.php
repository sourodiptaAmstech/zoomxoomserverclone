<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Model\Setting;
use App\Model\Driver\DriverProfiles;
use App\User;
use App\Http\Controllers\Api\Passenger\AuthController;
use App\Http\Controllers\Api\Driver\DriverAuthController;
use App\model\Passenger\PassengersProfile;


class AuthenticityVerificationController extends Controller
{
    private $mobile;
    private $otp;
    private $isdCode;

    private function twilioMobileVerification(){
        $twilio_id="AC8a3e5f8b2adfaa3f1af9cbeb265c06a7";
        $twilio_token="073e05c75c6f5211fa3fbe961ddba752";
        $authBase64=base64_encode($twilio_id.":".$twilio_token);
        $twilio_ph_no="+14159656861";
        $digits_otp = 4;
        $this->otp= rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
        //curl of twillo
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twilio.com/2010-04-01/Accounts/$twilio_id/Messages.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_USERPWD=>"$twilio_id:$twilio_token",
            CURLOPT_HTTPAUTH=>CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => "Body=You one time password for verification is $this->otp&From=$twilio_ph_no&To=$this->isdCode.$this->mobile",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic $authBase64"
            ),
        ));
        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
       // print_r($httpcode);
        $response=json_decode($response,true);
        $msg="OTP sent successfully!";
        if($httpcode!==201){
            $msg= $response['message'];
        }

        return ["message"=>$msg,"status"=>$httpcode];

    }

    public function mobile(Request $request){
        try{
            $rule=['user_id' => 'required','user_type' => 'required|in:user,driver','mobile' => 'required','isdCode' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            $user=[];
            if($request->user_type=="user")
            {
                $rule=['mobile' => 'required|unique:users'];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
                $user=User::where("id",$request->user_id)->firstOrFail();
            }
            else if($request->user_type=="driver"){
                $rule=['mobile' => 'required|unique:users'];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
                $user=User::where("id",$request->user_id)->firstOrFail();
            }
            else{
                return response(['message'=>"Invalid User Type","data"=>[],"errors"=>array("exception"=>["Invalid User Type"])],422);
            }
            $this->mobile=$request->mobile;
            $this->isdCode=$request->isdCode;
            $returnData=$this->twilioMobileVerification();
            if($returnData['status']!==201){
                return response(['message'=>$returnData['message'],"data"=>(object)["otp"=>$this->otp],"errors"=>array("exception"=>["From Twillo"])],(int)$returnData['status']);
            }
            else{
                if(!empty($user)){
                    $user->mobile=$this->mobile;
                    $user->isdCode=$this->isdCode;
                    $user->otp=$this->otp;
                    $user->save();
                    return response(['message'=>$returnData['message'],"data"=>(object)["otp"=>$this->otp],"errors"=>array("exception"=>["OTP SEND"])],(int)$returnData['status']);
                }
            }
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }

    public function mobileOTPVerification(Request $request){
        try{
            $rule=['user_id' => 'required','user_type' => 'required|in:user,driver','isValid' => 'required|numeric|max:1'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };

            if($request->user_type=="user"){$user=User::where("id",$request->user_id)->firstOrFail();}
            else if($request->user_type=="driver"){$user=User::where("id",$request->user_id)->firstOrFail();}
            else{return response(['message'=>"Invalid User Type","field"=>"","errors"=>array("exception"=>["Invalid User Type"])],422);}

            $user->isMobileVerified=$request->isValid;
            $user->save();
            if($request->isValid==0)
                return response(['message'=>"Could not able to verify your mobile number! Please try again","data"=>(object)[],"errors"=>array("exception"=>["Mobile Verification Failed"])],200);
            else{
                if($request->user_type=="user"){
                    $UserLogin = new AuthController;
                    $token=$UserLogin->loginByID($user->id);
                    return response(['message'=>"Thank you verifying your mobile number! ","data"=>(object)$token['data'],"errors"=>array("exception"=>["Invalid validation"])],201);
                }
                else if($request->user_type=="driver"){

                    $DriverLogin=new DriverAuthController;
                    $token=$DriverLogin->loginByID($user->id);
                  //  print_r($token); exit;

                    return response(['message'=>"Thank you verifying your mobile number! ","data"=>(object)$token['data'],"errors"=>array("exception"=>["Invalid validation"])],201);
                }
                else{
                    return response(['message'=>'Unable to authorize you. Try again after some time',"errors"=>array("exception"=>["Authorization Failed"])],401);
                }

             //   print_r();exit;

            }

            }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }
}

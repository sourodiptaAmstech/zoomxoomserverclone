<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Model\Setting;

class SettingController extends Controller
{
    //
    public function bootstrap(Request $request){
        try{
            $setting=Setting::all()->toArray();
            if(!empty($setting))
            return response(['message'=>"Bootstrap data send","data"=>(array)$setting,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Bootstrapping not possible. Contact Administrator.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch (Exception $e) {
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Internal Server Error"])],500);
        }
    }
}

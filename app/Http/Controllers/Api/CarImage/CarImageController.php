<?php

namespace App\Http\Controllers\Api\CarImage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;
use App\Model\Driver\DriverCarImages;
use App\Model\Driver\DriverServices;

class CarImageController extends Controller
{
    public function get(Request $request){
        try{
           // $rule=['service_id' => 'required'];
           // $validator=$this->requestValidation($request->all(),$rule);
          //  if($validator->status=="false"){
            //    return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
           // }

           // get service id of the user

           $DriverService=DriverServices::where("user_id",Auth::user()->id)->first();
           $request->service_id=$DriverService->service_id;


            $CarImages=DriverCarImages::where("service_id",$request->service_id)->get()->toArray();
            if(!empty($CarImages)) return response(['message'=>"Car image data send","data"=>(array)$CarImages,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Car image not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function getFromPessanger(Request $request){
        try{
            $rule=['driver_id' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }

           // get service id of the user

           $DriverService=DriverServices::where("user_id",$request->driver_id)->first();
           $request->service_id=$DriverService->service_id;


            $CarImages=DriverCarImages::where("service_id",$request->service_id)->get()->toArray();
            if(!empty($CarImages)) return response(['message'=>"Car image data send","data"=>(array)$CarImages,"errors"=>array("exception"=>["Everything is OK."])],200);
            else
            return response(['message'=>"Car image not found.","data"=>[],"errors"=>array("exception"=>["No Content"])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function store(Request $request){
        try{
            $rule=['picture' => 'required|mimes:jpeg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
             // get service id of the user

           $DriverService=DriverServices::where("user_id",Auth::user()->id)->first();
           $request->service_id=$DriverService->service_id;
            $CarImages=new DriverCarImages();

            if ($request->picture != "") {
               // $Storage=Storage::delete($User->picture);
                $CarImages->image_url = $request->picture->store('public/driver/carImage/'.$request->service_id);
                $CarImages->image_url=str_replace("public", "storage", $CarImages->image_url);
            }
            $CarImages->service_id=$request->service_id;
            $CarImages->save();
            $carImg=$CarImages->toArray();
            return response(['message'=>"Thank you for uploading your car image!","data"=>(array)[$carImg],"errors"=>array("exception"=>["Resoures Created"])],201);
       }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
    public function put(Request $request){

    }
    public function delete(Request $request){
        try{
            $rule=['image_id' => 'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
             // get service id of the user

           $DriverService=DriverServices::where("user_id",Auth::user()->id)->first();
           $request->service_id=$DriverService->service_id;
            $CarImage=DriverCarImages::findOrFail($request->image_id);
            $Storage=Storage::delete($CarImage->image_url);
            $CarImage->delete();

            return response(['message'=>"Car Images is deleted!","data"=>[],"errors"=>array("exception"=>["Resoures Deleted"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }

}

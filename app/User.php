<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'user_scope', 'login_by'
    ];



    public function admin()
    {
        return $this->hasOne('App\Model\Admin', 'user_id', 'id');
    }

    public function passenger_profile()
    {
        return $this->hasOne('App\Model\Passenger\PassengersProfile', 'user_id', 'id');
    }
    public function driver_profile()
    {
        return $this->hasOne('App\Model\Driver\DriverProfiles', 'user_id', 'id');
    }

    public function driver_service()
    {
        return $this->hasOne('App\Model\Driver\DriverServices', 'user_id', 'id');
    }

    public function passenger_request()
    {
        return $this->hasMany('App\Model\Request\PassengerRequest','driver_id','id');
    }

    public function bid_request()
    {
        return $this->hasMany('App\Model\Request\BidRequest','driver_id','id');
    }

    public function report_issue()
    {
        return $this->hasMany('App\Model\ReportIssue\ReportIssue','user_id','id');
    }

}

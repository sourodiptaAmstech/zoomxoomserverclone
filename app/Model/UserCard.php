<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_cards';
    protected $primaryKey = 'user_cards_id';


}

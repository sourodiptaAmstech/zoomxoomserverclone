<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceType extends Model
{
   	use SoftDeletes;

   	protected $table = 'service_types';

    protected $fillable = [
        'name',
        'provider_name',
        'image',
        'insure_price',
        'price',
        'fixed',
        'min_price',
        'description',
        'status',
        'minute',
        'distance',
        'calculator',
        'capacity'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'. 'deleted_at'
    ];

}

<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverRatingAlert extends Model
{
    protected $table = 'driver_rating_alert';
    protected $primaryKey = 'driver_rating_alert_id';
}

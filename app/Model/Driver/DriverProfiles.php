<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DriverProfiles extends Model
{
    //
    use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'drivers_profile';
    protected $primaryKey = 'profile_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
}

<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverDocumentLog extends Model
{
    protected $table = 'driver_document_log';
    protected $primaryKey = 'driver_document_log_id';
}

<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverServices extends Model
{
   //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'driver_services';
    protected $primaryKey = 'service_id';
}

<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverReason extends Model
{
    protected $table = 'driver_reason';
    protected $primaryKey = 'driver_reason_id';
}

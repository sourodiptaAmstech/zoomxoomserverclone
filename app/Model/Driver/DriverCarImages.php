<?php


namespace App\Model\Driver;
use Illuminate\Database\Eloquent\Model;

class DriverCarImages extends Model
{
    protected $table = 'driver_car_images';
    protected $primaryKey = 'image_id';

}

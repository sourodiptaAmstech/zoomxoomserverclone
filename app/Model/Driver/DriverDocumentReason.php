<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverDocumentReason extends Model
{
    protected $table = 'driver_document_reason';
    protected $primaryKey = 'driver_document_reason_id';
}

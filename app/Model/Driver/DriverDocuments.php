<?php

namespace App\Model\Driver;

use Illuminate\Database\Eloquent\Model;

class DriverDocuments extends Model
{
    //
    protected $table = 'driver_documents';

    public function document()
    {
        return $this->belongsTo('App\Model\Document', 'document_id');
    }
}

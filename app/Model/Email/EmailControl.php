<?php

namespace App\Model\Email;

use Illuminate\Database\Eloquent\Model;

class EmailControl extends Model
{
    protected $table = 'email_control';
}

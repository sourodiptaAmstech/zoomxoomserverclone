<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PassengerRequest
 * @package App\Model\Request
 */
class PassengerRequest extends Model
{
    //
    protected $table = 'passenger_request';
    protected $primaryKey = 'passenger_request_id';

    public function userDriver()
    {
        return $this->belongsTo('App\User', 'driver_id');
    }
    
    public function userPassenger()
    {
        return $this->belongsTo('App\User', 'passenger_id');
    }
}

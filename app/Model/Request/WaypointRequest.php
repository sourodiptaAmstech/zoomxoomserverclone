<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class WaypointRequest extends Model
{
    //
    protected $table = 'waypoint_request';
    protected $primaryKey = 'waypoint_request_id';

}

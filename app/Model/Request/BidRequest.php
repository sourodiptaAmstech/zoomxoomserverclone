<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class BidRequest extends Model
{
    //
    protected $table = 'bid_request';
    protected $primaryKey = 'bid_request_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'driver_id');
    }

}

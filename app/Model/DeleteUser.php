<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeleteUser extends Model
{
    protected $table = 'deleted_users';
    protected $primaryKey = 'deleted_user_id';
}

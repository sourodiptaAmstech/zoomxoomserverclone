<?php


namespace App\Model\Transaction;
use Illuminate\Database\Eloquent\Model;

class CreditTransactionLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'credit_transaction_log';
    protected $primaryKey = 'credit_transaction_log_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

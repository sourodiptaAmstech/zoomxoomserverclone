<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Admin extends Model
{
    use SoftDeletes;

    protected $table = 'admins';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'picture',
    ];



    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}

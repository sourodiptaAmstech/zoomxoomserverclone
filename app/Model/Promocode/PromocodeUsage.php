<?php

namespace App\Model\Promocode;

use Illuminate\Database\Eloquent\Model;

class PromocodeUsage extends Model
{
    protected $table = 'promocode_usages';
}

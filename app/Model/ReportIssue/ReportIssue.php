<?php

namespace App\Model\ReportIssue;

use Illuminate\Database\Eloquent\Model;

class ReportIssue extends Model
{
    protected $table = 'report_issues';
    protected $primaryKey = 'issues_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

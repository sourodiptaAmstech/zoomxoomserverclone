<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

//Route::get('bootstrap', 'Api\SettingController@bootstrap')->name("setting.bootstrap");


Route::post('passenger/signup', 'Api\Passenger\RegisterController@registration')->name("passenger.registration");
Route::post('passenger/login', 'Api\Passenger\AuthController@login')->name("passenger.login");
Route::post('passenger/social/login','Api\Passenger\AuthController@SocialLogin')->name("passenger.login");
Route::post('passenger/forgot/password','Api\Passenger\AuthController@forgot_password');
Route::post('passenger/reset/password','Api\Passenger\AuthController@reset_password');

Route::group(['middleware' => ['auth:api','scope:passenger-service,driver-passenger-service']], function () {
    /** profile **/
    Route::post('passenger/profile/image/update', 'Api\UtilityController@profileImageUpdate')->name("passenger.profile.without.login");
    Route::get('passenger/profile', 'Api\Passenger\ProfileController@get')->name("get.passengerProfile");
    Route::put('passenger/profile', 'Api\Passenger\ProfileController@edit')->name("edit.passengerProfile");

     // car images
    Route::post('passenger/car/image/by/driver/id', 'Api\CarImage\CarImageController@getFromPessanger')->name("get.CarImageBYDriverID");


    Route::put('passenger/password/change', 'Api\ChangePassword\ChangePasswordController@changePassword')->name('passenger.password.change');
    Route::post('passenger/request/ride', 'Api\RequestRide\RequestRideController@store')->name('passenger.request.ride');
    Route::put('passenger/accept/ride', 'Api\Bid\BidController@acceptBidRequest')->name('passenger.accept.bid');

    Route::get('passenger/completed/ride', 'Api\Bid\BidController@passengerCompletedRide')->name("get.passengerCompletedRide.bid");
    Route::get('passenger/shedule/ride', 'Api\Bid\BidController@passengerSheduleRide')->name("get.passengerSheduleRide.bid");
    Route::post('passenger/ride/details', 'Api\Bid\BidController@RideByRequest')->name("get.RideByRequest.bid");

      // submit remarks and rating

    Route::put('passenger/submit/rating/remarks', 'Api\RequestRide\RequestRideController@ratingRemarksSubmit')->name('passenger.ratingRemarks');
    Route::put('passenger/cancel/request', 'Api\RequestRide\RequestRideController@cancelRequest')->name('passenger.cancle');

    Route::get('passenger/logout','Api\Passenger\AuthController@logout');

    // report issue
    Route::post('passenger/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('passenger.report.issue');

    //firebase
    Route::post('passenger/firebase','Api\Firebase\FirebaseController@scheduleUpdate')->name('fireBase');
    

});


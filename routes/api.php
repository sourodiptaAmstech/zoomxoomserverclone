<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('bootstrap', 'Api\SettingController@bootstrap')->name("setting.bootstrap");
Route::post('mobile/verification', 'Api\AuthenticityVerificationController@mobile')->name("authenticity.verification");
Route::post('mobile/otp/verification', 'Api\AuthenticityVerificationController@mobileOTPVerification')->name("otp.mobile.verification");
Route::get('vehicles', 'Api\UtilityController@Vehicle')->name("utility.vehicles");
Route::get('service/type', 'Api\UtilityController@getServiceType')->name("utility.service.type");
Route::get('manual/send/push', 'Api\RequestRide\RequestRideController@sendScheduleRideNotification')->name('manual');
Route::get('help', 'Api\Help\HelpController@help')->name('help');


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

    Route::post('driver/signup', 'Api\Driver\RegisterController@registration')->name("driver.registration");
    //Route::post('driver/prelogin/profileimage/upload', 'Api\Driver\RegisterController@preLoginProfileImage')->name("driver.profile.without.login");
    Route::post('driver/login', 'Api\Driver\DriverAuthController@login')->name("passenger.login");
    Route::post('driver/forgot/password','Api\Driver\DriverAuthController@forgot_password');
    Route::post('driver/reset/password','Api\Driver\DriverAuthController@reset_password');

    Route::group(['middleware' => ['auth:api','scope:driver-service,driver-passenger-service']], function () {
        // signup
        Route::post('driver/passenger/signup', 'Api\Driver\RegisterController@passengerReegisterAsDriver')->name("driver.passengerReegisterAsDriver");
        Route::put('driver/password/change', 'Api\ChangePassword\ChangePasswordController@changePassword')->name('driver.password.change');

        // document
        Route::get('driver/document/list', 'Api\UtilityController@list')->name("utility.document.list");
        Route::post('driver/document/upload/{document_id}', 'Api\UtilityController@uploadDocument')->name("utility.document.upload");
        Route::get('driver/document/completion/percentage', 'Api\Driver\StatusController@DocumentCompletion')->name("DocumentCompletion");

        /** **Profile** */
        Route::get('driver/profile/completion/percentage', 'Api\Driver\ProfileController@ProfileCompletion')->name("ProfileCompletion");
        Route::get('driver/profile', 'Api\Driver\ProfileController@getProfile')->name("getDriverProfile");
        Route::post('driver/profile/image/update', 'Api\UtilityController@profileImageUpdate')->name("driver.profile.without.login");
        Route::put('driver/profile', 'Api\Driver\ProfileController@profileEdit')->name("profileEdit");


        // card
        Route::post('driver/add/card', 'Api\Driver\ProfileController@AddCard')->name("post.AddCard");
        Route::get('driver/cards', 'Api\Driver\ProfileController@ListCard')->name("get.ListCard");
        Route::delete('driver/cards', 'Api\Driver\ProfileController@DeleteCard')->name("DeleteCard");
        Route::patch('driver/cards', 'Api\Driver\ProfileController@setDefaultCard')->name("setDefaultCard");

        // car images
        Route::get('driver/car/image', 'Api\CarImage\CarImageController@get')->name("get.CarImage");
        Route::post('driver/car/image', 'Api\CarImage\CarImageController@store')->name("store.CarImage");
        Route::delete('driver/car/image', 'Api\CarImage\CarImageController@delete')->name("delete.CarImage");







        //packages
        Route::get('driver/packages', 'Api\Driver\PackageController@getPackages')->name("get.packages");
        Route::post('driver/buy/packages', 'Api\Driver\PackageController@buyPackages')->name("buy.packages");
        Route::get('driver/bought/packages', 'Api\Driver\PackageController@getBoughtPackages')->name("get.getBroughtPackages");
        Route::get('driver/total/debitcredit', 'Api\Driver\PackageController@totalDebitCredit')->name("get.totalDebitCredit");


        //service
        Route::put('driver/toggle/online/status', 'Api\Driver\ServiceController@toggleOnlineStatus')->name("post.toggleOnlineStatus");

        // place bid
        Route::put('driver/place/bid', 'Api\Bid\BidController@placeBid')->name("put.bid");
        Route::get('driver/my/bid', 'Api\Bid\BidController@driverMyBid')->name("get.driver.bid");
        Route::get('driver/my/booking', 'Api\Bid\BidController@driverMyBooking')->name("get.driverBooking.bid");
        Route::post('driver/total/earning', 'Api\Driver\EarningController@myEarning')->name("get.driverTotalEarning.bid");
       
        Route::get('driver/completed/ride', 'Api\Bid\BidController@driverCompletedRide')->name("get.driverCompletedRide.bid");
        Route::get('driver/shedule/ride', 'Api\Bid\BidController@driverSheduleRide')->name("get.driverSheduleRide.bid");
        Route::post('driver/ride/details', 'Api\Bid\BidController@RideByRequest')->name("get.RideByRequest.bid");

        // submit rating and remarks

        Route::put('driver/submit/rating/remarks', 'Api\RequestRide\RequestRideController@ratingRemarksSubmit')->name('driver.ratingRemarks');
        Route::put('driver/cancel/request', 'Api\RequestRide\RequestRideController@cancelRequest')->name('driver.cancle');
        Route::get('driver/logout','Api\Driver\DriverAuthController@logout');

        // report issue
        Route::post('driver/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('driver.report.issue');
    });


